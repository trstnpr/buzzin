<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AnnouncementRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title' => 'required',
            'announcement' => 'required',
            'published_date' => 'required',
            'expiration_date' => 'required'
        ];

        return $rules;
    }
}
