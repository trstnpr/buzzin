<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ContactUsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email_user' => 'required|email',
            'contact' => 'required|numeric',
            'subject' => 'required',
            'i_am' => 'required',
            'interested_in' => 'required'
        ];
    }
}
