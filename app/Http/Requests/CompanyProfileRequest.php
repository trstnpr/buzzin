<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CompanyProfileRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'first_name' => 'required',
            'last_name' => 'required',
            'company_name' => 'required',
            'brand_name' => 'required',
            'company_website' => 'required',
            'company_contact_no' => 'required|numeric',
            'company_address_1' => 'required',
            'company_city' => 'required',
            'company_state' => 'required',
            'company_zip' => 'required',
            'company_country' => 'required'
        ];

        return $rules;
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'company_contact_no.numeric' => 'The Contact Number field must be a number.'
        ];
    }
}
