<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class RegisterRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'role_id' => 'required',
            'password' => 'required|min:8|confirmed',
            'password_confirmation' => 'required',
            'email' => 'required|email|unique:user',
            'first_name' => 'required',
            'last_name' => 'required',
            'profile_category' => 'required',
            'agree' => 'required'
        ];

        /*if ($this->role_id == 2) {
            $rules['organization_name'] = 'required';
            $rules['about_blogger'] = 'required';
            $rules['blogger_pricing'] = 'required';
            $rules['website'] = 'required';
        }*/
        
        return $rules;
    }
}
