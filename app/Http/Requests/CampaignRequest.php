<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CampaignRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'campaign_name' => 'required',
            'campaign_description' => 'required',
            'campaign_url' => 'required',
            'category_id' => 'required',
            'image_url' => 'mimes:jpeg,jpg,png,gif',
            'file_url' => 'mimes:docx,doc,xls,pdf',
            'fund_details' => 'required',
            'date_start' => 'required',
            'date_end' => 'required',
            'status' => 'required'
        ];

        return $rules;
    }
}
