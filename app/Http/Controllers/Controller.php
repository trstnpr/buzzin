<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;

class Controller extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

    /**
     * Get user role.
     *
     * @param integer $role_id
     * @return string
     */
    protected function getUserRole($role_id)
    {
        switch ($role_id) {
            case 2:
                return 'influencer';
                break;
            case 3:
                return 'company';
                break;
            default:
                return 'admin';
                break;
        }
    }
}
