<?php
namespace App\Services;

/*
* Validation Service
* @author: CAC
* @description: Validation service class.
*
**/

class ValidationService {

	public function __construct(){
		$this->r = USER_REQUIRED_FIELDS; //get from the constants
	}

	public function validate($request,$dataToValidate=array()){
		$validator = \Validator::make($request->all(), self::requiredFields($dataToValidate));
		
		if ($validator->fails()) {
			return self::error_response($validator->errors());
		}
		return null;
	}

	public function error_response($errorMsgs, $statusCode=422){
		return response()->json([
		        'hasError' => 1,
		        'msg' => $errorMsgs
		    ], $statusCode);
	}

	public function success_response($successMsgs){
		return response()->json([
		        'hasError' => 0,
		        'msg' => $successMsgs
		    ], 200);
	}

	//Use this for multi auth
	public function authValidator($role='admin'){
		if(\Auth::guard($role)->check()){
			return true;
		}
		return false;
	}

	public function requiredFields($additionalValidation=array()){
		$required = array_merge($this->r, $additionalValidation);
		return $required;
	}
}