<?php
namespace App\Services;

use League\Flysystem\Exception;

/**
 * A service class to upload images.
 *
 * @author gab
 * @package Teachat\Services
 * @return string
 */
class ImageUploader
{
    /**
     * S3 Instance
     * @var [type]
     */
    private static $uploader;

    /**
     * FileUploader make
     */
    public static function make()
    {
        self::$uploader = new \S3(config('s3.key'), config('s3.secret'), false, config('s3.endpoint'));
        self::$uploader->setExceptions(true);
    }

    /**
     * Upload to S3
     *
     * @param string $fields
     * @param string $file_path
     * @return void
     */
    public static function upload($imageUrl, $campaign)
    {
        self::make();
        try {
            $extension = $imageUrl->getClientOriginalExtension();
            $file_name = str_random(rand(8,16));

        //dd(ltrim('campaign/' . $campaign . '/' .  $file_name  . '.' . $extension, '/'));
            self::$uploader->putObjectFile(
                $imageUrl->getRealPath(),
                config('s3.bucket'),
                ltrim('images/campaign/' . $campaign . '/' .  $file_name  . '.' . $extension, '/'),
                'public-read',
                [], $imageUrl->getMimeType()
            );

            return $file_name  . '.' . $extension;
        } catch(Exception $e) {
            return false;
        }
    }

    public static function deleteS3($full_path)
    {
        self::make();
        try {
            // if(self::$uploader->getObjectInfo(config('s3.bucket'), ltrim($full_path, '/'))) {
                self::$uploader->deleteobject(config('s3.bucket'), ltrim($full_path, '/'));
            //}
            return true;
        } catch(Exception $e) {
            return false;
        }
    }
}
