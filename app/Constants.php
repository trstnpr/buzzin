<?php
/*
* Constants Settings
* @autho: CAC
* @description: b2i constant variables
**/

define('APP_NAME', 'b2i.ph');
define('APP_VERSION', 'v1.0.0');
define('ADMIN_PREFIX_SEGMENT','admin');

define('INFLUENCER_ROLE_ID', 2);
define('BUSINESS_ROLE_ID', 1);

define('SUPER_ADMIN', 1);
define('ADMIN', 2);

const USER_REQUIRED_FIELDS = array(
		'first_name' => 'required',
		'last_name' => 'required',
		'role_id' => 'required'
	);

const USER_ROLES = array(
		BUSINESS_ROLE_ID => 'Business',
		INFLUENCER_ROLE_ID => 'Influencer'
	);

const ADMIN_ROLES = array(
		SUPER_ADMIN => 'Super Admin',
		ADMIN => 'Admin'
	);

const STATUS = array(
		'0' => 'Not Active',
		'1' => 'Active',
		'2' => 'Suspended'
	);