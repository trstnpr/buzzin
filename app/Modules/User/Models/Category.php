<?php namespace App\Modules\User\Models;


use Illuminate\Database\Eloquent\Model;

class Category extends Model {

	protected $table = 'category';

    protected $fillable = ['category'];

    public function blog()
    {
    	return $this->belongsTo('App\Modules\Influencer\Models\Blogs', 'category_id', 'category_id');
    }
}
