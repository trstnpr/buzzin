<?php namespace App\Modules\User\Models;


use Illuminate\Database\Eloquent\Model;

class Region extends Model {

	protected $table = 'region';

    protected $fillable = ['region'];

    protected $primaryKey = 'region_id';

}
