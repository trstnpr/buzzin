<?php namespace App\Modules\User\Models;


use Illuminate\Database\Eloquent\Model;

class ProfileCategory extends Model {

	protected $table = 'profile_category';

    protected $fillable = ['profile_id', 'category_id'];

    protected $primaryKey = 'profile_category_id';

    /**
     * Get the profile_category of the user.
     */
	public function category()
	{
		return $this->hasOne('App\Modules\User\Models\Category', 'category_id', 'category_id');
	}
}
