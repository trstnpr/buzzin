<?php namespace App\Modules\User\Models;

use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model {

	protected $table = 'profile';

    protected $fillable = ['user_id',  'first_name', 'last_name', 'organization_name', 'about_blogger', 'blogger_pricing', 'website', 'address_1', 'address_2', 'city', 'state', 'zip_code', 'country', 'contact_number'];

    protected $primaryKey = 'profile_id';

    /**
     * Get the user of the profile.
     */
	public function user()
	{
		return $this->belongsTo('App\Modules\User\Models\User');
	}

	public function influencerCampaign()
	{
		return $this->hasMany('App\Modules\Influencer\Models\InfluencerCampaign', 'profile_id', 'profile_id');
	}

	public function company()
	{
		return $this->hasOne('App\Modules\Company\Models\Company', 'profile_id', 'profile_id');
	}

	public function campaign()
	{
		return $this->hasMany('App\Modules\Campaign\Models\Campaign', 'profile_id', 'profile_id');
	}

}
