<?php namespace App\Modules\User\Models;


use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable {

	protected $table = 'user';

    protected $fillable = ['email', 'password', 'verification_code', 'role_id', 'status'];

    /**
     * Get the profile of the user.
     */
	public function profile()
	{
		return $this->hasOne('App\Modules\User\Models\UserProfile');
	}

}
