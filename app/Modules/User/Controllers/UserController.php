<?php namespace App\Modules\User\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests\RegisterRequest;
use App\Services\MailSender;
use App\Modules\User\Models\User;
use App\Modules\User\Models\UserProfile;

class UserController extends Controller {

	/**
     * User controller instance.
     *
     * @return void
     */
	public function __construct(){

	}

	/**
     * Create a new user.
     *
     * @param RegisterRequest $request
     * @return view
     */
	public function register(RegisterRequest $request, MailSender $mailSender)
	{
		$verification_code = $this->_generate_verification_code();		

		$request->merge(['password' => bcrypt($request->password), 'verification_code' => $verification_code]);

		$data_user = [
				'email'=>$request->email,
                'password'=>$request->password,
                'verification_code'=>$verification_code,
                'role_id' => $request->role_id
		];

		$user = User::insertGetId($data_user);

		$data_profile = [
				'user_id'=>$user,
				'first_name'=>$request->email,
                'last_name'=>$request->password,
                'organization_name'=>$verification_code
		];

		if($request->role_id == 2){
			$data_profile['about_blogger'] = $request->about_blogger;
			$data_profile['blogger_pricing'] = $request->blogger_pricing;
			$data_profile['website'] = $request->website;
		}

		if($user){
			if(UserProfile::create($data_profile))
			{
				$request->merge(['link' => env('APP_URL') . '/registration/activate/' . $verification_code]);

        		$mailSender->send('email.email_confirmation', 'Email Confirmation', $request->all());

				return json_encode(array('result' => 'success', 'message' => 'Registration Successful. Please check your email inbox to verify your account. <br> Also please check your SPAM folder if you don\'t see it in your inbox.'));
			}

			return json_encode(array('result' => 'error', 'message' => 'There\'s an error encountered while saving'));
			
		}

	}

	/**
     * Generate new verification code.
     *
     * @return string
     */
    private function _generate_verification_code()
    {
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZzxcvbnmasdfghjklqwertyuiop';

        $pin = mt_rand(10, 9999) . mt_rand(10, 9999) . $characters[rand(0, strlen($characters) - 1)];

        return str_shuffle($pin);
    }

    /**
     * Activate user account.
     *
     * @param string $verification_code
     * @return view
     */
    public function activate($verification_code)
    {
        if ($user = User::updateByAttributes(['verification_code' => $verification_code, 'status' => 0], ['status' => 1])) {

            return redirect('login')->with('account_activated', 'Your account is now activated.');
        }

        return redirect('login')->with('account_activated_already', 'Your account is already activated.');
    }

}



