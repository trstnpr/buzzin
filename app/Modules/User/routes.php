<?php

Route::group(array('module' => 'User', 'namespace' => 'App\Modules\User\Controllers'), function() {

    Route::resource('User', 'UserController');

    /*Register User*/
    Route::post('register', 'UserController@register');
    Route::post('register/activate/{verification_code}', 'UserController@activate');

    /*Login User*/
    Route::post('login', 'LoginController@authenticate');
    Route::post('logout', 'LoginController@logout');
    
});	