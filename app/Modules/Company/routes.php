<?php

Route::group(array('module' => 'Company', 'namespace' => 'App\Modules\Company\Controllers', 'middleware' => ['web', 'auth', 'company']), function () {

    // Route::resource('Company', 'CompanyController');

    // Dashboard
    Route::get('company/dashboard', 'DashboardController@dashboard');

    Route::put('company/dashboard/notif/{id}', 'DashboardController@notifStatus');

    // Campaign
    Route::get('company/campaign', 'CampaignController@campaign');
    Route::get('company/campaign/getAllPublished', 'CampaignController@getAllPublished');
    Route::get('company/campaign/getAllUnpublished', 'CampaignController@getAllUnpublished');
    Route::get('company/campaign/create', 'CampaignController@campaignPost');
    Route::post('company/campaign/store', 'CampaignController@store');
    Route::post('company/campaign/{id}/update', 'CampaignController@update');
    Route::delete('company/campaign/{id}', 'CampaignController@destroy');
    Route::get('company/campaign/{id}', 'CampaignController@campaignEdit');
    Route::get('company/campaign/influencer/{id}', 'CampaignController@campaign_influencer');

    // Campaign Counter
    /* Route::get('campaign/{c_view}', 'CampaignController@view_count');
    Route::get('campaign/view/{c_view}', 'CampaignController@campaignDetails');*/

    /*Route::get('company/campaign/view/{c_url}', 'CampaignController@count_url');*/

    // Company Counter
    Route::get('company/settings', 'CompanyController@profileSettings');
    Route::post('company/settings/{id}', 'CompanyController@updateProfile');
    Route::get('company/passwordSetting', 'CompanyController@passwordSetting');
    Route::post('company/changePassword', 'CompanyController@changePassword');

    Route::get('company/settings/category/{profile_id}', 'CompanyController@profileCategory');

    //Influencer Profile
    Route::get('company/influencer_profile/{id}', 'CompanyController@influencerProfile');
    Route::post('company/inviteInfluencer', 'CompanyController@invite_influencer');
});
