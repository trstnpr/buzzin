<?php namespace App\Modules\Company\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model {

	protected $table = 'company';

    protected $fillable = ['profile_id', 'company_name', 'brand_name', 'company_website', 'company_contact_no', 'company_address_1', 'company_city', 'company_state', 'company_zip', 'company_country'];

    public function profile()
    {
    	return $this->belongsTo('App\Modules\User\Models\UserProfile', 'profile_id', 'profile_id');
    }
    public function campaign()
    {
    	return $this->hasmany('App\Modules\Campaign\Models\Campaign', 'profile_id', 'profile_id');
    }
}
