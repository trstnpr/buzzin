@extends('companyMaster')

@section('title')
	<title>Account Settings</title>
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/company/views/index.css') }}" rel="stylesheet">
@stop

@section('content')

	<div class="settings-content">
		<div class="container">

			<h5 class="page-title">Password Settings</h5>

			<div class="section-content">
				<form class="passwordSetting-form" id="form_password_settings">
				<input type="hidden" id="_token" value="{{ csrf_token() }}">
					<div class="row">
						<div class="col l10 s12">
							<div class="card teal hide" id="result_div">
						        <i class="material-icons">check</i> Updated successfully!
						    </div>
							<div class="panel">
								<h6>Want to change your password?</h6>
								<div class="row">
									<div class="col s12">
										<div class="form-group">
											<label>Current Password</label>
											<input type="password" class="form-control b2i-field"  name="current_pass" id="cur_pass" placeholder="Type current password"/>
										</div>
									</div>
									<div class="col l6 s12">
										<div class="form-group">
											<label>New Password</label>
											<input type="password" class="form-control b2i-field"  name="password" id="password"/>
										</div>
									</div>
									<div class="col l6 s12">
										<div class="form-group">
											<label>Confirm New Password</label>
											<input type="password" class="form-control b2i-field"  name="password_confirmation" id="confirm"/>
										</div>
									</div>
								</div>
								<button class="btn purple waves-effect waves-light" type="submit" id="btn_password">update</button>
							</div>
						</div>
					</div>
				</form>
			</div>

		</div>
	</div>
@stop

@section('footer')
	<script src="{{ config('s3.bucket_link') . elixir('assets/company/views/index.js') }}"></script>
	<script src="{{ config('s3.bucket_link') . elixir('assets/company/views/settings/index.js') }}"></script>
@stop
