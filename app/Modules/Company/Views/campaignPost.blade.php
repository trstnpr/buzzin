@extends('companyMaster')

@section('title')
	<title>Post a Campaign</title>
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/company/views/index.css') }}" rel="stylesheet">
@stop

@section('content')

	<div class="campaign-content">
		<div class="container">

			<h5 class="page-title">Post a Campaign</h5>

			<div class="section-content">
				<form class="campaignPost-form" id="form_create_campaign" enctype="multipart/form-data">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="row">
						<div class="col l10 s12">
							<div class="card teal hide" id="result_div">
						        <i class="material-icons">check</i> Added successfully!
						    </div>
							<div class="panel">
								<div class="row">
									<div class="col s12">
										<div class="form-group">
											<label>Campaign</label>
											<input type="text" class="form-control b2i-field" name="campaign_name"/>
										</div>
									</div>
									<div class="col s3">
										<div class="form-group">
											<label>Campaign URL</label>
											<select class="form-control b2i-select-field" name="http">
												<option value="http://"> http:// </option>
												<option value="https://"> https:// </option>
											</select>
										</div>
									</div>
									<div class="col s9">
										<div class="form-group">
											<label>.</label>
											<input type="text" class="form-control b2i-field" name="campaign_url"/>
										</div>
									</div>
									<div class="col s12">
										<div class="form-group">
											<label>Category</label>
											<select class="form-control b2i-select-field" name="category_id">
												<option disabled selected>Choose Category</option>
												@foreach($category as $c)
				                                <option value="{{$c['category_id']}}">{{$c['category']}}</option>
				                                @endforeach
											</select>
										</div>
									</div>

									<div class="col s12">
										<div class="form-group">
											<label>Campaign Image</label>
											<div class="file-control">
												<input type="file" name="image_url" required=""/>
											</div>
										</div>
									</div>

									<div class="col s12">
										<div class="form-group">
											<label>Attach Campaign File / Press Release</label>
											<div class="file-control">
												<input type="file" name="file_url"/>
											</div>
										</div>
									</div>

									<div class="col s12">
										<div class="form-group">
											<label>Description</label>
											<textarea type="text" class="wysiwyg" name="campaign_description"></textarea>
										</div>
									</div>
									<div class="col s12">
										<div class="form-group">
											<label>Mode of Compensation</label>
											<select class="form-control b2i-select-field" id="budget" name="fund_type">
												<option disabled selected>Choose</option>
												<option value="0"> Monetary </option>
												<option value="1"> Non-monetary </option>
											</select>
										</div>
									</div>

									<div class="col s12 hide" id="campaign_budget">
										<div class="form-group">
											<label id="camps">Campaign Budget</label>
											<input type="text" class="form-control b2i-field" name="fund_details"/>
										</div>
									</div>

									<div class="col s12">
										<label>Publication</label><span></span>
										<div class="form-group">
											<input type="radio" id="test1" class="with-gap" value="1" name="status" />
	      									<label for="test1">Publish Now</label><br/>

	      									<input type="radio" id="test2" class="with-gap" value="0" name="status" />
	      									<label for="test2">Save and Edit Later</label>
										</div>
									</div>


									<div class="col m6 s12">
										<div class="form-group">
											<label>Date Start</label>
											<input type="date" class="form-control b2i-field datepicker" name="date_start" id="date_start"/>
										</div>
									</div>
									<div class="col m6 s12">
										<div class="form-group">
											<label>Date End</label>
											<input type="date" class="form-control b2i-field datepicker" name="date_end" id="date_end"/>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="col l2 s12">
							<div class="action-buttons">
								<button class="btn purple btn-large waves-effect waves-light col s12" type="submit" id="btn_create_campaign">submit</button>
								<a href="{{ url('company/campaign') }}" class="btn btn-large waves-effect waves-light red col s12">
									CANCEL
								</a>
							</div>
						</div>
					</div>
				</form>
			</div>

		</div>
	</div>

@stop

@section('footer')
	<script src="{{ config('s3.bucket_link') . elixir('assets/company/views/index.js') }}"></script>
	<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			tinymce.init({
				selector:'.wysiwyg',
				height:150,
				plugins: [
					'advlist autolink lists link image charmap print preview anchor',
					'searchreplace visualblocks code fullscreen',
					'insertdatetime media table contextmenu paste code'
				],
				toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image'
			});
		});

		$(function() {
		    $("#budget").on('change', function() {
		    	$('#campaign_budget').removeClass("hide");

		        if($("#budget").val() == 1){
		        	$('#camps').html("Offer");
		        }
		        else{
		        	$('#camps').html("Campaign Budget");
		        }
		    });
		});

	</script>

	<script src="{{ config('s3.bucket_link') . elixir('assets/company/views/campaign/index.js') }}"></script>
@stop
