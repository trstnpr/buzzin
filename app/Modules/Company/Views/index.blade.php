@extends('companyMaster')

@section('title')
	<title>Dashboard</title>
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/company/views/index.css') }}" rel="stylesheet">

	<style type="text/css">
		.collection-item{
			padding-bottom: 50px !important;
		}

		.truncate {
		  white-space: nowrap;
		  overflow: hidden;
		  text-overflow: ellipsis;
		}

	</style>


@stop

@section('content')

	<div class="dashboard-content">
		<div class="container">

			<h5 class="page-title">Dashboard</h5>
			<div class="section-content">
				<div class="row">
					<div class="col l3 s12">
						<div class="panel-brand date-panel">
							<div class="panel-head">
								<h4 class="panel-title">Date and Time</h4>
							</div>
							<div class="panel-body">
								<h6>
									<span class="day_"><span id="time"></span></span>
	                				<span class="year_"><span id="ampm"></span></span>
                				</h6><br/>
								<h5>
									<span class="day_" id="day_"></span>,
									<span class="year_" id="year_"></span>
								</h5>
								<h5>
									<span class="year_" id="date_"></span>
								</h5>
							</div>
						</div>
					</div>
					<div class="col l3 s12">
						<a href="#notif" id="notif_block" class="modal-trigger">
						<div class="panel-brand messages-panel purple orange">
							<div class="panel-head">
								<h4 class="panel-title">Notifications</h4>
							</div>
							<div class="panel-body">
								<h2 class="panel-label">{{ $count }}</h2>
							</div>
						</div>
						</a>
					</div>
					<div class="col l6 s12">
						<div class="panel-brand announcement-panel">
							<div class="panel-head amber">
								<h4 class="panel-title"><i class="fa fa-list"></i> Announcements</h4>
							</div>
							<div class="panel-body announcement-container">
								<ul class="announcement-content">
									@foreach($announcement as $a)
									<li class="item">
										<h6 class="item-title">{{ $a['title'] }}<small class="right">{{ $a['published_date'] }}</small></h6>
										<p class="item-content">{{ strip_tags($a['announcement']) }}</p>
									</li>
									<li class="item-divider"></li>
									@endforeach

								</ul>
							</div>
						</div>
					</div>
					<div class="col s12">
						<div class="panel-brand campaign-panel">
							<div class="panel-head amber">
								<h4 class="panel-title"><i class="fa fa-bullhorn"></i> Active Campaigns</h4>
							</div>
							<div class="panel-body campaign-container">
								@foreach($campaign as $c)
								<div class="campaign-item">
									<div class="row" style="margin:0;">
										<div class="col l2 hide-on-med-and-down">
											<img src="{{ config('s3.bucket_link') . config('cdn.campaign') .'/'. $c['campaign_image']['image_url'] }}" alt="" class="responsive-img center-block" />
										</div>
										<div class="col l10 s12">
											<h5 class="campaign-title">{{ $c['campaign_name'] }} <a class="btn pill right">{{ $c['category']['category'] }}</a></h5>
											<p class="campaign-description excerpt">{!! str_limit($c['campaign_description'], $limit = 250, $end = '...') !!} <a href="/company/campaign">Read More</a> </p>
										</div>
									</div>
								</div>
								@endforeach
							</div>
						</div>
					</div>

					<div class="col s12">
						<h5>Influencers</h5>
						<div class="row">
							@foreach($influencer as $f)
							<div class="col l3 m6 s12">
								<div class="card profile">
									<div class="influencer-image center" style="padding: 30px;">
										<a href="/company/influencer_profile/{{ $f['profile_id'] }}"><img class="circle" src="{{ config('s3.bucket_link') . elixir('images/assets/user.jpg') }}" height="150px" /></a>
									</div>
									<div class="card-content">
										<span class="truncate card-title grey-text text-darken-4">{{ $f['first_name'] }} {{ $f['last_name'] }}</span>
										<p class="label">{!! $f['city'] !!}</p>
										<br>
										<span class="chip purple white-text" style="border-radius: 2px;">{{ $f['category'] }}</span>
									</div>

								</div>
							</div>
							@endforeach
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>

	<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
		<a href="{{ url('company/campaign/create') }}" class="btn-floating btn-large purple tooltipped" data-position="left" data-tooltip="Post a Campaign">
			<i class="large mdi mdi-tooltip-edit"></i>
		</a>
	</div>

	<!-- Modal Notification Structure -->
	<div id="notif" class="modal bottom-sheet">
	    <div class="modal-content">
	    <input type="hidden" name="_token" value="{{ csrf_token() }}">
	        <h6 class="grey-text"><strong>Campaign Notifications</strong></h6>
	        <ul class="collection">
            @foreach($influencer_campaign as $ic)

               	<li class="collection-item avatar custom-collection blue-grey lighten-5">
               		<img src="{{ config('s3.bucket_link') . config('cdn.campaign') .'/'. $ic['campaign']['campaign_image']['image_url'] }}" alt="Image" class="circle">
               		<ul>
	                    <li><strong><span class="appointment_title">{{ $ic['campaign_name'] }}</span></strong><i class="material-icons right grey-text">notifications</i></li>
	                    <li><span class="appointment_desc">{{ $ic['first_name'] }} {{ $ic['last_name'] }}</span></li>
	                    <li><small>{{ $ic['created_at'] }}</small></span></li>
                    </ul>

                    <div class="notif-buttons right">
	                    <button class="btn waves-effect waves-light col s12 btn_approve" id="btn_approve" onclick="approveCampaign({{ $ic['blogger_campaign_id'] }});">Approve</button>

						<button class="btn waves-effect waves-light red col s12 btn_denied" id="btn_denied" onclick="declineCampaign({{ $ic['blogger_campaign_id'] }});" >Decline</button>
					</div>
                </li>
            @endforeach
        	</ul>
	    </div>
	    <div class="modal-footer grey lighten-3">
	        <a href="#!" class="modal-action modal-close waves-effect waves-green btn purple">Close</a>
	    </div>
	</div>

@stop

@section('footer')
	<script src="{{ config('s3.bucket_link') . elixir('assets/company/views/index.js') }}"></script>
	<script type="text/javascript">
		function approveCampaign(id){
			var param = {
		        status: 1,
		    };

		    $.ajax({
		        url: 'dashboard/notif/'+id,
		        data: param,
		        type: 'PUT',
		        headers:
	            {
	                'X-CSRF-Token': $('input[name="_token"]').val()
	            },
	            beforeSend: function(){ $('.btn_approve').html('Processing...'); document.getElementById("btn_approve").disabled = true; document.getElementById("btn_denied").disabled = true;},
		        success: function(data) {
		           Materialize.toast("You Approved the Application.", 2000,'green',function(){ location.reload(); })
		        }
		    });
		};

		function declineCampaign(id){
			var param = {
		        status: 2,
		    };

		    $.ajax({
		        url: 'dashboard/notif/'+id,
		        data: param,
		        type: 'PUT',
		        headers:
	            {
	                'X-CSRF-Token': $('input[name="_token"]').val()
	            },
	            beforeSend: function(){ $('.btn_denied').html('Processing...'); document.getElementById("btn_approve").disabled = true; document.getElementById("btn_denied").disabled = true; },
		        success: function(data) {
		           Materialize.toast("You Declined the Application.", 2000,'red',function(){ location.reload(); })
		        }
		    });
		};
	</script>
@stop
