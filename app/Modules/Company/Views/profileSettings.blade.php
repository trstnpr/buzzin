@extends('companyMaster')

@section('title')
	<title>Profile Settings</title>
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/company/views/index.css') }}" rel="stylesheet">
@stop

@section('content')

	<div class="settings-content">
		<div class="container">

			<h5 class="page-title">Profile Settings</h5>

			<div class="section-content">
				<form class="profileSetting-form" id="form_bus_settings">
					<input type="hidden" id="_token" value="{{ csrf_token() }}">
					<div class="row">
						<div class="col l10 s12">
							<div class="card teal hide" id="result_div">
						        <i class="material-icons">check</i> Updated successfully!
						    </div>
							<div class="panel">
								<div class="row">
									<div class="col s12">
										<div class="form-group">
											<label>Company</label>
											<input type="text" class="form-control b2i-field" name="company_name" value="{{$company['company_name']}}" required=""  />
										</div>
									</div>
									<div class="col l6 s12">
										<div class="form-group">
											<label>Firstname</label>
											<input type="text" class="form-control b2i-field" value="{{ $profile['first_name']}}" name="first_name" required=""  />
										</div>
									</div>
									<div class="col l6 s12">
										<div class="form-group">
											<label>Lastname</label>
											<input type="text" class="form-control b2i-field" value="{{ $profile['last_name']}}" name="last_name" required=""  />
										</div>
									</div>
									<div class="col s12">
										<label>Category</label>
									    <select class="material-select" name="profile_category[]" id="cat" multiple="multiple" required="" >
									        <option value="" disabled selected>Choose your category</option>
									      	@foreach($category as $c)
				                               <option value="{{$c['category_id']}}">{{$c['category']}}</option>
				                            @endforeach
									    </select>
									</div>
									<div class="col s12">
										<div class="form-group">
											<label>Website</label>
											<input type="text" class="form-control b2i-field" name="company_website" value="{{$company['company_website']}}" />
										</div>
									</div>
									<div class="col s12">
										<div class="form-group">
											<label>Brand Name</label>
											<input type="text" class="form-control b2i-field" name="brand_name" value="{{$company['brand_name']}}" />
										</div>
									</div>
									<div class="col s12">
										<div class="form-group">
											<label>Contact Number</label>
											<input type="text" class="form-control b2i-field" name="company_contact_no" value="{{$company['company_contact_no']}}" required=""  />
										</div>
									</div>
									<div class="col l5 s12">
										<div class="form-group">
											<label>Company Unit/Floor/Bldg. & Street</label>
											<input type="text" class="form-control b2i-field" name="company_address_1" value="{{$company['company_address_1']}}" required=""  />
										</div>
									</div>
									<div class="col l4 s12">
										<div class="form-group">
											<label>Subdivision/Barangay</label>
											<input type="text" class="form-control b2i-field" name="company_address_2" value="{{$company['company_address_2']}}" />
										</div>
									</div>
									<div class="col l3 s12">
										<div class="form-group">
											<label>City</label>
											<input type="text" class="form-control b2i-field" name="company_city" value="{{$company['company_city']}}" required=""  />
										</div>
									</div>
									<div class="col l5 s12" >
										<div class="form-group">
											<label>Select Region</label>
										    <select class="form-control b2i-select-field" id="region_select" name="company_state">
										      	<option value="" disabled selected>Choose your region</option>
									      	@foreach($region as $c)
				                               <option value="{{$c['region']}}" {{ ($c['region'] == $company['company_state']) ? 'selected' : '' }}>{{$c['region']}}</option>
				                            @endforeach
										    </select>
										</div>
									</div>
									<div class="col l4 s12">
										<div class="form-group">
											<label>Zip</label>
											<input type="text" class="form-control b2i-field" name="company_zip" value="{{$company['company_zip']}}" />
										</div>
									</div>
									<div class="col l3 s12">
										<div class="form-group">
											<label>Country</label>
											<input type="text" class="form-control b2i-field" name="company_country" value="{{$company['company_country']}}" required=""  />
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="col l2 s12">
							<div class="action-buttons">
								<button class="btn purple btn-large waves-effect waves-light col s12" type="submit" id="btn_bus_setting">submit</button>
								<button class="btn btn-large waves-effect waves-light red col s12">cancel</button>
							</div>
						</div>
					</div>
				</form>
			</div>

		</div>
	</div>

	<input type="hidden" id="comp_id" value="{{$company['company_id']}}">
	<input type="hidden" id="profile_id" value="{{$company['profile_id']}}">

	{{--<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
		<a href="{{ url('company/campaign/create') }}" class="btn-floating btn-large red tooltipped purple" data-position="left" data-tooltip="Post a Campaign">
			<i class="large mdi mdi-tooltip-edit"></i>
		</a>
	</div>--}}

@stop

@section('footer')
	<script src="{{ config('s3.bucket_link') . elixir('assets/company/views/index.js') }}"></script>
	<script src="{{ config('s3.bucket_link') . elixir('assets/company/views/settings/index.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function() {
		    $('#pc').material_select();
		});
	</script>
@stop
