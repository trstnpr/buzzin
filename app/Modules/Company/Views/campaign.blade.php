@extends('companyMaster')

@section('title')
	<title>Campaign</title>
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/company/views/index.css') }}" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{ config('s3.bucket_link') . elixir('assets/company/jquery.dataTables.min.css') }}">

@stop

@section('content')

	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	<div class="campaign-content">
		<div class="container">

			<h5 class="page-title">Campaign</h5>

			<div class="section-content card-panel">
				<!-- <div class="panel">
					<table class="responsive-table highlight" cellspacing="0" width="100%" id="campaign"></table>
				</div> -->

				<!--Tabs-->
                    <div class="row">
                        <div class="col s12">
                              <ul class="tabs">
                                    <li class="tab col s6"><a class="active" href="#test1">Published Campaign</a></li>
                                    <li class="tab col s5"><a class="" href="#test2">Unpublished Campaign</a></li>
                              </ul>
                        </div>

                        <div id="test1" class="col s12">
                            <table class="responsive-table dataTable striped highlight" cellspacing="0" width="100%" id="campaign">
                            	<thead class="yellow lighten-3">
						            <tr>
						                <th>Campaign Name</th>
						                <th>Campaign Category</th>
						                <th><center>Date Start</center></th>
						                <th><enter>Date End</enter></th>
						                <th><center>Count</center></th>
						                <th><center>Actions</center></th>
						            </tr>
						        </thead>
						        <tbody>
						        	@foreach($campaign as $c)
						            <tr>
						                <td>{{ str_limit($c->campaign_name, 30)}}</td>
						                <td>{{ $c->category }}</td>
						                <td><center>{{ date_format(date_create($c->date_start), "M d, Y") }}</center></td>
						                <td><center>{{ date_format(date_create($c->date_end), "M d, Y") }}</center></td>
						                <td>
						                	<center>
							                	<a data-toggle="modal" data-target="#view-influencer" onclick="viewInfluencer({{$c->campaign_id}}, '{{ $c->campaign_name }}')">
							                		<?php $count = App\Modules\Influencer\Models\InfluencerCampaign::where('campaign_id', $c->campaign_id)->count(); ?> {{ $count }}
							                	</a>
						                	</center>
						                </td>
						                <td class="action">
						                	<center>
						                	<button id="btn-view-campaign" type="button" class="btn btn-primary blue btn-circle white-text btn-view-campaign" title="View" data-toggle="modal" data-target="#view-campaign"
						                        onclick="viewCampaign(this)"
						                        data-campaign-id="{{ $c->campaign_id }}"
						                        data-campaign-name="{{ $c->campaign_name }}"
						                        data-campaign-description="{{ $c->campaign_description }}"
						                        data-category="{{ $c->category }}"
						                        data-pot-money="{{ $c->pot_money }}"
						                        data-fund-type="{{ $c->fund_type }}"
						                        data-fund-details="{{ $c->fund_details }}"
						                        data-campaign-start="{{ date_format(date_create($c->date_start), 'M d, Y') }}"
						                        data-campaign-end="{{ date_format(date_create($c->date_end), 'M d, Y') }}">
						                        <i class="material-icons">search</i>
						                    </button>
						                	<a href="campaign/{{ $c->campaign_id }}" class="btn waves-effect white-text" title="Edit"><i class="material-icons">edit</i></a>
						                	<button id="btn-delete-campaign" type="button" class="btn btn-danger red btn-circle red btn-delete-campaign" title="Delete" data-toggle="modal" data-target="#delete-campaign"
						                        onclick="deleteCampaign(this)"
						                        data-campaign-id="{{ $c->campaign_id }}">
						                        <i class="material-icons">delete</i>
						                    </button>
						                    </center>
						                </td>
						            </tr>
						        	@endforeach
						        </tbody>
                            </table>
                        </div>

                        <div id="test2" class="col s12">
                            <table class="responsive-table highlight" cellspacing="0" width="100%" id="campaignUnpublished"></table>
                        </div>
                    </div>
                <!--End Tabs-->

			</div>
		</div>

	</div>

    <div id="view-campaign" class="modal modal-fixed-footer">
	    <div class="modal-content">
	    	<div class="modal-header-campaign">
	    		<h5>Campaign Name: <span id="name"></span></h5>
	    	</div>

	        <ul class="collection with-header">
	            <li class="collection-item"><div>Descripiton: <span id="desc"></span></div><br /></li>
	            <li class="collection-item"><div>Campaign Category: <span id="cat"></span></div><br /></li>
	            <li class="collection-item"><div>Mode of Compensation <span id="asset"></span></div><br /></li>
	            <li class="collection-item"><div>Campaign Budget / Offer: <span id="fund"></span></div><br /></li>
	            <li class="collection-item"><div>Date Start: <span id="date_start"></span></div><br /></li>
	            <li class="collection-item"><div>Date End: <span id="date_end"></span></div><br /></li>
	        </ul>
	    </div>
	    <div class="modal-footer view-campaign">
	        <a href="#!" class="modal-action modal-close waves-effect waves-red btn grey">Close</a>
	    </div>
	</div>

	<div id="delete-campaign" class="modal">
        <div class="modal-content">
            <h5>Delete Campaign</h5>
            Do you want to delete this campaign?
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action modal-close btn green waves-effect waves-green btn-delete-yes-campaign">Yes</a>
            <a href="#!" class="modal-action modal-close btn red waves-effect waves-red">No</a>
        </div>
    </div>

    <div id="view-influencer" class="modal modal-fixed-footer">
	    <div class="modal-content">
	    	<div class="modal-header-campaign">
	    		<h5>Campaign Name: <span id="c_name"></span></h5>
	    	</div>
	        <ul class="collection with-header" id="view_influencer_ul">
	            <li class="collection-item"><div>Influencers: <span id="list"></span></div><br /></li>
	        </ul>
	    </div>
	    <div class="modal-footer view-influencer">
	        <a href="#!" class="modal-action modal-close waves-effect waves-red btn grey">Close</a>
	    </div>
	</div>
	<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
		<a href="{{ url('company/campaign/create') }}" class="btn-floating btn-large purple tooltipped" data-position="left" data-tooltip="Post a Campaign">
			<i class="large mdi mdi-tooltip-edit"></i>
		</a>
	</div>

@stop

@section('footer')
	<script src="{{ config('s3.bucket_link') . elixir('assets/company/views/index.js') }}"></script>
	<script src="{{ config('s3.bucket_link') . elixir('assets/company/views/campaign/index.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function(){
		    $('.dataTable').DataTable();
		});

		function deleteCampaign(campaign) {
	        $('#delete-campaign').modal('open');
	        $('.btn-delete-yes-campaign').attr('data-campaign-id', $(campaign).attr('data-campaign-id'));
	    }

	    function viewInfluencer(id, name) {
	        $('#view-influencer').modal('open');
	        $('#c_name').html(name);
	        $.get("campaign/influencer/"+id, function(data){
		        if(data.result) {
	               	var model = $('#list');
		            model.empty();   

		            $.each(data.message, function(key, value){
		                model.append("<li class='collection-item'>"+ value.first_name + value.last_name + "</li>");
		            });                  
		        }
		        else{
		        	var model = $('#list');
		            model.empty();
		            model.append("<li class='collection-item'>"+ data.message +"</li>");
		            
		        }

		    });
	    }

	    

	    function viewCampaign(campaign) {
	        $('#view-campaign').modal('open');
	        $('#name').html($(campaign).attr('data-campaign-name'));
	        $('#cat').html($(campaign).attr('data-category'));
	        if($(campaign).attr('data-fund-type') == 0){
	        	$('#asset').html("Monetary");
	        }
	        else{
	        	$('#asset').html("Non-monetary");
	        }

	        $('#fund').html($(campaign).attr('data-fund-details'));
	        $('#f_money').html($(campaign).attr('data-fund-money'));
	        $('#f_details').html($(campaign).attr('data-fund-details'));
	        $('#date_start').html($(campaign).attr('data-campaign-start'));
	        $('#date_end').html($(campaign).attr('data-campaign-end'));
	        $('#desc').html($(campaign).attr('data-campaign-description'));
	    }
	</script>
@stop
