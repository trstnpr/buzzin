@extends('companyMaster')

@section('title')
	<title>Influencer Profile</title>
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/company/views/index.css') }}" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{ config('s3.bucket_link') . elixir('assets/company/jquery.dataTables.min.css') }}">


@stop

@section('content')

	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	<div class="influencer-profile-content">
		<div class="container">

			<h5 class="page-title"></h5>

			<div class="section-content panel">
				<div class="container">
					<div class="row">
						<div class="col l3 m4 s12">
							<div class="campaign-image left">
								<img src="{{ config('s3.bucket_link') . elixir('images/assets/user.jpg') }}" width="200" height="200" alt="Campaign Image" class="circle">
							</div>
						</div>
						<div class="col l5 m4 s12">
							<div class="campaign-info left">
								<div class="">
									<h4>{{ $profile['first_name'] }} {{ $profile['last_name'] }}</h4>

									<p><i class="fa fa-envelope" aria-hidden="true"></i>  {{ $user['email'] }}</p>
									<p><i class="fa fa-map-marker" aria-hidden="true"></i> {{ $profile['city'] }} {{ $profile['state'] }}, {{ $profile['country'] }}</p>
								</div><br>
								@foreach($category as $c)
								<div class="chip purple white-text">
								{{ $c->category }}
								</div>
								@endforeach
							</div>
						</div>
						<div class="col l4 m4 s12">
							<div class="right">
								<button style="display: block; margin-bottom: 10px;" class="btn right" type="button" data-toggle="modal" data-target="#invite-influencer" onclick="inviteInfluencer(this)" ><span><i class="fa fa-plus" aria-hidden="true"></i></span> Invite</button>
								<!-- <button style="display: block;" class="btn right"><span><i class="fa fa-star" aria-hidden="true"></i></span> Add to my Favorites</button> -->
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12">
							{!! $profile['about_blogger'] !!}
						</div>
					</div>

					<div class="row">
						<hr>
						<div>
							<h5 style="font-weight: bold;padding-left: 5px;">Blogs Posts</h5><br>
							<div class="col s12">
								@foreach($blogs as $b)
								<div class="panel" style="border-radius: 1px;background:#f2f2f2;">
									<h6><strong>{{ $b->blog_name }}</strong></h6>
									<p>{{ $b->blog_url }}</p>
									<div>
										<div class="right">
											<span class="chip grey white-text">{{ date_format(date_create($b->created_at), 'M d, Y') }}</span>
											<a href="http://{{ $b->blog_url }}" class="chip teal white-text">View</a>
										</div>
									</div>

								</div>
								@endforeach
							</div>
						</div>
					</div>
				</div>


			</div>
		</div>

	</div>

	<div id="invite-influencer" class="modal">
        <div class="modal-content">
            <h5>Invite Influencer</h5>
            Do you want to invite {{ $profile['first_name'] }} {{ $profile['last_name'] }} to blog your campaign?

            <form class="campaignPost-form" id="form_create_campaign" enctype="multipart/form-data">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" id="profile_id" name="profile_id" value="{{ $profile['profile_id'] }}">
				<input type="hidden" id="type" name="type" value="invite">
				<div class="col s12">
					<div class="form-group">
						<label>Campaign</label>
						<select class="form-control b2i-select-field" id="campaign_id" name="campaign_id">
							<option disabled selected>Choose Campaign</option>
							@foreach($campaign as $c)
				            <option value="{{$c['campaign_id']}}">{{$c['campaign_name']}}</option>
				            @endforeach
							</select>
						</div>
					</div>
			</form>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action modal-close btn green waves-effect waves-green btn-invite-yes-influencer" style="margin-left: 10px;">Yes</a>
            <a href="#!" class="modal-action modal-close btn red waves-effect waves-red btn-invite-no-influencer">No</a>
        </div>
    </div>


@stop

@section('footer')
	<script src="{{ config('s3.bucket_link') . elixir('assets/company/views/index.js') }}"></script>
	<script src="{{ config('s3.bucket_link') . elixir('assets/company/views/campaign/index.js') }}"></script>
	<script type="text/javascript">
		function inviteInfluencer(campaign) {
	        $('#invite-influencer').modal('open');
	    }

	    $(".btn-invite-yes-influencer").click(function () { 
	    	var param = {
			    			profile_id: $('#profile_id').val(),
			    			type: $('#type').val(),
			    			campaign_id: $('#campaign_id').val()
	    				};

	    	console.log(param);

	        $.ajax({
	            url: "/company/inviteInfluencer",
	            headers:
		        {
		            'X-CSRF-Token': $('input[name="_token"]').val()
		        },
	            type: "POST",
	            data: param,
	            beforeSend: function(){ $('.btn-invite-yes-influencer').html('...');
	        							$('.btn-invite-no-influencer').html('...');},
	            /*error: function(data){
	                if(data.readyState == 4){
	                    errors = JSON.parse(data.responseText);
	                    $('#result_div').empty();
	                    $('#result_div').html('<ul class="error_list fa-ul white-text"></ul>');
	                    $.each(errors,function(key,value){
	                        $('.error_list').append('<li><i class="fa fa-li fa-times"></i> '+value+'</li>');
	                    });
	                    $('#result_div').removeClass('teal hide').addClass('red');
	                    $('#btn_create_campaign').html('Submit');

	                    setTimeout(function(){$('#result_div').addClass('hide').html(''); }, 4000);
	                }
	            },*/
	            success: function(data){
	                var msg = JSON.parse(data);
	                if(msg.result == 'success'){
	                   Materialize.toast(msg.message, 1000, "green", function(){ location.reload(); });
	                } else{
	                   Materialize.toast(msg.message, 4000, 'red');
	                }
	            }
	        });
	    });
	</script>

@stop
