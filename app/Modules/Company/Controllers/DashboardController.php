<?php namespace App\Modules\Company\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Modules\User\Models\UserProfile;
use App\Modules\User\Models\User;
use App\Modules\Campaign\Models\Campaign;
use App\Modules\Campaign\Models\CampaignImage;
use App\Modules\Influencer\Models\InfluencerCampaign;
use App\Modules\Admin\Models\Announcement;
use Auth;
use Carbon\carbon;
use App\Modules\User\Models\ProfileCategory;

use App\Services\MailSender;

class DashboardController extends Controller {

	public function dashboard() 
    {
        $userProfile = UserProfile::where('user_id', Auth::user()->id)->first();
        $new_carbon = date("Y-m-d");

        $data['campaign'] = Campaign::with('category')
                                ->with('campaign_image')
                                ->with(['influencer_campaign' => function($query){
                                        $query->select()
                                        ->with(['profile' => function($query){
                                            $query->select('profile_id');
                                        }]);
                                    }])
                                ->where('campaign.profile_id', $userProfile['profile_id'])
                                ->whereDate('date_end', '>=',  $new_carbon)
                                ->whereDate('date_start', '<=', $new_carbon)
                                ->where('campaign.status', 1)
                                ->groupBy('campaign.campaign_id')
                                ->orderBy('date_start', 'desc')
                                ->get();

        $campaign_ids = array();

        if ($data['campaign']) {
            foreach ($data['campaign'] as $key => $value) {
                $campaign_ids[] = $value['campaign_id'];
            }
        }

        else{
            $campaign_ids[] = 0;
        }

        /*$data['influencer'] = InfluencerCampaign::join('profile', 'influencer_campaign.profile_id', '=', 'profile.profile_id')
                                    ->whereIn('campaign_id', $campaign_ids)
                                    ->select('influencer_campaign.*', 'profile.first_name', 'profile.last_name')
                                    ->limit(4)
                                    ->get();*/

        $authCategory = ProfileCategory::where('profile_id', $userProfile['profile_id'])->get();

        $cat_id = array();

        if ($authCategory) {
            foreach ($authCategory as $key => $value) {
                $cat_id[] = $value['category_id'];
            }
        }

        else{
            $cat_id[] = 0;
        }


        $data['influencer'] = ProfileCategory::join('profile', 'profile_category.profile_id', '=', 'profile.profile_id')
                                    ->join('category', 'profile_category.category_id', '=', 'category.category_id')
                                    ->where('profile.profile_id', '!=', $userProfile['profile_id'])
                                    ->whereIn('profile_category.category_id', $cat_id)
                                    ->select('profile.*', 'category.category')
                                    ->limit(4)
                                    ->groupBy('profile_category.profile_id')
                                    ->get();


        $data['announcement'] = Announcement::where('expiration_date', '>=', $new_carbon)
                                ->where('published_date', '<=', $new_carbon)
                                ->orderBy('published_date', 'desc')
                                ->get()
                                ->toArray();

        $data['influencer_campaign'] = InfluencerCampaign::join('campaign', 'influencer_campaign.campaign_id', '=', 'campaign.campaign_id')
                                    ->join('profile as inf_prof', 'influencer_campaign.profile_id', '=', 'inf_prof.profile_id')
                                    ->join('profile', 'campaign.profile_id', '=', 'profile.profile_id')
                                    ->join('user', 'profile.user_id', '=', 'user.id')
                                    ->with(['campaign' => function($query){
                                        $query->select()
                                        ->with(['campaign_image' => function($query){
                                            $query->select();
                                        }]);
                                    }])
                                    ->select('influencer_campaign.*', 'campaign.campaign_name', 'inf_prof.first_name', 'inf_prof.last_name')
                                    ->where('user.id', Auth::user()->id)
                                    ->where('influencer_campaign.type', 'apply')
                                    ->where('influencer_campaign.status', 0)
                                    ->get();

        $data['count'] = InfluencerCampaign::join('campaign', 'influencer_campaign.campaign_id', '=', 'campaign.campaign_id')
                                    ->join('profile as inf_prof', 'influencer_campaign.profile_id', '=', 'inf_prof.profile_id')
                                    ->join('profile', 'campaign.profile_id', '=', 'profile.profile_id')
                                    ->join('user', 'profile.user_id', '=', 'user.id')
                                    ->with(['campaign' => function($query){
                                        $query->select()
                                        ->with(['campaign_image' => function($query){
                                            $query->select();
                                        }]);
                                    }])
                                    ->select('influencer_campaign.*', 'campaign.campaign_name', 'inf_prof.first_name', 'inf_prof.last_name')
                                    ->where('user.id', Auth::user()->id)
                                    ->where('influencer_campaign.type', 'apply')
                                    ->where('influencer_campaign.status', 0)
                                    ->count();

		return view('Company::index', $data);
	}

    /**
     * Update Status of Application in Campaign.
     *
     * @return response
     */
    public function notifStatus($id, Request $request, MailSender $mailSender) 
    {   
        $blogger = InfluencerCampaign::where('blogger_campaign_id', $id)->first();
        $campaign = Campaign::where('campaign_id', $blogger['campaign_id'])->first();

        $profile = InfluencerCampaign::join('profile', 'influencer_campaign.profile_id', '=', 'profile.profile_id')
                            ->join('user', 'profile.user_id', '=', 'user.id')
                            ->select('user.email', 'profile.first_name', 'profile.last_name')
                            ->where('influencer_campaign.blogger_campaign_id', $id)
                            ->first();

        if($request->status == 1){
            $url_code = $this->_generate_url_code();
            $new_url =  $campaign['campaign_name'] .'_'. $campaign['campaign_id'] .'_'. $url_code;

            $request->merge(['email' => $profile['email'], 'last_name' => $profile['last_name'], 'first_name' => $profile['first_name'],  'link' => env('APP_URL') . '/campaign/' . $new_url]);

            $mailSender->send('email.approve_application', 'Campaign Application', $request->all());
        }
        else{
            $new_url = '';
        }
    
        $status = InfluencerCampaign::where('blogger_campaign_id', $id)->update(['status' => $request->status, 'generated_url' => $new_url]);

        if($status){
            return json_encode(array('success' => 'success', 'message' => 'Successfully Updated!'));
        }

        return json_encode(array('result' => 'error', 'message' => 'There is an error occured while saving. Please try again later.'));
    }

    /**
     * Generate url code.
     *
     * @return string
     */
    private function _generate_url_code()
    {
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZzxcvbnmasdfghjklqwertyuiop';

        $pin = mt_rand(10, 99) . mt_rand(10, 99) . $characters[rand(0, strlen($characters) - 1)];

        return str_shuffle($pin);
    }
}
