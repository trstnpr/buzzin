<?php namespace App\Modules\Company\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Http\Requests\CampaignRequest;
use App\Http\Requests\CompanyProfileRequest;
use App\Http\Requests\ChangePasswordRequest;
use App\Modules\User\Models\UserProfile;
use App\Modules\User\Models\User;
use App\Modules\User\Models\Category;
use App\Modules\Campaign\Models\Campaign;
use App\Modules\Campaign\Models\CampaignImage;
use App\Modules\Company\Models\Company;
use App\Modules\Influencer\Models\InfluencerCampaign;
use Auth;
use Carbon\carbon;
use App\Services\ImageUploader;
use App\Services\FileUploader;

class CampaignController extends Controller {

	/**
     * View Campaign Index Page.
     *
     * @return view
     */
	public function campaign()
    {
        $profile = UserProfile::where('user_id', Auth::user()->id)->first();
        $data['campaign'] = Campaign::where('profile_id', $profile['profile_id'])
                    ->join('category', 'campaign.category_id', '=', 'category.category_id')
                    ->select('campaign.*', 'category.category')
                    ->where('campaign.status', 1)
                    ->get();
                   
		return view('Company::campaign', $data);
	}

    /**
     * Get all campaign.
     *
     * @return array
     */
    public function getAllPublished()
    {
        $profile = UserProfile::where('user_id', Auth::user()->id)->first();
        $campaign = Campaign::where('profile_id', $profile['profile_id'])
                    ->join('category', 'campaign.category_id', '=', 'category.category_id')
                    ->select('campaign.*', 'category.category')
                    ->where('campaign.status', 1)
                    ->get()
                    ->toArray();

        $sc = array_map(function ($structure) use ($campaign) {

            $start = date_format(date_create($structure['date_start']), "M d, Y");
            $end = date_format(date_create($structure['date_end']), "M d, Y");

            $action = ' <button id="btn-view-campaign" type="button" class="btn btn-primary blue btn-circle white-text btn-view-campaign" title="View" data-toggle="modal" data-target="#view-campaign"
                        onclick="viewCampaign(this)"
                        data-campaign-id="' . $structure['campaign_id'] . '"
                        data-campaign-name="' . $structure['campaign_name'] . '"
                        data-campaign-description="' . strip_tags($structure['campaign_description']) . '"
                        data-category="' . $structure['category'] . '"
                        data-pot-money="' . $structure['pot_money'] . '"
                        data-fund-type="' . $structure['fund_type'] . '"
                        data-fund-details="' . $structure['fund_details'] . '"
                        data-campaign-start="' . $start . '"
                        data-campaign-end="' . $end . '">
                        <i class="material-icons">search</i>
                    </button> ';

            $action .= '<a href="campaign/' . $structure['campaign_id'] . '" class="btn waves-effect white-text" title="Edit">
                            <i class="material-icons">edit</i>
                        </a> ';

            $action .= '<button id="btn-delete-campaign" type="button" class="btn btn-danger red btn-circle red btn-delete-campaign" title="Delete" data-toggle="modal" data-target="#delete-campaign"
                        onclick="deleteCampaign(this)"
                        data-campaign-id="' . $structure['campaign_id'] . '">
                        <i class="material-icons">delete</i>
                    </button>';

            if($structure['campaign_url'] !== ''){
                $link = "https://". $structure['campaign_url'];
                $url = '<a href="' . $link . '"> '.$structure['campaign_url'].' </a>';
            }

            else{
                $url = '<a href="/campaign/'.$structure['generated_url'].'" > /campaign/view/'.$structure['generated_url'].' </a>';
            }
            

            return [
                'name' => str_limit($structure['campaign_name'], 30),
                'category' => $structure['category'],
                'date_start' => $start,
                'date_end' => $end,
                'action' => $action
            ];
        }, $campaign);

        return ['data' => $sc];

    }

    /**
     * Get all campaign.
     *
     * @return array
     */
    public function getAllUnpublished()
    {
        $profile = UserProfile::where('user_id', Auth::user()->id)->first();
        $campaign = Campaign::where('profile_id', $profile['profile_id'])
                    ->join('category', 'campaign.category_id', '=', 'category.category_id')
                    ->select('campaign.*', 'category.category')
                    ->where('campaign.status', 0)
                    ->get()
                    ->toArray();

        $sc = array_map(function ($structure) use ($campaign) {

            $start = date_format(date_create($structure['date_start']), "M d, Y");
            $end = date_format(date_create($structure['date_end']), "M d, Y");

            $action = ' <button id="btn-view-campaign" type="button" class="btn btn-primary blue btn-circle white-text btn-view-campaign" title="View" data-toggle="modal" data-target="#view-campaign"
                        onclick="viewCampaign(this)"
                        data-campaign-id="' . $structure['campaign_id'] . '"
                        data-campaign-name="' . $structure['campaign_name'] . '"
                        data-campaign-description="' . strip_tags($structure['campaign_description']) . '"
                        data-category="' . $structure['category'] . '"
                        data-pot-money="' . $structure['pot_money'] . '"
                        data-fund-type="' . $structure['fund_type'] . '"
                        data-fund-details="' . $structure['fund_details'] . '"
                        data-campaign-start="' . $start . '"
                        data-campaign-end="' . $end . '">
                        <i class="material-icons">search</i>
                    </button> ';

            $action .= '<a href="campaign/' . $structure['campaign_id'] . '" class="btn waves-effect white-text" title="Edit">
                            <i class="material-icons">edit</i>
                        </a> ';

            $action .= '<button id="btn-delete-campaign" type="button" class="btn btn-danger red btn-circle red btn-delete-campaign" title="Delete" data-toggle="modal" data-target="#delete-campaign"
                        onclick="deleteCampaign(this)"
                        data-campaign-id="' . $structure['campaign_id'] . '">
                        <i class="material-icons">delete</i>
                    </button>';

            if($structure['campaign_url'] !== ''){
                $link = "https://". $structure['campaign_url'];
                $url = '<a href="' . $link . '"> '.$structure['campaign_url'].' </a>';
            }

            else{
                $url = '<a href="/campaign/'.$structure['generated_url'].'" > /campaign/view/'.$structure['generated_url'].' </a>';
            }
            

            return [
                'name' => str_limit($structure['campaign_name'], 30),
                'url' => $url,
                'category' => $structure['category'],
                'date_start' => $start,
                'date_end' => $end,
                'action' => $action
            ];
        }, $campaign);

        return ['data' => $sc];

    }

    /**
     * View Create Campaign Page.
     *
     * @return view
     */
	public function campaignPost() {
		$data['category'] = Category::select('category_id', 'category')->get();

		return view('Company::campaignPost', $data);
	}

    /**
     * Store Campaign
     *
     * @return Response
     */
    public function store(CampaignRequest $request)
    {      
        $profile = UserProfile::where('user_id', Auth::user()->id)->first();

        $start_date = date_format(date_create($request->date_start), 'Y-m-d');
        $end_date = date_format(date_create($request->date_end), 'Y-m-d');

        $url_code = $this->_generate_url_code();

        $data_campaign = [
            'profile_id' => $profile['profile_id'],
            'campaign_name' => $request->campaign_name,
            'campaign_description' => $request->campaign_description,
            'category_id' => $request->category_id,
            'fund_type' => $request->fund_type,
            'fund_details' => $request->fund_details,
            'status' => $request->status,
            'date_start' => $start_date,
            'date_end' => $end_date,
            'created_at' => Carbon::now(),
            'campaign_url' => $request->http .''. $request->campaign_url,
            'generated_url' => $request->campaign_name .' '. $url_code
        ];

        $campaign = Campaign::insertGetId($data_campaign);

        if ($request->hasFile('file_url')){
            $fileUrl = $request->file_url;

            $fileUploader = new FileUploader;

            $file = $fileUploader->upload($fileUrl, $campaign);

            $data_campaign_file = [
                'file_url' => $file
            ];

            $url = Campaign::where('campaign_id', $campaign)->update($data_campaign_file);
        }

        if($campaign){
            if ($request->hasFile('image_url')){
                 $imageUrl = $request->image_url;
                $this->_validate_image_format($imageUrl->getClientOriginalExtension());

                            $imageUploader = new ImageUploader;

                            $result = $imageUploader->upload($imageUrl, $campaign);

                            $data_campaign_image = [
                                'campaign_id' => $campaign,
                                'image_url' => $campaign.'/'.$result,
                                'created_at' => Carbon::now()
                            ];

                        $insert_Images = CampaignImage::insert($data_campaign_image);
                /*$images = $request->image_url;*/

                /*if($images != ''){
                    foreach($images as $imageUrl){
                            $this->_validate_image_format($imageUrl->getClientOriginalExtension());

                            $imageUploader = new ImageUploader;

                            $result = $imageUploader->upload($imageUrl, $campaign);

                            $data_campaign_image = [
                                'campaign_id' => $campaign,
                                'image_url' => $campaign.'/'.$result,
                                'created_at' => Carbon::now()
                            ];

                        $insert_Images = CampaignImage::insert($data_campaign_image);
                    }
                }*/
            }

            return json_encode(array('result' => 'success', 'message' => 'Campaign Successfully Added!'));
        }
        return json_encode(array('result' => 'error', 'message' => 'There\'s an error encountered while saving'));
    }

	/**
     * View Edit Campaign Page.
     *
     * @return view
     */
	public function campaignEdit($id) {
		$profile = UserProfile::where('user_id', Auth::user()->id)->first();
		$data['category'] = Category::select('category_id', 'category')->get();

		$data['campaign'] = Campaign::where('campaign_id', $id)
					->join('category', 'campaign.category_id', '=', 'category.category_id')
                    ->with('campaign_image')
					->select('campaign.*', 'category.category')
					->first();

		return view('Company::campaignEdit', $data);
	}

	/**
     * Update Campaign.
     *
     * @return Response
     */
	public function update($id, CampaignRequest $request)
	{	      
		$profile = UserProfile::where('user_id', Auth::user()->id)->first();

		$start_date = date_format(date_create($request->date_start), 'Y-m-d');
        $end_date = date_format(date_create($request->date_end), 'Y-m-d');

        $url_code = $this->_generate_url_code();

		$data_campaign = [
			'campaign_name' => $request->campaign_name,
            'campaign_description' => $request->campaign_description,
            'category_id' => $request->category_id,
            'fund_type' => $request->fund_type,
            'fund_details' => $request->fund_details,
            'status' => $request->status,
            'date_start' => $start_date ,
            'date_end' => $end_date,
            'updated_at' => Carbon::now(),
            'campaign_url' => $request->campaign_url,
            'generated_url' => $request->campaign_name .'_'. $url_code
		];

        if ($request->hasFile('file_url')){
            $fileUrl = $request->file_url;

            $fileUploader = new FileUploader;

            $file = $fileUploader->upload($fileUrl, $id);

            $data_campaign = [
                'file_url' => $file
            ];
        }

		$campaign = Campaign::where('campaign_id', $id)->update($data_campaign);

		if($campaign){

			if ($request->hasFile('image_url')){

                $imageUrl = $request->image_url;
				$del = CampaignImage::where('campaign_id', $id)->delete();

                $this->_validate_image_format($imageUrl->getClientOriginalExtension());

                            $imageUploader = new ImageUploader;

                            $result = $imageUploader->upload($imageUrl, $id);

                            $data_campaign_image = [
                                'campaign_id' => $id,
                                'image_url' => $id.'/'.$result,
                                'updated_at' => Carbon::now(),
                            ];

                        $insert_Images = CampaignImage::insert($data_campaign_image);

				/*$images = $request->image_url;

				if($images != ''){
					foreach($images as $imageUrl){
				            $this->_validate_image_format($imageUrl->getClientOriginalExtension());

				            $imageUploader = new ImageUploader;

				            $result = $imageUploader->upload($imageUrl, $id);

				            $data_campaign_image = [
				            	'campaign_id' => $id,
			            		'image_url' => $id.'/'.$result,
			            		'updated_at' => Carbon::now(),
				            ];

			            $insert_Images = CampaignImage::insert($data_campaign_image);
			        }
				}*/
			}
			return json_encode(array('result' => 'success', 'message' => 'Campaign Successfully Updated!'));
		}
		return json_encode(array('result' => 'error', 'message' => 'There\'s an error encountered while updating'));
	}

	/**
     * Delete campaign.
     *
     * @return Response
     */
    public function destroy($id)
    {
        if (Campaign::where('campaign_id', $id)->delete()) {
        	/*CampaignImage::where('campaign_id', $id)->delete();*/
            return json_encode(array('result' => 'success', 'message' => 'Successfully Deleted.'));
        }

        return json_encode(array('result' => 'error', 'message' => 'There is an error occured while deleting. Please try again later.'));
    }

    /**
     * Validating image format
     *
     * @return string
     */
    private function _validate_image_format($file_extension)
    {
        if (!in_array($file_extension, array('gif', 'png', 'jpg', 'jpeg', 'PNG', 'JPG'))) {
            return redirect()->back()->with('error', 'Image invalid format.')->withInput();
        }
    }

    /**
     * Generate url code.
     *
     * @return string
     */
    private function _generate_url_code()
    {
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZzxcvbnmasdfghjklqwertyuiop';

        $pin = mt_rand(10, 99) . mt_rand(10, 99) . $characters[rand(0, strlen($characters) - 1)];

        return str_shuffle($pin);
    }

    /**
     * Campaign Views.
     *
     * @return string
     */
    public function view_count($c_view)
    {	
    	$campaign = Campaign::where('generated_url', $c_view)->increment('view_count');
        return redirect('/campaign/view/'.''.$c_view);
    }

    /**
     * Campaign Details.
     *
     * @return string
     */
    public function campaignDetails($c_view)
    {      
        $data['campaign_details'] = InfluencerCampaign::where('generated_url', $c_view)
                                ->with([
                                    'campaign' => function($query){
                                        $query->select()
                                        ->with([
                                        'campaign_image' => function($query){
                                            $query->select();
                                        }])
                                        ->with([
                                        'category' => function($query){
                                            $query->select();
                                        }])
                                        ->with([
                                        'profile' => function($query){
                                            $query->select()
                                            ->with([
                                            'company' => function($query){
                                                $query->select();
                                            }]);
                                        }]);
                                    }])
                                ->first();

    	return view('Campaign::index', $data);
    }

    /**
     * Campaign URL Views.
     *
     * @return string
     */
    public function count_url($c_url)
    {   
        $campaign = Campaign::where('campaign', $c_url)->increment('view_count');
        return redirect($c_url);
    }

    /**
     * Campaign URL Views.
     *
     * @return string
     */
    public function campaign_influencer($id)
    {   
        $inf = InfluencerCampaign::join('profile', 'influencer_campaign.profile_id', '=', 'profile.profile_id')
                ->select('profile.first_name', 'profile.last_name')
                ->where('influencer_campaign.campaign_id', $id)
                ->get()->toArray();

        if (!empty($inf)) {
            return ['result' => true, 'message' => $inf];
        }

        return ['result' => false, 'message' => 'No Influencer.'];

    }
}
