<?php namespace App\Modules\Company\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\CompanyProfileRequest;
use App\Modules\Company\Models\Company;
use App\Modules\User\Models\Category;
use App\Modules\User\Models\ProfileCategory;
use App\Modules\User\Models\User;
use App\Modules\User\Models\UserProfile;
use App\Modules\User\Models\Region;
use App\Modules\Influencer\Models\Blogs;
use App\Modules\Campaign\Models\Campaign;
use App\Modules\Influencer\Models\InfluencerCampaign;
use Auth;

class CompanyController extends Controller
{

    /**
     * Profile Settings View.
     *
     * @return array
     */
    public function profileSettings()
    {
        $data['profile'] = UserProfile::where('user_id', Auth::user()->id)->first();
        $data['company'] = Company::where('profile_id', $data['profile']['profile_id'])->first();
        $data['category'] = Category::select('category_id', 'category')->get();
        $data['region'] = Region::select('region_id', 'region')->get();
        return view('Company::profileSettings', $data);
    }

    /**
     * Update Profile Settings.
     *
     * @return Response
     */
    public function updateProfile($id, CompanyProfileRequest $request)
    {
        $profile = UserProfile::where('user_id', Auth::user()->id)->first();

        $data_profile = [
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
        ];

        $data_company = [
            'company_name' => $request->company_name,
            'brand_name' => $request->brand_name,
            'company_website' => $request->company_website,
            'company_contact_no' => $request->company_contact_no,
            'company_address_1' => $request->company_address_1,
            'company_address_2' => $request->company_address_2,
            'company_city' => $request->company_city,
            'company_state' => $request->company_state,
            'company_zip' => $request->company_zip,
            'company_country' => $request->company_country,

        ];
        $profileUpdate = UserProfile::where('user_id', Auth::user()->id)->update($data_profile);
        $company = Company::where('profile_id', $profile['profile_id'])->update($data_company);

        if ($company) {
            $del = ProfileCategory::where('profile_id', $profile['profile_id'])->delete();

            $profile_category = $request->profile_category;

            if ($profile_category != null) {
                foreach ($profile_category as $pc) {
                    $dataPersonal_category = ['profile_id' => $profile['profile_id'], 'category_id' => $pc];
                    $insert_Personal_category = ProfileCategory::insert($dataPersonal_category);
                }
            }

            return json_encode(array('result' => 'success', 'message' => 'Successfully Updated.'));
        }

        return json_encode(array('result' => 'error', 'message' => 'There is an error occured while updating. Please try again later.'));
    }

    /**
     * Password Settings View.
     *
     * @return array
     */
    public function passwordSetting()
    {
        return view('Company::passwordSettings');
    }

    /**
     * Change Password
     *
     * @return view
     */
    public function changePassword(ChangePasswordRequest $request)
    {
        $id = Auth::user()->id;
        if (Auth::validate(['id' => $id, 'password' => $request->current_pass])) {
            $newPassword = bcrypt($request->password);
            if (User::where('id', $id)->update(['password' => $newPassword])) {
                return json_encode(array('result' => 'success', 'message' => 'Password changed successfully.'));
            }

            return json_encode(array('result' => 'success', 'message' => 'Error'));
        } else {
            return json_encode(array('result' => 'error', 'message' => 'Current password does not match.'));
        }

    }

    /**
     * Get all category by company profile id.
     *
     * @param $profile_id
     * @return view
     */
    public function profileCategory($profile_id)
    {
        $prof_category = ProfileCategory::where('profile_id', $profile_id)->get()->toArray();

        if (!empty($prof_category)) {
            return ['result' => true, 'message' => $prof_category];
        }

        return ['result' => false, 'message' => 'No Profile Category Available.'];

    }

    public function influencerProfile($id)
    {      
        $auth = UserProfile::where('user_id', \Auth::user()->id)->first();

        $data['profile'] = UserProfile::where('profile_id', $id)->first();

        $data['user'] = User::where('id', $data['profile']['user_id'])->first();

        $data['blogs'] = Blogs::where('profile_id', $id)->get();

        $data['category'] = ProfileCategory::where('profile_id', $id)
                            ->join('category', 'profile_category.category_id', '=', 'category.category_id')
                            ->get();

        $data['i_campaign'] = InfluencerCampaign::where('profile_id', $id)->whereIn('status', [0,1])->get();

        $i_campaign_ids = array();

        if ($data['i_campaign']) {
            foreach ($data['i_campaign'] as $key => $value) {
                $i_campaign_ids[] = $value['campaign_id'];
            }
        }

        else{
            $i_campaign_ids[] = 0;
        }

        $data['campaign'] = Campaign::where('profile_id', $auth['profile_id'])->whereNotIn('campaign_id', $i_campaign_ids)->get();

        return view('Company::influencer_profile', $data);
    }

    public function invite_influencer(Request $request)
    {   
        $invite = InfluencerCampaign::create($request->all());

        if($invite){
            return json_encode(array('result' => 'success', 'message' => 'Successfully Invited'));
        }

        return json_encode(array('result' => 'error', 'message' => 'An error occured'));
    }
}
