@extends('appMaster')

@section('title')
	<title>Buzzin - About</title>
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/app/views/index.css') }}" rel="stylesheet">

@stop

@section('content')

	<div class="about-page">
		<section class="banner-head parallax-container valign-wrapper">
			<div class="parallax"><img src="{{ config('s3.bucket_link') . elixir('images/assets/header-image.jpg') }}"></div>
		</section>

		<section class="about-content">
			<div class="container">
				<h4 class="section-title" data-aos="fade-right">WHAT IS <span class="b2i">BUZZIN</span></h4>

				<div class="row">
					<div class="col l8 m7 s12">
						<div class="section-text">
							<p>Buzzin connects businesses to influencers by using a high technology system that enables streamlined matching, objective measurement of key performance indicators, and monetization of content.</p>
							<p>We compare and measure hits, reach, and engagement among all content creators across all campaigns. Buzzin is  a search trawler that figures out what you write about, what niche interests you the most, and what your stance is. Then it matchmakes you, our favorite Blogger/Influencer, with your perfect Business partner.</p>
						</div>

					</div>
					<div class="col l4 m7 s12">

						<div class="section-list">
							<ul>
								<li>attend exclusive events</li>
								<li>earn through partnership</li>
								<li>grow your influence</li>
								<li>network with us</li>
							</ul>
						</div>

					</div>
				</div>

				<h4 class="section-title" data-aos="fade-right">WHAT IS <span class="b2i">BLOGAPALOOZA</span></h4>
				<div class="row">
					<div class="col s12">
						<div class="section-text">
							<p>Blogapalooza Inc. is the trailblazer of influencer marketing in the Philippines. Established in 2011 by the founders of WhenInManila.com, the company hosts grand annual networking and marketing events called “Blogapalooza”, and through the combination of interactive booths, hyper specialized talks by industry leaders, and its large network of bloggers, online influencers, and digital superstars, the company has reinvented how blogger and influencer relations are done.</p>
							<p>With the introduction of Buzzin, Blogapalooza Inc. is expanding its horizons and teaching everyone how influencer marketing should be done.</p>
							<p>Blogapalooza is the place to be if you’re a Blogger or Online Influencer looking to meet your future business partners, and fellow bloggers. Pioneered by WhenInManila.com as a small, intimate gathering amongst friends who were either bloggers or business owners way back in 2011, Blogapalooza still keeps that homey vibe while being more upfront about the business implications of our Bloggers and Influencers meeting our trusted Business partners.</p>
						</div>
					</div>
				</div>

				<h4 class="section-title" data-aos="fade-right">WHAT IS <span class="b2i">INFLUENCER MARKETING</span></h4>
				<div class="row">
					<div class="col s12">
						<div class="section-text">
							<p>Influencer marketing is tapping into the thought leaders and opinion makers of a field, and asking them to share their knowledge and opinions with the audience. The audience doesn’t have to be just their fans; it can be anyone looking to gain valuable insight on the topic at hand. Influencer marketing is all about authenticity -- in endorsement, opinion, content, and even measurement.</p>
							<p>From the Philippines’ top beauty bloggers sharing their opinion on the best lippie for morenas to an Instagram famous college student telling her followers where to go in Manila this weekend, influencer marketing is filling our feeds and replacing the way companies get our attention. And Blogapalooza is leading the way.</p>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>

@stop

@section('footer')
	<script src="{{ config('s3.bucket_link') . elixir('assets/app/views/index.js') }}"></script>
	<script src="{{ config('s3.bucket_link') . elixir('assets/app/slick.min.js') }}"></script>
		<!-- Animate on Scroll -->
	<script type="text/javascript">
		AOS.init({
		  duration: 1200
		});
	</script>
@stop
