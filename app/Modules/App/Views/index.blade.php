@extends('appMaster')

@section('title')
	<title>Buzzin by Blogapalooza</title>
@stop

@section('meta')
	{{-- Put Seo tags Here... --}}
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/app/views/index.css') }}" rel="stylesheet">
@stop

@section('content')

	<div class="app-content">

		@include('App::partials.head_banner')

		@include('App::partials.section2')

		@include('App::partials.section3')

		@include('App::partials.section4')

		@include('App::partials.section5')

	 	@include('App::partials.section6')

	</div>

	<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
		<button type="button" class="btn-floating btn-large orange darken-1"
		id="back-to-top" style="display:none;">
			<i class="large material-icons">arrow_upward</i>
		</button>
	</div>

@stop

@section('footer')
	<script src="{{ config('s3.bucket_link') . elixir('assets/app/views/index.js') }}"></script>
	<script src="{{ config('s3.bucket_link') . elixir('assets/app/slick.min.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function() {

			function getTimeRemaining(endtime) {
				var t = Date.parse(endtime) - Date.parse(new Date());
				var seconds = Math.floor((t / 1000) % 60);
				var minutes = Math.floor((t / 1000 / 60) % 60);
				var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
				var days = Math.floor(t / (1000 * 60 * 60 * 24));
				return {
					'total': t,
					'days': days,
					'hours': hours,
					'minutes': minutes,
					'seconds': seconds
				};
            }

            function initializeClock(id, endtime) {
				var clock = document.getElementById(id);
				var daysSpan = clock.querySelector('#day');
				var hoursSpan = clock.querySelector('#hour');
				var minutesSpan = clock.querySelector('#minute');
				var secondsSpan = clock.querySelector('#second');

				function updateClock() {
					var t = getTimeRemaining(endtime);

					daysSpan.innerHTML = t.days;
					hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
					minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
					secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

					if (t.total <= 0) {
						clearInterval(timeinterval);
					}
				}

				updateClock();
				var timeinterval = setInterval(updateClock, 1000);
            }

            var deadline = 'November 05 2016 08:00:59 UTC+8';
            initializeClock('clockdiv', deadline);
		});
	</script>
	<!-- Animate on Scroll -->
	<script type="text/javascript">
		AOS.init({
		  duration: 1200
		});
	</script>
@stop
