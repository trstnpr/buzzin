@extends('appMaster')

@section('title')
	<title>Buzzin - Influencer</title>
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/app/views/index.css') }}" rel="stylesheet">
@stop

@section('content')
	<div class="influencer-page">

		<section class="banner-head parallax-container valign-wrapper">
			<div class="parallax"><img src="{{ config('s3.bucket_link') . elixir('images/assets/header-image.jpg') }}"></div>
		</section>

		<section class="influencer-content">
			<div class="container">
				<h4 class="section-title"><span class="b2i">INFLUENCER</span></h4>

				<div class="article">
					<div class="section-text">
						<p>Get ready to be a part of a whole new world! By being a Buzzin influencer member, you’ve got instant access to one of the most interesting and constantly growing communities here in the Philippines. Being a trusted partner of Blogapalooza means you’ll be constantly leveraging your expertise as an influencer for some of the best brands in the country. While getting paid! Now doesn’t that sound fun?</p>
					</div>
				</div>

				<div class="article">
					<div class="section-text">
						<h5 class="article-title">Influencer Benefits</h5>
						<p class="benefits">Work with New Brands</p><p>Browse through campaigns looking for their long term influencer partner, and discover new brands and new topics to create content for.</p>
						<p class="benefits">Expand Your Audience</p><p>Create content that can promote and help you grow your niche and expertise into topics you never would have thought of.</p>
						<p class="benefits">Earn Money from Every Post</p><p>Get paid to create content on your terms, without compromising the brands you work with.</p>
					</div>
				</div>

				<div class="article-steps">
					<h6 class="article-title">Working as an Influencer on B2<span class="lowercase">i</span> is as easy as 1, 2, 3 - probably as easy as taking a selfie!</h6>
					<p><strong>Step by Step Process:</strong></p>
					<div class="">
						<h6 class="b2i">BROWSE CAMPAIGNS</h6>
						<p>You’ll have access to every campaign we run on Buzzin, but to make sure we’re tapping the right influencers, we’ll only be sending invites to those who fall under the campaign’s category. You can still request to be a part of the campaign, in fact, we’d love you even more if you did. </p>
					</div>
					<div class="">
						<h6 class="b2i">CREATE CONTENT</h6>
						<p>So you’ve been invited to create content (and get paid) for Company X. Now comes the fun part! You, our trusted influencer, can put a spin to the content however you like. You don’t have to copy paste the PR release; we’re actively encouraging you to take it as a springboard to do whatever, as long as it’s cool and totally you!</p>
					</div>
					<div class="">
						<h6 class="b2i">GET PAID</h6>
						<p>The moment the campaign’s done, and the reports are in, you will receive a check with thanks for your amazing contribution to the campaign. And we mean “as soon as the reports are in”, so expect your check ASAP!</p>
						<p>The better you rank against everyone else, the bigger your check. Blogger A might have greater reach, but you’ve got better engagement, and Company X was looking for engagement, so you’re getting paid more. Simple as that.</p>
					</div>
				</div>

				<div class="section-join">
					<a href="{{ route('app.signup') }}" class="btn btn-large waves-effect waves-light orange">JOIN A CAMPAIGN</a>
				</div>

			</div>
		</section>

	</div>
@stop

@section('footer')
	<script src="{{ config('s3.bucket_link') . elixir('assets/app/views/index.js') }}"></script>
	<script src="{{ config('s3.bucket_link') . elixir('assets/app/slick.min.js') }}"></script>
@stop
