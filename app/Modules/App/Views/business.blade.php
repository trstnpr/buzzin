@extends('appMaster')

@section('title')
	<title>Buzzin - Business</title>
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/app/views/index.css') }}" rel="stylesheet">
@stop

@section('content')
	<div class="business-page">

		<section class="banner-head parallax-container valign-wrapper">
			<div class="parallax"><img src="{{ config('s3.bucket_link') . elixir('images/assets/header-image.jpg') }}"></div>
		</section>

		<section class="business-content">
			<div class="container">
				<h4 class="section-title"><span class="b2i">BUSINESS</span></h4>
				<div class="article">
					<div class="section-text">
						<p>Find out exactly how much your marketing campaign is worth using Buzzin and its performance-based pay metrics. With a dashboard that tracks your campaign’s overall reach, engagement, and attitude conversion, you can quantitatively calculate how much to spend based on your objectives with Buzzin. Now you don’t have to individually negotiate with bloggers and influencers on compensation structure (which could be very subjective, if you ask around).</p>
					</div>
				</div>

				<div class="article">
					<div class="section-text">
						<h5 class="article-title">Business Benefits</h5>
						<p class="benefits">Live Data to Analyze</p><p>Find out how your campaigns are doing, anytime, anywhere with just one look at Buzzin’s dashboard.</p>
						<p class="benefits">Reports after each Campaign</p><p>Empower your influencer marketing strategies by getting automated reports after each campaign.</p>
						<p class="benefits">Sentiment Analysis</p><p>Know what your consumers say about your product in reaction to your influencers with just one subscription.</p>
					</div>
				</div>

				<div class="article-steps">
					<h6 class="article-title">Working as a Business on B2<span class="lowercase">I</span> is as easy as 1, 2, 3</h6>
					<p><strong>Step by Step Process:</strong></p>
					<div class="">
						<h6 class="b2i">SET UP YOUR CAMPAIGN</h6>
						<p>The key to a successful campaign is in setting the right objectives and parameters before you launch. You don’t want to waste your money by throwing it to the wind and praying it hits your mark.</p>
						<p>If it’s your first time setting up a campaign with us, Buzzin has pre-set packages you can customize for your specific needs. If you want to target reach, engagement, conversion, or all of the above for your specific niche, Buzzin’s advanced influencer catalogue system helps you tap the right kind of influencers to do just that by nominating them as the ones most aligned with your brand.</p>
					</div>
					<div class="">
						<h6 class="b2i">LAUNCH YOUR CAMPAIGN</h6>
						<p>Now that you’ve set your targets and objectives aka the hard part, the next part is super easy. Just give your campaign the go signal and watch Buzzin do its thing. You’ll be receiving live status updates about what influencers have joined your campaign, what they posted, and the statistics from their posts; roughly everything you need to calculate the ROI of your marketing campaign. All in real time.</p>
					</div>
					<div class="">
						<h6 class="b2i">GET REPORTS ABOUT YOUR CAMPAIGN</h6>
						<p>Finally your campaign’s done, and now it’s time to analyze what went right and what went wrong, with the help of Buzzin’s informative analysis summary. We scoured your data for insights that might prove to be helpful to you, from whether your campaign’s hashtags did better on Facebook, Instagram, or Twitter, to which influencer brought in the most engagement. Buzzin’s reports show you what you’re doing great, and what you can improve on in your next campaign, a first in the influencer marketing game.</p>
					</div>
				</div>

				<div class="section-start">
					<a href="{{ route('app.signup') }}" class="btn btn-large waves-effect waves-light orange">START A CAMPAIGN</a>
				</div>

			</div>
		</section>

	</div>
@stop

@section('footer')
	<script src="{{ config('s3.bucket_link') . elixir('assets/app/views/index.js') }}"></script>
	<script src="{{ config('s3.bucket_link') . elixir('assets/app/slick.min.js') }}"></script>

@stop
