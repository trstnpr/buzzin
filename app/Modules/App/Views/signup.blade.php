@extends('appformMaster')

@section('title')
	<title>Buzzin by Blogapalooza</title>
@stop

@section('meta')
	{{-- Put Seo tags Here... --}}
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/app/views/index.css') }}" rel="stylesheet">

	<style type="text/css">

		.section-signup .card-panel .tabs .tab a.active {
			color: #ed771b;
			font-weight: bold;
		}

		.section-signup .card-panel .tabs .indicator {
			background-color: #ed771b;
		}

		[type="checkbox"]:checked + label:before, [type="checkbox"]:checked + label:after,
		[type="checkbox"].filled-in:checked + label:before, [type="checkbox"].filled-in:checked + label:after
		 {
		    border-right: 2px solid #9c27b0;
		    border-bottom: 2px solid #9c27b0;
		}
		[type="checkbox"].filled-in:checked + label:after {
			border: 2px solid #9c27b0;
			background-color: #fff;
		}

.dropdown-content li > a, .dropdown-content li > span {
    font-size: 16px;
    color: #9c27b0;
}
	</style>
@stop

@section('content')

	<div class="signup-content">
		<section class="section-signup">
			<div class="container">
				<div class="row">
					<div class="col l6 offset-l3 m6 offset-m3 s12">

						<div class="card teal hide" id="result_div">
	                        <i class="material-icons">check</i> Signed Up in Successfully!
	                    </div>

						<div class="card-panel light-blue lighten-5">
							<a href="{{ route('app.index') }}">
								<img src="{{ config('s3.bucket_link') . elixir('images/assets/buzzinlogo2.png') }}" class="responsive-img brand-img" />
							</a>
							<h5 class="section-title">Sign Up</h5>

							<ul class="tabs">
								<li class="tab col s6"><a class="active" href="#business">business</a></li>
						        <li class="tab col s6"><a href="#influencer">influencer</a></li>
							</ul>

							<div class="tab-container" id="business">
								<form id="form_bus_registration">
									<div class="row">
										<input type="hidden" name="_token" value="{{ csrf_token() }}">
										<input type="hidden" name="role_id" value="3">
										<div class="input-field s12 card teal hide" id="result_div">
					                        <i class="material-icons">check</i> Signed Up in Successfully!
					                    </div>
										<div class="col s12">
											<div class="form-group">
												<input placeholder="Email Address" type="email" class="form-control signup-field validate" name="email" id="email" required>
									        </div>
									    </div>
									    <div class="col s12">
											<div class="form-group">
												<input placeholder="Company Name" type="text" class="form-control signup-field" name="company_name" required>
									        </div>
									    </div>
									    <div class="col s12">
										    <select class="material-select" multiple name="profile_category[]" id="pc">
										        <option value="" disabled selected>Choose your category</option>
										      	@foreach($category as $c)
				                                <option value="{{$c['category_id']}}">{{$c['category']}}</option>
				                                @endforeach
										    </select>
										</div>
									    <div class="col s6">
											<div class="form-group">
												<input placeholder="First Name" type="text" class="form-control signup-field" name="first_name" required>
									        </div>
									    </div>
									    <div class="col s6">
											<div class="form-group">
												<input placeholder="Last Name" type="text" class="form-control signup-field" name="last_name" required>
									        </div>
									    </div>
									    <div class="col m6 s12">
											<div class="form-group">
												<input placeholder="Password" type="password" class="form-control signup-field" name="password" id="password" required>
									        </div>
									    </div>
									    <div class="col m6 s12">
											<div class="form-group">
												<input placeholder="Confirm Password" type="password" class="form-control signup-field" name="password_confirmation" id="confirm_password" required>
									        </div>
									    </div>
									    <div class="col s12">
											<div class="form-group">
												<button class="purple btn col s12 waves-effect" type="submit" id="btn_business">Submit</button>
									        </div>
									    </div>
									    <div class="col s12">
											<div class="form-group">
												<p>
													<input type="checkbox" class="filled-in" id="filled-in-box" name="agree" required/>
													<label for="filled-in-box">I agree to Buzzin's <a href="#">Terms of Use</a> and <a href="#">Privacy Policy</a>.</label>
												</p>
									        </div>
									    </div>
								    </div>
								</form>
							</div>

    						<div class="tab-container" id="influencer">
    							<div class="row">
    								<div class="col s6">
    									<div class="form-group">
    										<a href="{{ route('login.auth.redirect', 'facebook') }}" class="btn btn-facebook col s12"><i class="fa fa-facebook" aria-hidden="true"></i> facebook</a>
    									</div>
    								</div>
    								<div class="col s6">
    									<div class="form-group">
    										<a href="{{ route('login.auth.redirect', 'google') }}" class="btn btn-google col s12"><i class="fa fa-google-plus" aria-hidden="true"></i> google</a>
    									</div>
    								</div>
    								<div class="col s6">
    									<div class="form-group">
    										<a href="{{ route('login.auth.redirect', 'twitter') }}" class="btn btn-twitter col s12"><i class="fa fa-twitter" aria-hidden="true"></i> twitter</a>
    									</div>
    								</div>
    								<div class="col s6">
    									<div class="form-group">
    										<a href="{{ route('login.auth.redirect', 'instagram') }}" class="btn purple accent-3 col s12"><i class="fa fa-instagram" aria-hidden="true" style="color: #fff"></i> instagram</a>
    									</div>
    								</div>
    							</div>

    							<h6 class="center">-- OR --</h6>

    							<form id="form_blog_registration">
	    							<div class="row">
	    								<input type="hidden" name="_token" value="{{ csrf_token() }}">
	    								<input type="hidden" name="role_id" value="2">
										<div class="col s12">
											<div class="form-group">
												<input placeholder="Email Address" type="email" class="form-control signup-field validate" name="email" required>
									        </div>
									    </div>
									    <div class="col s12">
										    <select class="material-select" multiple name="profile_category[]" id="pc">
										        <option value="" disabled selected>Choose your category</option>
											       @foreach($category as $c)
					                                <option value="{{$c['category_id']}}">{{$c['category']}}</option>
					                                @endforeach
										    </select>
										</div>
									    <div class="col s6">
											<div class="form-group">
												<input placeholder="First Name" type="text" class="form-control signup-field" name="first_name" required>
									        </div>
									    </div>
									    <div class="col s6">
											<div class="form-group">
												<input placeholder="Last Name" type="text" class="form-control signup-field" name="last_name" required>
									        </div>
									    </div>
									    <div class="col m6 s12">
											<div class="form-group">
												<input placeholder="Password" type="password" class="form-control signup-field" name="password" id="password1" required>
									        </div>
									    </div>
									    <div class="col m6 s12">
											<div class="form-group">
												<input placeholder="Confirm Password" type="password" class="form-control signup-field" name="password_confirmation" id="confirm_password1" required>
									        </div>
									    </div>
									    <div class="col s12">
											<div class="form-group">
												<button class="purple btn col s12 waves-effect" type="submit" id="btn_blog">Submit</button>
									        </div>
									    </div>
									    <div class="col s12">
											<div class="form-group">
												<p>
													<input type="checkbox" class="filled-in" id="filled-in-boxs" name="agree" required/>
													<label for="filled-in-boxs">I agree to Buzzin's <a href="#">Terms of Use</a> and <a href="#">Privacy Policy</a>.</label>
												</p>
									        </div>
									    </div>
								    </div>
							    </form>
    						</div>

						</div>

						<h6 class="center">Already have an account? <a href="{{ route('app.login') }}">Login Now</a></h6>

					</div>
				</div>
			</div>
		</section>
	</div>

@stop

@section('footer')
	<script src="{{ config('s3.bucket_link') . elixir('assets/app/views/index.js') }}"></script>
	<script src="{{ config('s3.bucket_link') . elixir('assets/user/views/register/index.js') }}"></script>

	<script type="text/javascript">
		$(document).ready(function() {
		    $('#pc').material_select();
		});
	</script>

@stop
