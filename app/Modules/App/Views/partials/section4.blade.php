
<section class="section4 parallax-container">
	<div class="parallax"><img src="{{ config('s3.bucket_link') . elixir('images/assets/team.jpg') }}"></div>
	<div class="container">
		<div class="card-panel center" data-aos="flip-left">
			<h6>ready to get started? sign up and follow us on social networks</h6>
			<a href="{{ route('app.signup') }}" class="btn btn-rounded btn-orange center-block"><i class="fa fa-check white-text left"></i>signup</a>
			<a href="https://www.facebook.com/blogapalooza" class="btn btn-rounded btn-facebook center-block"><i class="fa fa-facebook left"></i>facebook</a>
			<a href="https://twitter.com/blogapalooza" class="btn btn-rounded btn-twitter center-block"><i class="fa fa-twitter left"></i>twitter</a>
			<a href="https://www.instagram.com/blogapaloozaph/" class="btn btn-rounded btn-instagram center-block"><i class="fa fa-instagram left"></i>instagram</a>
		</div>
	</div>
</section>
