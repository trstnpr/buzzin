
<section class="section3">
	<div class="container">
		<h4 class="section-title">WHY SIGN UP?</h4>
		<p class="title-pretext">Be part of a whole new world! By being a part of Buzzin, you get instant<br> access to one of the most interesting and constantly growing<br> communities here in the Philippines</p>

		<div class="row benefits">
			<div class="benefits-header">
				<h5 class="center b2i as-a">AS A BUSINESS</h5>
			</div>
			<div class="col l4 m4 s12">
				<div class="item center-block">
					<img class="responsive-img center-block item-img" src="{{ config('s3.bucket_link') . elixir('images/assets/data.png') }}" data-aos="fade-down" /><br/>
					<div class="item-text">
						<h6 class="item-caption">live data to analyze</h6>
						<p class="item-description">Find out how your campaigns are doing, anytime, anywhere with just one look at B2i’s dashboard.</p>
					</div>
				</div>
			</div>

			<div class="col l4 m4 s12">
				<div class="item center-block">
					<img class="responsive-img center-block item-img" src="{{ config('s3.bucket_link') . elixir('images/assets/report.png') }}" data-aos="fade-down" /><br/>
					<div class="item-text">
						<h6 class="item-caption">reports after each campaign</h6>
						<p class="item-description">Empower your influencer marketing strategies by getting automated reports after each campaign.</p>
					</div>
				</div>
			</div>

			<div class="col l4 m4 s12">
				<div class="item center-block">
					<img class="responsive-img center-block item-img" src="{{ config('s3.bucket_link') . elixir('images/assets/talk.png') }}" data-aos="fade-down" /><br/>
					<div class="item-text">
						<h6 class="item-caption">sentiment analysis</h6>
						<p class="item-description">Know what your consumers say about your product in reaction to your influencers with just one subscription.</p>
					</div>
				</div>
			</div>
		</div>
		<div class="row benefits">
			<div class="benefits-header">
				<h5 class="center b2i as-a">AS AN INFLUENCER</h5>
			</div>
			<div class="col l4 m4 s12">
				<div class="item center-block">
					<img class="responsive-img center-block item-img" src="{{ config('s3.bucket_link') . elixir('images/assets/work.png') }}" data-aos="fade-down" /><br/>
					<div class="item-text">
						<h6 class="item-caption">work with new brands</h6>
						<p class="item-description">Browse through campaigns looking for their long term influencer partner, and discover new brands and new topics to create content for.</p>
					</div>
				</div>
			</div>

			<div class="col l4 m4 s12">
				<div class="item center-block">
					<img class="responsive-img center-block item-img" src="{{ config('s3.bucket_link') . elixir('images/assets/promote.png') }}" data-aos="fade-down" /><br/>
					<div class="item-text">
						<h6 class="item-caption">expand your audience</h6>
						<p class="item-description">Create content that can promote and help you grow your niche and expertise into topics you never would have thought of.</p>
					</div>
				</div>
			</div>

			<div class="col l4 m4 s12">
				<div class="item center-block">
					<img class="responsive-img center-block item-img" src="{{ config('s3.bucket_link') . elixir('images/assets/money.png') }}" data-aos="fade-down" /><br/>
					<div class="item-text">
						<h6 class="item-caption">earn money from every post</h6>
						<p class="item-description">Get paid to create content on your terms, without compromising the brands you work with.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
