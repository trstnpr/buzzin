
<section class="banner-head parallax-container valign-wrapper">
	<div class="parallax"><img src="{{ config('s3.bucket_link') . elixir('images/assets/header-image.jpg') }}"></div>
	<div class="container">
		<div class="banner-text">
			<h5><br></h5>
			<p class="tag-line" data-aos="fade-left" style="font-size: 30px; float: right; padding-left: 50%;">THE PHILIPPINES' PREMIER AND MOST TRUSTED INFLUENCER MARKETING PARTNER</p>
			<div class="action-button" data-aos="zoom-in-left">
				<a href="{{ route('app.signup') }}"><button class="waves-effect btn-floating purple">START A CAMPAIGN</button></a>
				<a href="{{ route('app.signup') }}"><button class="waves-effect btn-floating orange" href="{{ route('app.signup') }}">&nbsp;JOIN A CAMPAIGN</button></a>
			</div>
		</div>
	</div>
</section>
