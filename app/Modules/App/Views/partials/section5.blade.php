
<section class="section5">
	<div class="container ">
		<h4 class="section-title">WHAT'S BUZZIN</span></h4>

		{{-- <div class="countdown-timer" id="clockdiv">
			<div class="row">
				<div class="col l3 m3 s6">
					<div class="item-time circle center" data-aos="zoom-in">
						<h3 class="item-value" id="day"></h3>
						<strong>Days</strong>
					</div>
				</div>
				<div class="col l3 m3 s6">
					<div class="item-time circle center" data-aos="zoom-in">
						<h3 class="item-value" id="hour"></h3>
						<strong>Hours</strong>
					</div>
				</div>
				<div class="col l3 m3 s6">
					<div class="item-time circle center" data-aos="zoom-in">
						<h3 class="item-value" id="minute"></h3>
						<strong>Minutes</strong>
					</div>
				</div>
				<div class="col l3 m3 s6">
					<div class="item-time circle center" data-aos="zoom-in">
						<h3 class="item-value" id="second"></h3>
						<strong>Seconds</strong>
					</div>
				</div>
			</div>
		</div> --}}

		<?php
			$campaigns = App\Modules\Campaign\Models\Campaign::with('campaign_image')->where('status', '=', 1)->count();
			$business = App\Modules\User\Models\User::with('profile')->where('role_id', '=', 3)->count();
			$influencer = App\Modules\User\Models\User::with('profile')->where('role_id', '=', 2)->count();
		?>
		<div class="stat-count">
			<div class="row">
				<div class="col l4 m4 s12">
					<div class="item circle amber valign-wrapper" data-aos="zoom-in">
						<div class="center-block center">
							<h1 class="item-value" id="day">{{ $business }}</h1>
							<strong>Businesses</strong>
						</div>
					</div>
				</div>
				<div class="col l4 m4 s12">
					<div class="item circle amber valign-wrapper" data-aos="zoom-in">
						<div class="center-block center">
							<h1 class="item-value" id="day">{{ $campaigns }}</h1>
							<strong>Campaigns</strong>
						</div>
					</div>
				</div>
				<div class="col l4 m4 s12">
					<div class="item circle amber valign-wrapper" data-aos="zoom-in">
						<div class="center-block center">
							<h1 class="item-value" id="day">{{ $influencer }}</h1>
							<strong>Influencers</strong>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="call-to-action center" style="padding: 20px;" data-aos="fade-down">
			<a href="{{ route('app.signup') }}" class="btn btn-rounded purple center-block">START A CAMPAIGN</a>
			<a href="{{ route('app.signup') }}" class="btn btn-rounded orange center-block">JOIN A CAMPAIGN</a>
		</div>

	</div>
</section>
