
<section class="section2">
	<div class="container">
		<h4 class="section-title">WHAT IS <span class="b2i">BUZZIN</span></h4>

		<div class="row">
			<div class="col l8 m7 s12">

				<div class="section-text">
					<p>Buzzin connects businesses to influencers by using a high technology system that enables streamlined matching, objective measurement of key performance indicators, and monetization of content.</p>
					<p>We compare and measure hits, reach, and engagement among all content creators across all campaigns. Buzzin is  a search trawler that figures out what you write about, what niche interests you the most, and what your stance is. Then it matchmakes you, our favorite Blogger/Influencer, with your perfect Business partner.</p>
				</div>

			</div>
			<div class="col l4 m7 s12">

				<div class="section-list">
					<ul>
						<li>attend exclusive events</li>
						<li>earn through partnership</li>
						<li>grow your influence</li>
						<li>network with us</li>
					</ul>
				</div>

			</div>
		</div>

		<h4 class="section-title">WHAT IS <span class="b2i">BLOGAPALOOZA</span></h4>

		<div class="row">
			<div class="col l12 m12 s12">

				<div class="section-text">
					<p>Blogapalooza Inc. is the trailblazer of influencer marketing in the Philippines. Established in 2011 by the founders of WhenInManila.com, the company hosts grand annual networking and marketing events called “Blogapalooza”, and through the combination of interactive booths, hyper specialized talks by industry leaders, and its large network of bloggers, online influencers, and digital superstars, the company has reinvented how blogger and influencer relations are done.</p>
					<p>With the introduction of Buzzin, Blogapalooza Inc. is expanding its horizons and teaching everyone how influencer marketing should be done.</p>
				</div>

			</div>


	</div>
</section>
