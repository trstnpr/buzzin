@extends('appformMaster')

@section('title')
	<title>Buzzin by Blogapalooza</title>
@stop

@section('meta')
	{{-- Put Seo tags Here... --}}
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/app/views/index.css') }}" rel="stylesheet">
	<style type="text/css">
		* {
			color: #f2f2f2;
		}

		h5, input[type=password], input[type=email], label, span, h6 {
			color: #4a4a4a !important;
		}

		.section-signup .card-panel .tabs .tab a.active {
			color: #ed771b;
			font-weight: bold;
		}

		.section-signup .card-panel .tabs .indicator {
			background-color: #ed771b;
		}
		.card-panel {
			padding-bottom: 0px !important;
		}

		[type="checkbox"]:checked + label:before, [type="checkbox"]:checked + label:after,
		[type="checkbox"].filled-in:checked + label:before, [type="checkbox"].filled-in:checked + label:after
		 {
		    border-right: 2px solid #9c27b0;
		    border-bottom: 2px solid #9c27b0;
		}
		[type="checkbox"].filled-in:checked + label:after {
			border: 2px solid #9c27b0;
			background-color: #fff;
		}
	</style>
@stop

@section('content')

	<div class="signup-content">
		<section class="section-signup">
			<div class="container">
				<div class="row">
					<div class="col l4 offset-l4 m6 offset-m3 s12">
						<div class="card teal hide" id="result_div">
	                        <i class="material-icons">check</i> Login Successfully!
	                    </div>
						<div class="card-panel light-blue lighten-5">
							<a href="{{ route('app.index') }}">
								<img src="{{ config('s3.bucket_link') . elixir('images/assets/buzzinlogo2.png') }}" class="responsive-img brand-img" />
							</a>

							<!-- <h5 class="section-title">Login</h5> -->

							<ul class="tabs">
								<li class="tab col s6"><a class="active" href="#business">business</a></li>
						        <li class="tab col s6"><a href="#influencer">influencer</a></li>
							</ul>

							<div class="tab-container" id="business">
								<form id="form_bus_login">
									<div class="row">
										<input type="hidden" name="_token" value="{{ csrf_token() }}">
										<div class="col s12">
											<div class="form-group">
												<input placeholder="Email Address" type="email" class="form-control signup-field validate" name="email" required="required">
									        </div>
									    </div>
									    <div class="col s12">
											<div class="form-group">
												<input placeholder="Password" type="password" class="form-control signup-field" name="password" id="password" required="required">
									        </div>
									    </div>
									    <div class="col s12">
											<div class="form-group">
												<button class="purple btn col s12 waves-effect" type="submit" id="btn_bus">Submit</button>
									        </div>
									    </div>
									    <div class="col s12">
											<div class="form-group">
												<p>
													<input type="checkbox" class="filled-in" id="filled-in-box"/>
													<label for="filled-in-box">Remember me</label>
													<p class="right"><a href="#forgot_password_modal" data-toggle="modal" data-target="#forgot_password_modal" onclick="forgotPassword()">Forgot Password?</a></p>
												</p>

									        </div>
									    </div>
								    </div>
								</form>
							</div>

    						<div class="tab-container" id="influencer">
    							<div class="row">
    								<div class="col s6">
    									<div class="form-group">
    										<a href="{{ route('login.auth.redirect', 'facebook') }}" class="btn btn-facebook col s12" style="font-size: 13px;"><i class="fa fa-facebook" aria-hidden="true"></i> facebook</a>
    									</div>
    								</div>
    								<div class="col s6">
    									<div class="form-group">
    										<a href="{{ route('login.auth.redirect', 'google') }}" class="btn btn-google col s12" style="font-size: 13px;"><i class="fa fa-google-plus" aria-hidden="true"></i> google</a>
    									</div>
    								</div>
    								<div class="col s6">
    									<div class="form-group">
    										<a href="{{ route('login.auth.redirect', 'twitter') }}" class="btn btn-twitter col s12" style="font-size: 13px;"><i class="fa fa-twitter" aria-hidden="true"></i> twitter</a>
    									</div>
    								</div>
    								<div class="col s6">
    									<div class="form-group">
    										<a href="{{ route('login.auth.redirect', 'instagram') }}" class="btn purple accent-3 col s12" style="font-size: 13px;"><i class="fa fa-instagram" aria-hidden="true"></i> instagram</a>
    									</div>
    								</div>
    							</div>

    							<h6 class="center">-- OR --</h6>

    							<form id="form_blog_login">
    								<input type="hidden" name="_token" value="{{ csrf_token() }}">
	    							<div class="row">
										<div class="col s12">
											<div class="form-group">
												<input placeholder="Email Address" type="email" class="form-control signup-field validate" name="email" required="required">
									        </div>
									    </div>
									    <div class="col s12">
											<div class="form-group">
												<input placeholder="Password" type="password" class="form-control signup-field" name="password" id="password1" required="required">
									        </div>
									    </div>

									    <div class="col s12">
											<div class="form-group">
												<button class="purple btn col s12 waves-effect" type="submit" id="btn_blog">Submit</button>
									        </div>
									    </div>
									    <div class="col s12">
											<div class="form-group">
												<p>
													<input type="checkbox" class="filled-in" id="filled-in-boxs" />
													<label for="filled-in-boxs">Remember me</label>
													<p class="right"><a href="#forgot_password_modal" data-toggle="modal" data-target="#forgot_password_modal" onclick="forgotPassword()">Forgot Password?</a></p>
												</p>
									        </div>
									    </div>
								    </div>
								</form>
    						</div>

						</div>

						<h6 class="center">No account yet? <a href="{{ route('app.signup') }}">Signup Now</a></h6>

					</div>
				</div>
			</div>
		</section>
	</div>

<!-- Forgot Password Modal -->
<div id="forgot_password_modal" class="modal forgot_password_modal">
    <div class="modal-content">
        <form id="forgot_password_form" class="col s12">
        	<input type="hidden" id="_tokens" value="{{ csrf_token() }}">
            <div class="div_notif"></div>
            <div class="row">
                <h4 class="grey-text">Forgot Password?</h4>
                <div class="input-field col s12 m12">
                  <input id="emails" type="email" class="validate" required="required" name="email" aria-required="true">
                  <label for="email" data-error="Invalid">Enter Email</label>
                </div>
            </div>

                <div class="input-field col s12">
                    <button class="waves-effect waves-light btn btn-block orange accent-3" id="btn-reset-password" type="submit">Reset</button>
                </div>
                <div class="input-field col s12 m12" style="padding-bottom: 30px;">
                    <a href="#" class="waves-effect waves-white btn purple modal-action modal-close right freakin-modal">Close</a>
                </div>
        </form>
    </div>
</div>

@stop

@section('footer')
	<script src="{{ config('s3.bucket_link') . elixir('assets/app/views/index.js') }}"></script>
	<script src="{{ config('s3.bucket_link') . elixir('assets/user/views/login/index.js') }}"></script>

	<script>
		function forgotPassword(){
			$('#forgot_password_modal').modal('open');
		}
	</script>

	@if(session('account_activated'))
	    <script type="text/javascript">

	        $(document).ready(function(){
	            Materialize.toast('Hooray! Your account is now activated.', 12000);
	        });

	    </script>
	@endif

	@if(session('account_activated_already'))

	    <script type="text/javascript">

	        $(document).ready(function(){
	            Materialize.toast('Oops! Your account is already activated.', 12000);
	        });

	    </script>

	@endif


@stop
