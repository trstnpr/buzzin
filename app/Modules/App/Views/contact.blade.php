@extends('appMaster')

@section('title')
	<title>Buzzin - Contact Us</title>
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/app/views/index.css') }}" rel="stylesheet">
@stop

@section('content')

	<div class="contact-page">

		<section class="contact-content">
			<div class="container">
				<h4 class="section-title">&nbsp;CONTACT <span class="b2i">US</span></h4>
				<p>&nbsp;&nbsp;For inquiries, comments, questions & suggestions you can contact us. </p>
				<form id="form_contact_us">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="row">
						<div class="col l7 m7 s12">
							<div class="col l12 m12 s12">
								<div class="form-group">
									<label>Name</label>
									<input type="text" class="form-control contact-field" name="name" required="" />
								</div>
							</div>
							<div class="col l12 m12 s12">
								<div class="form-group">
									<label>Email</label>
									<input type="email" class="form-control contact-field validate" name="email_user" required="" />
								</div>
							</div>
							<div class="col l12 m12 s12">
								<div class="form-group">
									<label>Contact Number</label>
									<input type="text" class="form-control contact-field" name="contact" required="" />
								</div>
							</div>
							<div class="col l12 m12 s12">
								<div class="form-group">
									<label>Subject</label>
									<input type="text" class="form-control contact-field" name="subject" required="" />
								</div>
							</div>
							<div class="col l12 m12 s12">
								<div class="form-group">
									<label>I am a</label>
									<select class="form-control contact-field" name="i_am" required="">
										<option value="Blogger">Blogger</option>
										<option value="Business">Business</option>
									</select>
								</div>
							</div>
							<div class="col l12 m12 s12">
								<div class="form-group">
									<label>Interested in</label>
									<select class="form-control contact-field" name="interested_in" required="">
										<option disabled selected>Choose Category</option>
										@foreach($category as $c)
					                    <option value="{{$c['category']}}">{{$c['category']}}</option>
					                    @endforeach
									</select>
								</div>
							</div>
							<div class="col l12 m12 s12">
								<div class="form-group">
									<label>Message</label>
									<textarea class="form-control textarea-contact-field" name="message" required=""></textarea>
								</div>
							</div>
							<div class="col l12 s12">
								<div class="form-group">
									<button class="btn col s12" type="submit" id="btn_contact">Submit</button>
								</div>
						    </div>
					    </div>
					    <!-- ADDRESS, SOCIAL MEDIA & MAP -->
					    <div class="col l5 m5 s12 location-content">
							<div class="">
								<h4 class="section-title">Location</h4>

								<ul class="address">
									<li>Unit 10-1 10/F One Global Place</li>
									<li>25th St. cor. 5th Ave.</li>
									<li>Bonifacio Global City, Taguig City 1634</li>
									<li>Philippines</li>
								</ul>
							</div>
							<div class="">
								<h4 class="section-title"><span class="b2i">Follow Us</span></h4>
								<br/>
								<ul class="social-btn">
									<li><a href="https://www.facebook.com/blogapalooza"><i class="mdi mdi-facebook"></i></a></li>
									<li><a href="https://twitter.com/blogapalooza"><i class="mdi mdi-twitter"></i></a></li>
									<li><a href="https://www.instagram.com/blogapaloozaph/"><i class="mdi mdi-instagram"></i></a></li>
								</ul>
							</div>

							<div class="b2i-map">
								<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3861.872472703343!2d121.04738842327878!3d14.549284460872746!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x2fd54c517460be9e!2sOne+Global+Place!5e0!3m2!1sen!2sph!4v1477563691391" width="100%" height="250" frameborder="0" style="border:0;margin:0;" allowfullscreen></iframe>
							</div>
						</div>
					</div>
				</form>
			</div>
		</section>
	</div>

@stop

@section('footer')
	<script src="{{ config('s3.bucket_link') . elixir('assets/app/views/index.js') }}"></script>
	<script src="{{ config('s3.bucket_link') . elixir('assets/app/views/contactus/index.js') }}"></script>

@stop
