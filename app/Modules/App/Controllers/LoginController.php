<?php namespace App\Modules\App\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\ForgotPasswordRequest;
use App\Modules\User\Models\User;
use App\Modules\User\Models\UserProfile;
use Auth;
use App\Services\MailSender;

class LoginController extends Controller 
{   
    /**
     * @var Auth
     */
    private $auth;

    /**
     * Login controller instance.
     *
     * @return void
     */

	public function __construct(Auth $auth)
    {
        $this->auth = $auth;
	}

	/**
     * Display Login page.
     *
     * @return view
     */
    public function index()
    {
        return view('App::login');
    }

    /**
     * Authenticate user credentials.
     *
     * @param LoginRequest $login_request
     * @return view
     */
    public function authenticate(LoginRequest $login_request)
    {
        if (!Auth::attempt(['email' => $login_request->email, 'password' => $login_request->password, 'status' => 1])) {
            $user = User::where('email', '=', $login_request->email)->first();
            if($user){
                if($user->status == 0){
                    return json_encode(array('result' => 'error', 'message' => 'Your account is not activated yet! Please check your inbox for the activation email.'));
                }
            }
            return json_encode(array('result' => 'error', 'message' => 'Invalid email or password.'));
        }
        return json_encode(array('result' => 'success', 'message' => $this->getUserRole(Auth::user()->role_id) . '/dashboard'));
    }

    /**
     * Password reset
     *
     * @param FogatPasswordRequerst $request
     * @param MailSender $mailSender
     * @return Response
     */
    public function reset(ForgotPasswordRequest $request, MailSender $mailSender)
    {
        $user_data = User::where('email', $request->email)->first();
        $user_profile = UserProfile::where('user_id', $user_data['id'])->first();

        if (!$user_data) {
            return json_encode(array('result' => 'error', 'message' => 'Failed! User does not exist.'));
        }

        $data = [
            'temp_password' => $this->_generate_temporary_password(),
            'email' => $request->email,
            'first_name' => $user_profile['first_name'],
            'last_name' => $user_profile['last_name']
        ];

        User::where('email', $request->email)->update(['password' => bcrypt($data['temp_password'])]);

        if ($mailSender->send('email.reset_password', 'Reset Password Request', $data)) {
            return json_encode(array('result' => 'success', 'message' => 'Success! New password has been sent to your email.'));
        }

        return json_encode(array('result' => 'error', 'message' => 'Failed! There is an error occured while sending. Please try again.'));
    }

    /**
     * Logout user.
     * @return Redirect
     */
    public function logout()
    {   
        Auth::logout();
        return redirect('/login');
    }

    /**
     * Generate temporary password.
     *
     * @return string
     */
    private function _generate_temporary_password()
    {
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZzxcvbnmasdfghjklqwertyuiop';
        // generate a pin based on 2 * 7 digits + a random character
        $pin = mt_rand(100, 9999) . mt_rand(100, 9999) . $characters[rand(0, strlen($characters) - 1)];
        // shuffle the result
        return str_shuffle($pin);
    }
}
