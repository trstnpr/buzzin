<?php namespace App\Modules\App\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests\ContactUsRequest;
use App\Modules\User\Models\Category;
use App\Services\MailSender;

class ContactUsController extends Controller 
{   
	/**
     * Display Contact Us Page.
     *
     * @return view
     */
    public function index()
    {   
        $data['category'] = Category::select('category_id', 'category')->get();
        return view("App::contact", $data);
    }

    /**
     * Submit Contact Form
     *
     * @param LoginRequest $login_request
     * @return view
     */
    public function contactUs(ContactUsRequest $request, MailSender $mailSender)
    {    
        $request->merge(['first_name' => $request->name, 'last_name' => '', 'email' => 'corsinoronalyn@gmail.com' /*'info@teachat.co'*/]);

        if ($mailSender->send('email.contact_us', 'Contact Us from b2i.devhub.ph', $request->all())) {
            return json_encode(array('result' => 'success', 'message' => 'Thank you for contacting us. Your message has been sent.'));
        }

        return json_encode(array('result' => 'error', 'message' => 'Failed! There is an error occured while sending. Please try again.'));

    }

}
