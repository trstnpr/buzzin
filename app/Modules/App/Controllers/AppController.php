<?php namespace App\Modules\App\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\Modules\Company\Models\Company;
use App\Modules\Influencer\Models\SocialMedia;
use App\Modules\User\Models\Category;
use App\Modules\Campaign\Models\Campaign;
use App\Modules\User\Models\ProfileCategory;
use App\Modules\User\Models\User;
use App\Modules\User\Models\UserProfile;
use App\Modules\Influencer\Models\InfluencerCampaign;
use App\Services\MailSender;
use Auth;
use Carbon\Carbon;
use Hash;
use Illuminate\Http\Request;
use Socialite;
use Cookie;

class AppController extends Controller
{
    /**
     * Redirect the user to the Social Media authentication page.
     *
     * @return Response
     */
    public function redirectToProvider($provider)
    {
        if ($provider == 'facebook') {
            return Socialite::driver($provider)->fields([
                'first_name', 'last_name', 'email', 'gender', 'birthday',
            ])->scopes([
                'email', 'user_birthday',
            ])->redirect();
        }

        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from Social Media.
     *
     * @return Response
     */
    public function handleProviderCallback($provider)
    {
        if ($provider == 'facebook') {
            $user = Socialite::driver($provider)->fields([
                'first_name', 'last_name', 'email', 'gender', 'birthday',
            ])->user();
        } else {
            $user = Socialite::driver($provider)->user();
        }

        /*CHECK IF LOGIN*/
        if (Auth::check()) {

            /*CHECK IF THE ACCOUNT IS EXISTING IN THE SOCIAL MEDIA ACCOUNT MODULE*/
            if (!($social = SocialMedia::where(array('provider_name' => $provider, 'provider_id' => $user->id))->first())) {
                $profile_id = UserProfile::where('user_id', '=', Auth::user()->id)->value('profile_id');
                SocialMedia::create(['profile_id' => $profile_id, 'provider_name' => $provider, 'provider_id' => $user->id, 'provider_key' => $user->id, 'profile_url' => null]);
            }
            return redirect(route('influencer.social.index'));

        } else {

            /*CHECK IF THE ACCOUNT IS EXISTING IN THE SOCIAL MEDIA ACCOUNT MODULE*/
            if (!($social = SocialMedia::where(array('provider_name' => $provider, 'provider_id' => $user->id))->first())) {

                if ($provider == 'facebook') {

                    if ($result = User::create(['email' => $user->email, 'password' => Hash::make($user->email), 'role_id' => 2, 'status' => 1])) {
                        if ($result2 = UserProfile::create(['user_id' => $result->id, 'first_name' => $user->user['first_name'], 'last_name' => $user->user['last_name']])) {
                            SocialMedia::create(['profile_id' => $result2->profile_id, 'provider_name' => $provider, 'provider_id' => $user->id, 'provider_key' => $user->id, 'profile_url' => null]);
                            if (Auth::loginUsingId($result->id)) {
                                //return json_encode(array('result' => 'success', 'message' => 'Success!', 'redirect' => route('influencer.dashboard')));
                                return redirect(route('influencer.dashboard'));
                            }
                        }
                        //return json_encode(array('result' => 'error', 'message' => 'Error in Profile Table!'));
                        return redirect(route('app.login'));
                    }
                    //return json_encode(array('result' => 'error', 'message' => 'Error in User Table!'));
                    return redirect(route('app.login'));

                } else if ($provider == 'twitter') {

                    if ($result = User::create(['email' => $user->email, 'password' => Hash::make($user->email), 'role_id' => 2, 'status' => 1])) {
                        if ($result2 = UserProfile::create(['user_id' => $result->id, 'first_name' => $user->name, 'last_name' => null])) {
                            SocialMedia::create(['profile_id' => $result2->profile_id, 'provider_name' => $provider, 'provider_id' => $user->id, 'provider_key' => $user->id, 'profile_url' => null]);
                            if (Auth::loginUsingId($result->id)) {
                                //return json_encode(array('result' => 'success', 'message' => 'Success!', 'redirect' => route('influencer.dashboard')));
                                return redirect(route('influencer.dashboard'));
                            }
                        }
                        //return json_encode(array('result' => 'error', 'message' => 'Error in Profile Table!'));
                        return redirect(route('app.login'));
                    }
                    //return json_encode(array('result' => 'error', 'message' => 'Error in User Table!'));
                    return redirect(route('app.login'));

                } else if ($provider == 'google') {

                    if ($result = User::create(['email' => $user->email, 'password' => Hash::make($user->email), 'role_id' => 2, 'status' => 1])) {
                        if ($result2 = UserProfile::create(['user_id' => $result->id, 'first_name' => $user->name, 'last_name' => null])) {
                            SocialMedia::create(['profile_id' => $result2->profile_id, 'provider_name' => $provider, 'provider_id' => $user->id, 'provider_key' => $user->id, 'profile_url' => null]);
                            if (Auth::loginUsingId($result->id)) {
                                //return json_encode(array('result' => 'success', 'message' => 'Success!', 'redirect' => route('influencer.dashboard')));
                                return redirect(route('influencer.dashboard'));
                            }
                        }
                        //return json_encode(array('result' => 'error', 'message' => 'Error in Profile Table!'));
                        return redirect(route('app.login'));
                    }
                    //return json_encode(array('result' => 'error', 'message' => 'Error in User Table!'));
                    return redirect(route('app.login'));
                }

            } else {

                $profile = UserProfile::where(['profile_id' => $social->profile_id])->first();

                if (Auth::loginUsingId($profile->user_id)) {
                    //return json_encode(array('result' => 'success', 'message' => 'Success!', 'redirect' => route('influencer.dashboard')));
                    return redirect(route('influencer.dashboard'));
                }

            }
        }
    }

    public function handleInstagramCallback()
    {
        $user = Socialite::driver('instagram')->user();
        if ($result = User::create(['email' => $user->email, 'password' => Hash::make($user->email), 'role_id' => 2, 'status' => 1])) {
            if ($result2 = UserProfile::create(['user_id' => $result->id, 'first_name' => $user->name, 'last_name' => null])) {
                SocialMedia::create(['profile_id' => $result2->profile_id, 'provider_name' => 'instagram', 'provider_id' => $user->id, 'provider_key' => $user->id, 'profile_url' => null]);
                if (Auth::loginUsingId($result->id)) {
                    //return json_encode(array('result' => 'success', 'message' => 'Success!', 'redirect' => route('influencer.dashboard')));
                    return redirect(route('influencer.dashboard'));
                }
            }
            //return json_encode(array('result' => 'error', 'message' => 'Error in Profile Table!'));
            return redirect(route('app.login'));
        }
        //return json_encode(array('result' => 'error', 'message' => 'Error in User Table!'));
        return redirect(route('app.login'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view("App::index");
    }

    public function signup()
    {
        $data['category'] = Category::select('category_id', 'category')->get();
        return view("App::signup", $data);
    }

    public function login()
    {
        return view("App::login");
    }

    public function about()
    {
        return view("App::about");
    }

    public function business()
    {
        return view("App::business");
    }

    public function influencer()
    {
        return view("App::influencer");
    }

    /**
     * Create a new user.
     *
     * @param RegisterRequest $request
     * @return view
     */
    public function register(RegisterRequest $request, MailSender $mailSender)
    {
        //dd($request->all());

        $verification_code = $this->_generate_verification_code();

        $request->merge(['password' => bcrypt($request->password), 'verification_code' => $verification_code]);

        $data_user = [
            'email' => $request->email,
            'password' => $request->password,
            'verification_code' => $verification_code,
            'role_id' => $request->role_id,
            'status' => 0,
            'created_at' => Carbon::now(),
        ];

        $user = User::insertGetId($data_user);

        $data_profile = [
            'user_id' => $user,
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'created_at' => Carbon::now(),
        ];

        /*if($request->role_id == 2){
        $data['organization_name'] = $request->organization_name;
        $data_profile['about_blogger'] = $request->about_blogger;
        $data_profile['blogger_pricing'] = $request->blogger_pricing;
        $data_profile['website'] = $request->website;
        }*/

        if ($user) {
            if ($user_profile = UserProfile::insertGetId($data_profile)) {

                $profile_category = $request->profile_category;

                if ($profile_category != null) {
                    foreach ($profile_category as $pc) {
                        $dataPersonal_category = ['profile_id' => $user_profile, 'category_id' => $pc];
                        $insert_Personal_category = ProfileCategory::insert($dataPersonal_category);
                    }
                }

                if ($request->role_id == 3) {
                    $data_company = ['profile_id' => $user_profile, 'company_name' => $request->company_name];
                    Company::create($data_company);
                }

                $request->merge(['link' => env('APP_URL') . '/signup/activate/' . $verification_code]);

                $mailSender->send('email.email_confirmation', 'Email Confirmation', $request->all());

                return json_encode(array('result' => 'success', 'message' => 'Registration Successful. Please check your email inbox to verify your account. <br> Also please check your SPAM folder if you don\'t see it in your inbox.'));
            }

            return json_encode(array('result' => 'error', 'message' => 'There\'s an error encountered while saving'));

        }
    }

    /**
     * Generate new verification code.
     *
     * @return string
     */
    private function _generate_verification_code()
    {
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZzxcvbnmasdfghjklqwertyuiop';

        $pin = mt_rand(10, 9999) . mt_rand(10, 9999) . $characters[rand(0, strlen($characters) - 1)];

        return str_shuffle($pin);
    }

    /**
     * Activate user account.
     *
     * @param string $verification_code
     * @return view
     */
    public function activate($verification_code)
    {
        if ($user = User::where(['verification_code' => $verification_code, 'status' => 0])->update(['status' => 1])) {

            return redirect('login')->with('account_activated', 'Your account is now activated.');
        }

        return redirect('login')->with('account_activated_already', 'Your account is already activated.');
    }

    /**
     * Campaign Views.
     *
     * @return string
     */
    public function view_counts($c_view)
    {   
        return redirect('/campaign/view/'.$c_view);
    }

    /**
     * Campaign Details.
     *
     * @return string
     */
    public function campaignDetail($c_view)
    {   
        $inf_campaign = InfluencerCampaign::join('campaign', 'influencer_campaign.campaign_id', '=', 'campaign.campaign_id')
                            ->where('influencer_campaign.generated_url', $c_view)
                            ->first();

        $value = Cookie::get('campaign');

        if($value == $c_view){
            return redirect($inf_campaign['campaign_url']);
        }
        else{
            $url = str_replace(' ','%20',$c_view);
            $path = '/campaign/view/'.$url;

            $campaign = InfluencerCampaign::where('generated_url', $c_view)->increment('view_count');
            $queue = Cookie::queue('campaign', $c_view, 100000, $path);

            return redirect($inf_campaign['campaign_url']);
        }
    }

    /*public function increment($c_view)
    {   
        $campaign = InfluencerCampaign::where('generated_url', $c_view)->increment('view_count');
    }*/
}
