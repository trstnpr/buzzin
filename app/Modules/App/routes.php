<?php

Route::group(array('module' => 'App', 'namespace' => 'App\Modules\App\Controllers', 'middleware' => ['web']), function () {

    /*Social Auth*/
    Route::get('login/auth/{provider}', ['uses' => 'AppController@redirectToProvider', 'as' => 'login.auth.redirect']);
    Route::get('login/auth/{provider}/callback', ['uses' => 'AppController@handleProviderCallback', 'as' => 'login.auth.callback']);

    Route::get('instagram', ['uses' => 'AppController@handleInstagramCallback', 'as' => 'login.auth.instagram']);
    /*Landing Page*/
    Route::get('/', ['uses' => 'AppController@index', 'as' => 'app.index']);

    // Campaign Counter
    Route::get('/campaign/{c_view}', 'AppController@view_counts');
    Route::get('/campaign/view/{c_view}', 'AppController@campaignDetail');
    /*Route::get('/campaign/add/{c_view}', 'AppController@increment');*/

    /*Pages*/
    Route::get('/business', ['uses' => 'AppController@business', 'as' => 'app.business']);
    Route::get('/influencer', ['uses' => 'AppController@influencer', 'as' => 'app.influencer']);
    Route::get('/about', ['uses' => 'AppController@about', 'as' => 'app.about']);
    Route::get('/contactus', ['uses' => 'ContactUsController@index', 'as' => 'app.contact']);

    /*Register User*/
    Route::get('/signup', ['uses' => 'AppController@signup', 'as' => 'app.signup', 'middleware' => 'guest']);
    Route::post('/signup/register', ['uses' => 'AppController@register', 'as' => 'app.signup.process']);
    Route::get('/signup/activate/{verification_code}', ['uses' => 'AppController@activate', 'as' => 'app.activate']);

    /*Login User*/
    Route::get('login', ['uses' => 'AppController@login', 'as' => 'app.login', 'middleware' => 'guest']);
    Route::post('login/authenticate', ['uses' => 'LoginController@authenticate', 'as' => 'app.login.process']);
    Route::get('logout', ['middleware' => 'auth', 'uses' => 'LoginController@logout', 'as' => 'app.logout']);

    Route::post('/password/reset', ['uses' => 'LoginController@reset', 'as' => 'app.password.reset']);
    Route::post('/contactus/store', ['uses' => 'ContactUsController@contactUs', 'as' => 'app.contact.store']);

});
