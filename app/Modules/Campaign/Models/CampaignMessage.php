<?php namespace App\Modules\Campaign\Models;

use Illuminate\Database\Eloquent\Model;

class CampaignMessage extends Model {

	protected $table = 'campaign_message';

    protected $fillable = ['campaign_id',  'profile_id', 'message'];

}
