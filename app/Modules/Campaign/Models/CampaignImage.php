<?php namespace App\Modules\Campaign\Models;

use Illuminate\Database\Eloquent\Model;

class CampaignImage extends Model {

	protected $table = 'campaign_image';

    protected $fillable = ['campaign_id', 'image_url'];

    public function campaign()
    {
    	return $this->belongsTo('App\Modules\Campaign\Models\Campaign', 'campaign_id', 'campaign_id');
    }

}
