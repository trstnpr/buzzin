<?php namespace App\Modules\Campaign\Models;

use Illuminate\Database\Eloquent\Model;

class Campaign extends Model {

	protected $table = 'campaign';

    protected $fillable = ['profile_id',  'campaign_name', 'campaign_description', 'category_id', 'pot_money', 'fund_type', 'fund_details', 'date_start', 'date_end', 'status', 'generated_url', 'campaign_url'];

    protected $primaryKey = 'campaign_id';

    protected $dates = ['date_start', 'date_end'];

    public function influencer_campaign()
    {
        return $this->hasMany('App\Modules\Influencer\Models\InfluencerCampaign', 'campaign_id', 'campaign_id');
    }

    public function campaign_image()
    {
        return $this->hasOne('App\Modules\Campaign\Models\CampaignImage', 'campaign_id', 'campaign_id');
    }

    public function category()
    {
        return $this->hasOne('App\Modules\User\Models\Category', 'category_id', 'category_id');
    }

    public function profile()
    {
        return $this->belongsTo('App\Modules\User\Models\UserProfile', 'profile_id', 'profile_id');
    }

}
