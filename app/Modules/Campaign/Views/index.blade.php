@extends('appMaster')

@section('title')
	<title>Buzzin by Blogapalooza</title>
@stop

@section('meta')
	{{-- Put Seo tags Here... --}}
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/app/views/index.css') }}" rel="stylesheet">
@stop

@section('content')

	<div class="campaign-content">

		<section class="campaign-image parallax-container valign-wrapper" style="background-image:url('{{ config('s3.bucket_link') . config('cdn.campaign') .'/'. $campaign_details['campaign']['campaign_image']['image_url'] }}');">
			<div class="parallax"><img src="{{ config('s3.bucket_link') . config('cdn.campaign') .'/'. $campaign_details['campaign']['campaign_image']['image_url'] }}"></div>
			<div class="container">
				<div class="banner-text">
					<h4 class="campaign-title">{{ $campaign_details['campaign']['campaign_name'] }}</h4>
					<br/>
					<h5 class="campaign-author">{{ $campaign_details['campaign']['profile']['company']['company_name'] }}</h5>
				</div>
			</div>
		</section>

		<section class="campaign-details">
			<div class="container">
				<div class="row">
					<div class="col l8 s12">
						<div class="campaign-description">
							<h5 class="section-title">About <span class="b2i">{{ $campaign_details['campaign']['campaign_name'] }}</span></h5>
							<p class="section-text">{{ strip_tags($campaign_details['campaign']['campaign_description']) }}</p>

							<a class="btn pill campaign-category">{{ $campaign_details['campaign']['category']['category'] }}</a>
						</div>
					</div>


				</div>
			</div>
		</section>

	</div>

	<input type="hidden" id="camp" value="{{ $campaign_details['generated_url'] }}"

@stop

@section('footer')
	<script src="{{ config('s3.bucket_link') . elixir('assets/app/views/index.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function(){

			var user = getCookie("campaign");
			if (user != "") {
			    console.log("Welcome to "+user);
			} else {
			    user = $("#camp").val();
			    if (user != "" && user != null) {
			        setCookie("campaign", user, 365);
			        console.log("setcookie");

			        $.ajax({
			            url: "/campaign/add/"+$("#camp").val(),
			            type: "GET",
			            data: new FormData(this),
			            contentType: false,
			            cache: false,
			            processData: false,
			            success: function(data){
			            }
			        });
			    }
			}

			function getCookie(cname) {
			    var name = cname + "=";
			    var ca = document.cookie.split(';');

			    for(var i = 0; i < ca.length; i++) {
			        var c = ca[i];
			        while (c.charAt(0) == ' ') {
			            c = c.substring(1);
			        }
			        if (c.indexOf(name) == 0) {
			            return c.substring(name.length, c.length);
			        }
			    }
			    return "";
			}

		  	function setCookie(cname, cvalue, exdays) {
			    var d = new Date();
			    d.setTime(d.getTime() + (exdays*24*60*60*1000));
			    var expires = "expires="+d.toUTCString();
			    var url = $("#camp").val();
			    var new_url = url.replace(/ /g, "%20");

			    var path = "/campaign/view/"+new_url ;

			    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=" + path;
			}
		});
	</script>
@stop
