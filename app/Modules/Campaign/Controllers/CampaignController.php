<?php namespace App\Modules\Campaign\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests\CampaignRequest;
use App\Modules\Campaign\Models\Campaign;
use App\Modules\Campaign\Models\CampaignImage;
use App\Modules\User\Models\UserProfile;
use Carbon\Carbon;
use Auth;

class CampaignController extends Controller {

	public function index()
	{
		return view('Campaign::index');
	}
}
