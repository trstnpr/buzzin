<?php namespace App\Modules\Influencer\Controllers;

use App\Http\Requests;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\BlogsRequest;

use App\Modules\Influencer\Models\SocialMedia;
use App\Modules\User\Models\User;
use App\Modules\User\Models\UserProfile;
use App\Modules\User\Models\Category;
use App\Modules\Influencer\Models\Blogs;

use Auth;

class BlogsController extends Controller
{

    public function blogs()
    {
        $profile_id = UserProfile::where('user_id', '=', Auth::user()->id)->value('profile_id');

        $this->data['blogs'] = Blogs::with('category')->where('profile_id', '=', $profile_id)->get();

        return view("Influencer::blogs.index", $this->data);
    }

    /**
     * Get all campaign.
     *
     * @return array
     */
    public function getBlogs()
    {
        $profile = UserProfile::where('user_id', Auth::user()->id)->first();

        $blogs = Blogs::where('profile_id', $profile['profile_id'])
                    ->join('category', 'blogs.category_id', '=', 'category.category_id')
                    ->select('blogs.*', 'category.category')
                    ->get()
                    ->toArray();

        $sc = array_map(function ($structure) use ($blogs) {

            $action = '<a href="'.route('influencer.blogs.edit', $structure['blog_id']).'" class="btn waves-effect white-text" title="Edit">
                            <i class="material-icons">edit</i>
                        </a> ';

            $action .= '<button id="btn-delete-blog" type="button" class="btn btn-danger red btn-circle red btn-delete-blog" title="Delete" data-toggle="modal" data-target="#delete-blog"
                        onclick="deleteBlog(this)"
                        data-blog-id="' . $structure['blog_id'] . '">
                        <i class="material-icons">delete</i>
                    </button>';
            $url = '<a href="' . $structure['blog_url'] . '"> '.$structure['blog_url'].' </a>';
            

            return [
                'name' => $structure['blog_name'],
                'url' => $url,
                'category' => $structure['category'],
                'action' => $action
            ];
        }, $blogs);

        return ['data' => $sc];

    }

    public function createBlogs()
    {
        $this->data['categories'] = Category::all();

        return view("Influencer::blogs.create", $this->data);
    }

    public function storeBlogs(BlogsRequest $request)
    {
        $profile_id = UserProfile::where('user_id', '=', Auth::user()->id)->value('profile_id');

        $input['profile_id'] = $profile_id;
        $input['blog_name'] = $request->blog_name;
        $input['blog_url'] = $request->blog_url;
        $input['category_id'] = $request->category_id;

        if(Blogs::create($input)){
            return json_encode(array('result'=>'success', 'message'=>'Blog Created!', 'redirect'=>route('influencer.blogs.index')));
        }
        return json_encode(array('result'=>'error', 'message'=>'There is an error occured while creating. Please try again later.'));
    }

    public function editBlogs($id)
    {
        $this->data['categories'] = Category::all();
        $this->data['blog'] = Blogs::find($id);

        return view("Influencer::blogs.edit", $this->data);
    }

    public function updateBlogs($id, BlogsRequest $request)
    {
        $profile_id = UserProfile::where('user_id', '=', Auth::user()->id)->value('profile_id');
        
        $input['blog_name'] = $request->blog_name;
        $input['blog_url'] = $request->blog_url;
        $input['category_id'] = $request->category_id;

        if(Blogs::where('blog_id', '=', $id)->where('profile_id', '=', $profile_id)->update($input)){
            return json_encode(array('result'=>'success', 'message'=>'Blog Updated!', 'redirect'=>route('influencer.blogs.index')));
        }
        return json_encode(array('result'=>'error', 'message'=>'There is an error occured while updating. Please try again later.'));
    }

    /**
     * Delete blogs.
     *
     * @return Response
     */
    public function deleteBlogs($id)
    {
        $profile_id = UserProfile::where('user_id', '=', Auth::user()->id)->value('profile_id');

        if (Blogs::where('blog_id', '=', $id)->where('profile_id', '=', $profile_id)->delete()) {
            return json_encode(array('result' => 'success', 'message' => 'Blog Deleted!'));
        }

        return json_encode(array('result' => 'error', 'message' => 'There is an error occured while deleting. Please try again later.'));
    }

}
