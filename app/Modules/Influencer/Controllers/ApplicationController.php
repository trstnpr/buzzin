<?php namespace App\Modules\Influencer\Controllers;

use Auth;

use App\Http\Controllers\Controller;

use App\Modules\Influencer\Models\InfluencerCampaign;
use App\Modules\User\Models\UserProfile;

class ApplicationController extends Controller
{

    public function application()
    {
    	$profile_id = UserProfile::where('user_id', '=', Auth::user()->id)->value('profile_id');

    	$this->data['campaigns'] = InfluencerCampaign::where('profile_id', '=', $profile_id)->where('type', '=', 'apply')->where('status', '=', 0)->get();

        return view("Influencer::application.index", $this->data);
    }

}
