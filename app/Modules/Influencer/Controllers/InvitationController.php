<?php namespace App\Modules\Influencer\Controllers;

use Auth;

use App\Http\Controllers\Controller;

use App\Modules\Influencer\Models\InfluencerCampaign;
use App\Modules\User\Models\UserProfile;

class InvitationController extends Controller
{

    public function invitation()
    {
    	$profile_id = UserProfile::where('user_id', '=', Auth::user()->id)->value('profile_id');

    	$this->data['campaigns'] = InfluencerCampaign::where('profile_id', '=', $profile_id)->where('type', '=', 'invite')->where('status', '!=', 1)->get();

        return view("Influencer::invitation.index", $this->data);
    }

}
