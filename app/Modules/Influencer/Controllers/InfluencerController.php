<?php namespace App\Modules\Influencer\Controllers;

use App\Http\Requests;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\InfluencerRequest;
use App\Http\Requests\PasswordRequest;

use App\Modules\Influencer\Models\SocialMedia;
use App\Modules\User\Models\User;
use App\Modules\User\Models\UserProfile;
use App\Modules\User\Models\ProfileCategory;
use App\Modules\User\Models\Category;
use App\Modules\Campaign\Models\Campaign;
use App\Modules\Admin\Models\Announcement;
use App\Modules\User\Models\Region;
use App\Modules\Influencer\Models\InfluencerCampaign;

use Auth;
use Hash;

use App\Services\MailSender;

class InfluencerController extends Controller
{

    /**
     * Display dashboard of the influencer.
     *
     * @return Response
     */
    public function index()
    {   
         $new_carbon = date("Y-m-d");
        $this->data['profile'] = UserProfile::with(['influencerCampaign' => function($query){
                                                $query->select('*')->where('status', '=', 1)->get();
                                            }])
                                            ->where('user_id', '=', Auth::user()->id)
                                            ->first();
        $userCategory = array();
        $category = ProfileCategory::where('profile_id', '=', $this->data['profile']['profile_id'])->get();
        foreach ($category as $key => $value) {
            array_push($userCategory, $value['category_id']);
        }
        $this->data['campaigns'] = Campaign::with(['profile', 'campaign_image'])->whereIn('category_id', $userCategory)->where('status', '=', 1)->get();

        $this->data['announcement'] = Announcement::where('expiration_date', '>=', $new_carbon)
                                ->where('published_date', '<=', $new_carbon)
                                ->orderBy('published_date', 'desc')
                                ->get()
                                ->toArray();

        $profile_id = UserProfile::where('user_id', Auth::user()->id)->first();

        $this->data['influencer_campaign'] = InfluencerCampaign::join('campaign', 'influencer_campaign.campaign_id', '=', 'campaign.campaign_id')
                                    ->join('profile', 'campaign.profile_id', '=', 'profile.profile_id')
                                    ->join('company', 'profile.profile_id', '=', 'company.profile_id')
                                    ->with(['campaign' => function($query){
                                        $query->select()
                                        ->with(['campaign_image' => function($query){
                                            $query->select();
                                        }]);
                                    }])
                                    ->select('influencer_campaign.*', 'campaign.campaign_name', 'company.company_name')
                                    ->where('influencer_campaign.profile_id', $profile_id['profile_id'])
                                    ->where('influencer_campaign.type', 'invite')
                                    ->where('influencer_campaign.status', 0)
                                    ->get();

        $this->data['count'] = InfluencerCampaign::join('campaign', 'influencer_campaign.campaign_id', '=', 'campaign.campaign_id')
                                    ->join('profile', 'campaign.profile_id', '=', 'profile.profile_id')
                                    ->join('company', 'profile.profile_id', '=', 'company.profile_id')
                                    ->with(['campaign' => function($query){
                                        $query->select()
                                        ->with(['campaign_image' => function($query){
                                            $query->select();
                                        }]);
                                    }])
                                    ->select('influencer_campaign.*', 'campaign.campaign_name', 'company.company_name')
                                    ->where('influencer_campaign.profile_id', $profile_id['profile_id'])
                                    ->where('influencer_campaign.type', 'invite')
                                    ->where('influencer_campaign.status', 0)
                                    ->count();

        return view("Influencer::index", $this->data);
    }

    /**
     * Display profile settings of the influencer
     *
     * @return Response
     */
    public function profileSettings()
    {
        $this->data['profile'] = UserProfile::where('user_id', '=', Auth::user()->id)->first();
        $this->data['region'] = Region::select('region_id', 'region')->get();

        return view("Influencer::settings.profile", $this->data);
    }

    /**
     * Update profile settings of the influencer
     *
     * @return Response
     */
    public function profileSettingsUpdate(InfluencerRequest $request)
    {
        $input = $request->except('_token');
        if (UserProfile::where('user_id', '=', Auth::user()->id)->update($input)) {
            return json_encode(array('result' => 'success', 'message' => 'Profile Updated!'));
        }
        return json_encode(array('result' => 'error', 'message' => 'There\'s an error updating your profile!'));
    }

    /**
     * Display password settings of the influencer
     *
     * @return Response
     */
    public function passwordSettings()
    {
        return view("Influencer::settings.password");
    }

    /**
     * Display password settings of the influencer
     *
     * @return Response
     */
    public function passwordSettingsUpdate(PasswordRequest $request)
    {
        $password = User::where('id', '=', Auth::user()->id)->value('password');
        if (Hash::check($request->current_password, $password)) {
            User::where('id', '=', Auth::user()->id)->update(['password' => Hash::make($request->password)]);
            return json_encode(array('result' => 'success', 'message' => 'Password Updated!'));
        }
        return json_encode(array('result' => 'error', 'message' => 'There\'s an error updating your password!'));
    }

    /**
     * Display scoail accounts of the influencer
     *
     * @return Response
     */
    public function socialAccounts()
    {
        $profile_id = UserProfile::where('user_id', '=', Auth::user()->id)->value('profile_id');
        $this->data['facebook'] = SocialMedia::where('profile_id', '=', $profile_id)->where('provider_name', '=', 'facebook')->first();
        $this->data['google'] = SocialMedia::where('profile_id', '=', $profile_id)->where('provider_name', '=', 'google')->first();
        $this->data['twitter'] = SocialMedia::where('profile_id', '=', $profile_id)->where('provider_name', '=', 'twitter')->first();
        $this->data['instagram'] = SocialMedia::where('profile_id', '=', $profile_id)->where('provider_name', '=', 'instagram')->first();

        //dd($this->data);
        return view("Influencer::social-accounts.index", $this->data);
    }

    /**
     * Display category settings of the influencer
     *
     * @return Response
     */
    public function categories()
    {
        $profile_id = UserProfile::where('user_id', '=', Auth::user()->id)->value('profile_id');
        $this->data['myCategories'] = array();
        $this->data['categories'] = Category::all();
        $result = ProfileCategory::select('category_id')->where('profile_id', '=', $profile_id)->get();
        foreach ($result as $key => $value) {
            array_push($this->data['myCategories'], $value['category_id']);
        }
        //dd($this->data);
        return view("Influencer::settings.categories", $this->data);
    }

    /**
     * Display category settings of the influencer
     *
     * @return Response
     */
    public function categoriesUpdate(Request $request)
    {
        $profile_id = UserProfile::where('user_id', '=', Auth::user()->id)->value('profile_id');
        if($request->category != null){
            ProfileCategory::where(['profile_id'=>$profile_id])->delete();
            $category = $request->category;
            for($i=0;$i<count($request->category);$i++){
                ProfileCategory::create(['profile_id'=>$profile_id, 'category_id'=>$category[$i]]);
            }
            return json_encode(array('result' => 'success', 'message' => 'Success'));
        }
        return json_encode(array('result' => 'error', 'message' => 'Select at least one of the categories.'));
    }

    /**
     * Update Status of Application in Campaign.
     *
     * @return response
     */
    public function notifStatus($id, Request $request, MailSender $mailSender) 
    {   
        $blogger = InfluencerCampaign::where('blogger_campaign_id', $id)->first();
        $campaign = Campaign::where('campaign_id', $blogger['campaign_id'])->first();

        $profile = InfluencerCampaign::join('profile', 'influencer_campaign.profile_id', '=', 'profile.profile_id')
                            ->join('user', 'profile.user_id', '=', 'user.id')
                            ->select('user.email', 'profile.first_name', 'profile.last_name')
                            ->where('influencer_campaign.blogger_campaign_id', $id)
                            ->first();

        if($request->status == 1){
            $url_code = $this->_generate_url_code();
            $new_url =  $campaign['campaign_name'] .'_'. $campaign['campaign_id'] .'_'. $url_code;

            $request->merge(['email' => $profile['email'], 'last_name' => $profile['last_name'], 'first_name' => $profile['first_name'],  'link' => env('APP_URL') . '/campaign/' . $new_url]);

            $mailSender->send('email.approve_invite', 'Campaign Invitation', $request->all());
        }
        else{
            $new_url = '';
        }
    
        $status = InfluencerCampaign::where('blogger_campaign_id', $id)->update(['status' => $request->status, 'generated_url' => $new_url]);

        if($status){
            return json_encode(array('success' => 'success', 'message' => 'Successfully Updated!'));
        }

        return json_encode(array('result' => 'error', 'message' => 'There is an error occured while saving. Please try again later.'));
    }

    /**
     * Generate url code.
     *
     * @return string
     */
    private function _generate_url_code()
    {
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZzxcvbnmasdfghjklqwertyuiop';

        $pin = mt_rand(10, 99) . mt_rand(10, 99) . $characters[rand(0, strlen($characters) - 1)];

        return str_shuffle($pin);
    }

}
