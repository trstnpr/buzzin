<?php namespace App\Modules\Influencer\Controllers;

use Auth;

use Input;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Modules\Influencer\Models\InfluencerCampaign;
use App\Modules\Campaign\Models\Campaign;
use App\Modules\User\Models\UserProfile;
use App\Modules\User\Models\Category;

use App\Services\MailSender;

class CampaignController extends Controller
{

    public function campaign()
    {
    	$profile_id = UserProfile::where('user_id', '=', Auth::user()->id)->value('profile_id');

    	$this->data['campaigns'] = InfluencerCampaign::where('profile_id', '=', $profile_id)->where('status', '=', 1)->get();

        return view("Influencer::campaign.index", $this->data);
    }

    public function showCampaign($id)
    {
        $profile_id = UserProfile::where('user_id', '=', Auth::user()->id)->value('profile_id');
        $this->data['campaign'] = Campaign::find($id);
        $this->data['influencer'] = InfluencerCampaign::where('profile_id', '=', $profile_id)->where('campaign_id', '=', $id)->first();

        //dd($this->data['influencer']);
        return view("Influencer::applycampaign", $this->data);
    }

    public function allcampaigns()
    {
        if(\Request::input('id')){
            $id = \Request::input('id');
            $this->data['campaigns'] = Campaign::where('category_id', '=', $id)->get();
            $this->data['selected'] = $id;
        } else {
            $this->data['campaigns'] = Campaign::all();
            $this->data['selected'] = null;
        }

        $this->data['categories'] = Category::all();

        return view("Influencer::allcampaigns", $this->data);
    }

    public function updateCampaign($id, Request $request, MailSender $mailSender)
    {
        $profile_id = UserProfile::where('user_id', '=', Auth::user()->id)->value('profile_id');

        if($request->type == 'apply'){
            if($request->status == 'apply'){
                InfluencerCampaign::create(['campaign_id'=>$id, 'profile_id'=>$profile_id, 'type'=>'apply']);
                return json_encode(array('result'=>'success', 'message'=>'Success!'));
            } else if($request->status == 'cancel'){
                InfluencerCampaign::where(['campaign_id'=>$id, 'profile_id'=>$profile_id, 'type'=>'apply'])->delete();
                return json_encode(array('result'=>'success', 'message'=>'Success!'));
            }
            return json_encode(array('result'=>'error', 'message'=>'There is an error occured while saving. Please try again later.'));
        } else if($request->type == 'invite') {
            $campaign = Campaign::where('campaign_id', $id)->first();

            $profile = InfluencerCampaign::join('profile', 'influencer_campaign.profile_id', '=', 'profile.profile_id')
                            ->join('user', 'profile.user_id', '=', 'user.id')
                            ->select('user.email', 'profile.first_name', 'profile.last_name')
                            ->where('influencer_campaign.campaign_id', $id)
                            ->where('influencer_campaign.profile_id', $profile_id)
                            ->first();

            if($request->status == 'approve'){
                $url_code = $this->_generate_url_code();
                $new_url =  $campaign['campaign_name'] .'_'. $campaign['campaign_id'] .'_'. $url_code;

                $request->merge(['email' => $profile['email'], 'last_name' => $profile['last_name'], 'first_name' => $profile['first_name'],  'link' => env('APP_URL') . '/campaign/' . $new_url]);

                $mailSender->send('email.approve_invite', 'Campaign Invitation', $request->all());

                InfluencerCampaign::where(['campaign_id'=>$id, 'profile_id'=>$profile_id, 'type'=>'invite'])->update(['status'=>1, 'generated_url' => $new_url]);

                return json_encode(array('result'=>'success', 'message'=>'Success!'));
            } else if($request->status == 'decline'){
                InfluencerCampaign::where(['campaign_id'=>$id, 'profile_id'=>$profile_id, 'type'=>'invite'])->update(['status'=>2]);
                return json_encode(array('result'=>'success', 'message'=>'Success!'));
            }
        }
        return json_encode(array('result'=>'error', 'message'=>'There is an error occured while saving. Please try again later.'));
    }

    /**
     * Delete campaign.
     *
     * @return Response
     */
    public function withdrawCampaign($id)
    {      
        $profile_id = UserProfile::where('user_id', '=', Auth::user()->id)->value('profile_id');

        $campaign = Campaign::find($id);
        $influencer = InfluencerCampaign::where('profile_id', '=', $profile_id)->where('campaign_id', '=', $id)->first();

        if (InfluencerCampaign::where('blogger_campaign_id', $influencer['blogger_campaign_id'])->delete()) {
            /*CampaignImage::where('campaign_id', $id)->delete();*/
            return json_encode(array('result' => 'success', 'message' => 'Successfully Deleted.'));
        }

        return json_encode(array('result' => 'error', 'message' => 'There is an error occured while deleting. Please try again later.'));
    }

    /**
     * Generate url code.
     *
     * @return string
     */
    private function _generate_url_code()
    {
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZzxcvbnmasdfghjklqwertyuiop';

        $pin = mt_rand(10, 99) . mt_rand(10, 99) . $characters[rand(0, strlen($characters) - 1)];

        return str_shuffle($pin);
    }


}
