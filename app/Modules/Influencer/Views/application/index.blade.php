@extends('userMaster')

@section('title')
	<title>My Applications</title>
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/influencer/views/index.css') }}" rel="stylesheet">

@stop

@section('content')

	<div class="dashboard-content">
		<div class="container">

			<h5 class="page-title">Applications</h5>

			<div class="section-content">

				<div class="panel">
					<table class="responsive-table highlight" cellspacing="0" width="100%" id="influencerapplication">
						<thead>
							<th>Company Name</th>
							<th>Campaign Name</th>
							<th>Category</th>
							<th>Date Start</th>
							<th>Date End</th>
							<th>Status</th>
							<th class="center">Action</th>
						</thead>
						<tbody>
							@if($campaigns->count() == 0)
							<tr>
								<td colspan="7" class="center">No data available in table</td>
							</tr>
							@else
								@foreach($campaigns as $c)
									<tr>
										<td>{{ str_limit($c->campaign->profile->company->company_name, 30) }}</td>
										<td>{{ str_limit($c->campaign->campaign_name, 30) }}</td>
										<td>{{ $c->campaign->category->category }}</td>
										<td>{{ $c->campaign->date_start->format('M d, Y') }}</td>
										<td>{{ $c->campaign->date_end->format('M d, Y') }}</td>
										<td>
											@if($c->status == 0)
												Pending
											@else
												Declined
											@endif
										</td>
										<td class="center">
											<button type="button" onclick="window.location.href='{{ route('influencer.campaign.show', $c->campaign_id) }}'" class="btn btn-danger">VIEW</button>
		                    				<button type="button" id="cancel-button" class="btn red" onclick="updateCampaign('apply', 'cancel', '{{$c->campaign_id}}')">CANCEL</button>
		                    			</td>
									</tr>
								@endforeach
							@endif
						</tbody>
					</table>
				</div>

			</div>

		</div>

	</div>
	<input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
@stop

@section('footer')
	<script src="{{ config('s3.bucket_link') . elixir('assets/influencer/views/index.js') }}"></script>
	<script src="{{ config('s3.bucket_link') . elixir('assets/influencer/views/campaigns/index.js') }}"></script>
@stop
