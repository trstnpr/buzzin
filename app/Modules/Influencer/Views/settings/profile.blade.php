@extends('userMaster')

@section('title')
	<title>Influencer</title>
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/influencer/views/index.css') }}" rel="stylesheet">
@stop

@section('content')

	<div class="settings-content">
		<div class="container">

			<h5 class="page-title">Profile Settings</h5>

			<div class="section-content">
				<form id="profile-form" action="{{ route('influencer.profile.update') }}">
					<input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
					<div class="row">
						<div class="col l10 s12">
							<div class="panel">
								<div class="row">
									<div class="col s6">
										<div class="form-group">
											<label>First Name</label>
											<input type="text" name="first_name" required="required" class="form-control b2i-field" placeholder="Your Firstname" value="{{ $profile->first_name }}" />
										</div>
									</div>
									<div class="col s6">
										<div class="form-group">
											<label>Last Name</label>
											<input type="text" name="last_name" required="required" class="form-control b2i-field" placeholder="Your Lastname" value="{{ $profile->last_name }}" />
										</div>
									</div>
									<div class="col s12">
										<div class="form-group">
											<label>About Me</label>
											<textarea type="text" name="about_blogger" class="wysiwyg">{!! $profile->about_blogger !!}</textarea>
										</div>
									</div>
									<div class="col s6">
										<div class="form-group">
											<label>Address 1</label>
											<input type="text" name="address_1" required="required" class="form-control b2i-field" placeholder="House/Unit/Block No. Street" value="{{ $profile->address_1 }}" />
										</div>
									</div>
									<div class="col s6">
										<div class="form-group">
											<label>Address 2 (optional)</label>
											<input type="text" name="address_2" class="form-control b2i-field" placeholder="Village/Subdivision/Barangay" value="{{ $profile->address_2 }}" />
										</div>
									</div>
									<div class="col s4">
										<div class="form-group">
											<label>City</label>
											<input type="text" name="city" required="required" class="form-control b2i-field" value="{{ $profile->city }}" />
										</div>
									</div>
									<div class="col s4">
										<div class="form-group">
											<label>State / Province</label>
											<input type="text" name="state" required="required" class="form-control b2i-field" value="{{ $profile->state }}" />
										</div>
									</div>
									<div class="col s4">
										<div class="form-group">
											<label>Zip Code (optional)</label>
											<input type="text" name="zip_code" class="form-control b2i-field" placeholder="Zip Code" value="{{ $profile->zip_code }}" />
										</div>
									</div>
									<div class="col s6">
										<div class="form-group">
											<label>Region</label>
											<select class="form-control b2i-select-field" id="region_select" name="state">
										    <option value="" disabled selected>Choose your region</option>
									      	@foreach($region as $c)
				                               <option value="{{$c['region']}}" {{ ($c['region'] == $profile['state']) ? 'selected' : '' }}>{{$c['region']}}</option>
				                            @endforeach
										    </select>
										</div>
									</div>
									<div class="col s6">
										<div class="form-group">
											<label>Country</label>
											<input type="text" name="country" required="required" class="form-control b2i-field" placeholder="Country" value="{{ $profile->country }}" />
										</div>
									</div>
									<div class="col s12">
										<div class="form-group">
											<label>Contact Number</label>
											<input type="text" name="contact_number" required="required" class="form-control b2i-field" placeholder="Primary Mobile Number" value="{{ $profile->contact_number }}" />
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="col l2 s12">
							<div class="action-buttons">
								<button type="submit" id="profile-button" class="btn btn-large purple waves-effect waves-light col s12" style="margin-bottom: 10px;">submit</button>
								<a href="{{ route('influencer.dashboard') }}" class="btn btn-large waves-effect waves-light red col s12">cancel</a>
							</div>
						</div>
					</div>
				</form>
			</div>

		</div>
	</style>

@stop

@section('footer')
	<script src="{{ config('s3.bucket_link') . elixir('assets/influencer/views/index.js') }}"></script>
	<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			tinymce.init({
				selector:'.wysiwyg',
				height:150,
				plugins: [
					'advlist autolink lists link image charmap print preview anchor',
					'searchreplace visualblocks code fullscreen',
					'insertdatetime media table contextmenu paste code'
				],
				toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image'
			});
		});
	</script>

	<script src="{{ config('s3.bucket_link') . elixir('assets/influencer/views/settings/profile.js') }}"></script>
@stop
