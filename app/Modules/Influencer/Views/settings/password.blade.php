@extends('userMaster')

@section('title')
	<title>Influencer</title>
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/influencer/views/index.css') }}" rel="stylesheet">
@stop

@section('content')

	<div class="settings-content">
		<div class="container">

			<h5 class="page-title">Password Settings</h5>

			<div class="section-content">
				<form id="password-form" action="{{ route('influencer.password.update') }}">
					<input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
					<div class="row">
						<div class="col l10 s12">
							<div class="panel">
								<div class="row">
									<div class="col s12">
										<h6>Want to change your password?</h6>
									</div>
									<div class="col s12">
										<div class="form-group">
											<label>Current Password</label>
											<input type="password" name="current_password" class="form-control b2i-field" required="required" />
										</div>
									</div>
									<div class="col l6 s12">
										<div class="form-group">
											<label>New Password</label>
											<input type="password" name="password" class="form-control b2i-field" required="required">
										</div>
									</div>
									<div class="col l6 s12">
										<div class="form-group">
											<label>Confirm New Password</label>
											<input type="password" name="password_confirmation" class="form-control b2i-field" required="required">
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="col l2 s12">
							<div class="action-buttons">
								<button type="submit" id="password-button" class="btn btn-large purple waves-effect waves-light col s12" style="margin-bottom: 10px;">save changes</button>
								<a href="{{ route('influencer.dashboard') }}" class="btn btn-large waves-effect waves-light red col s12">cancel</a>
							</div>
						</div>
					</div>
				</form>
			</div>

		</div>
	</style>

@stop

@section('footer')
	<script src="{{ config('s3.bucket_link') . elixir('assets/influencer/views/index.js') }}"></script>

	<script src="{{ config('s3.bucket_link') . elixir('assets/influencer/views/settings/password.js') }}"></script>
@stop
