@extends('userMaster')

@section('title')
	<title>Influencer</title>
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/influencer/views/index.css') }}" rel="stylesheet">
@stop

@section('content')

	<div class="settings-content">
		<div class="container">

			<h5 class="page-title">Categories of Interest</h5>

			<div class="section-content">
				<form id="categories-form" action="{{ route('influencer.categories.update') }}">
					<input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
					<div class="row">
						<div class="col l9 s12">
							<div class="panel">
								<div class="row">
									@foreach($categories as $cat)
									<div class="col l12 s12" style="padding-bottom: 5px">
										<input type="checkbox" id="{{ $cat->category_id }}" name="category[]" value="{{ $cat->category_id }}" {{ (in_array($cat->category_id, $myCategories)) ? "checked" : "" }}/>
      									<label for="{{ $cat->category_id }}">{{ $cat->category }}</label>
									</div>
									@endforeach
								</div>
							</div>
						</div>
						<div class="col l3 s12">
							<div class="action-buttons">
								<button type="submit" id="categories-button" class="btn btn-large purple waves-effect waves-light col s12" style="margin-bottom: 10px;">save changes</button>
								<a href="{{ route('influencer.dashboard') }}" class="btn btn-large waves-effect waves-light red col s12">cancel</a>
							</div>
						</div>
					</div>
				</form>
			</div>

		</div>
	</style>

@stop

@section('footer')
	<script src="{{ config('s3.bucket_link') . elixir('assets/influencer/views/index.js') }}"></script>

	<script src="{{ config('s3.bucket_link') . elixir('assets/influencer/views/settings/categories.js') }}"></script>
@stop
