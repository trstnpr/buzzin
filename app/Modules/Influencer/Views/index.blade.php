@extends('userMaster')

@section('title')
	<title>Dashboard</title>
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/influencer/views/index.css') }}" rel="stylesheet">
	<style type="text/css">
		.text-ellipsis {
			text-overflow: ellipsis !important;
			 overflow: hidden !important;
   			 white-space: nowrap !important;
   			 width:250px;
   			 display: block;
		}
	</style>
@stop

@section('content')

<div class="dashboard-content">
		<div class="container">

			<h5 class="page-title">Dashboard</h5>
			<div class="section-content">
				<div class="row">
					<div class="col l3 s12">
						<div class="panel-brand date-panel">
							<div class="panel-head">
								<h4 class="panel-title">Date and Time</h4>
							</div>
							<div class="panel-body">
								<h6>
									<span class="day_"><span id="time"></span></span>
	                				<span class="year_"><span id="ampm"></span></span>
                				</h6><br/>
								<h5>
									<span class="day_" id="day_"></span>,
									<span class="year_" id="year_"></span>
								</h5>
								<h5>
									<span class="year_" id="date_"></span>
								</h5>
							</div>
						</div>
					</div>
					<div class="col l3 s12">
						<a href="#notif" id="notif_block" class="modal-trigger">
						<div class="panel-brand messages-panel orange">
							<div class="panel-head">
								<h4 class="panel-title">Notifications</h4>
							</div>
							<div class="panel-body">
								<h2 class="panel-label">{{ $count }}</h2>
							</div>
						</div>
						</a>
					</div>
					<div class="col l6 s12">
						<div class="panel-brand announcement-panel">
							<div class="panel-head amber lighten-1">
								<h4 class="panel-title"><i class="fa fa-list"></i> Announcements</h4>
							</div>
							<div class="panel-body announcement-container">
								<ul class="announcement-content">
									@foreach($announcement as $a)
									<li class="item">
										<h6 class="item-title">{{ $a['title'] }}<small class="right">{{ $a['published_date'] }}</small></h6>
										<p class="item-content">{{ strip_tags($a['announcement']) }}</p>
									</li>
									<li class="item-divider"></li>
									@endforeach
								</ul>
							</div>
						</div>
					</div>
					<div class="col s12">
						<div class="panel-brand campaign-panel">
							<div class="panel-head amber lighten-1">
								<h4 class="panel-title"><i class="fa fa-bullhorn"></i> My Campaigns</h4>
							</div>
							<div class="panel-body campaign-container">
								@if($profile->influencerCampaign == null)
									<div class="row center">
										<h5>No record found.</h5>
									</div>
								@else
									@foreach($profile->influencerCampaign as $camp)
									<div class="campaign-item">
										<div class="row" style="margin:0;">
											<div class="col l2 hide-on-med-and-down">
												@if($camp->campaign->campaign_image == null)
												<img class="responsive-img center-block" src="{{ asset('images/duck.jpg') }}">
												@else
												<img class="responsive-img center-block" src="{{ config('s3.bucket_link') . config('cdn.campaign') . '/' . $camp->campaign->campaign_image->image_url }}">
												@endif
											</div>
											<div class="col l10 s12">
												<h5 class="campaign-title">{{ $camp->campaign->campaign_name }} <a class="btn pill right">{{ $camp->campaign->category->category }}</a></h5>
												<p class="campaign-description excerpt">{!! str_limit($camp->campaign->campaign_description, $limit = 250, $end = '...') !!} <a href="{{ route('influencer.campaign.show', $camp->campaign_id) }}">Read More</a> </p>


												<div class="influencers">
													<a class="btn pill left">{{ $camp->campaign->profile->company->company_name }}</a>
													<a href="{{ route('influencer.campaign.show', $camp->campaign_id) }}" class="btn purple pill right" >View Campaign</a>
												</div>
											</div>
										</div>
									</div>
									@endforeach
								@endif
							</div>
						</div>
					</div>

					<div class="col s12">
						<h5 class="top-border">Available Campaigns</h5>
						<div class="row">
							@if($campaigns->count() == 0)
							<div class="row center">
								<h6>No Available Campaigns Found.</h6>
							</div>
							@else
								@foreach($campaigns as $campaign)
								<div class="col l4 m6 s12">
									<div class="card {{-- materialboxed --}} profile">
										<div class="card-image waves-effect waves-block waves-light" >

											@if($campaign->campaign_image == null)
											<a href="{{ route('influencer.campaign.show', $campaign->campaign_id) }}" >
											<img class="activator camp-img" src="{{ asset('images/duck.jpg') }}">
											</a>
											@else
											<a href="{{ route('influencer.campaign.show', $campaign->campaign_id) }}" >
											<img class="activator camp-img" src="{{ config('s3.bucket_link') . config('cdn.campaign') . '/' . $campaign->campaign_image->image_url }}">
											</a>
											@endif

										</div>
										<div class="card-content">
											<span class="text-ellipsis card-title grey-text text-darken-4">{{ $campaign->campaign_name }}</span>
											<p class="label">{{ $campaign->profile->company->company_name }}</p>
										</div>
										<div class="card-reveal">
											<span class="card-title grey-text text-darken-4">{{ $campaign->campaign_name }}<i class="material-icons right">close</i></span>
											<p class="bio">{!! $campaign->campaign_description !!}</p>
											<div class="right">
												<a href="{{ route('influencer.campaign.show', $campaign->campaign_id) }}" class="btn grey">View More</a>
											</div>
										</div>
									</div>
								</div>
								@endforeach
							@endif
						</div>
					</div>

					<!-- Button View More -->
					<div class="container">
						<a href="{{ route('influencer.allcampaigns') }}" class="waves-effect btn-large purple right">View All</a>
					</div>
				</div>
			</div>
		</div>

	<!-- Modal Notification Structure -->
	<div id="notif" class="modal bottom-sheet">
	    <div class="modal-content">
	    <input type="hidden" name="_token" value="{{ csrf_token() }}">
	        <h6 class="grey-text"><strong>Campaign Invite Notifications</strong></h6>
	        <ul class="collection">
            @foreach($influencer_campaign as $ic)

               	<li class="collection-item avatar custom-collection blue-grey lighten-5">
               		<img src="{{ config('s3.bucket_link') . config('cdn.campaign') .'/'. $ic['campaign']['campaign_image']['image_url'] }}" alt="Image" class="circle">
               		<ul>
	                    <li><strong><span class="appointment_title">{{ $ic['campaign_name'] }}</span></strong><i class="material-icons right grey-text">notifications</i></li>
	                    <li><span class="appointment_desc">{{ $ic['company_name'] }}</span></li>
	                    <li><small>{{ $ic['created_at'] }}</small></span></li>
                    </ul>

                    <div class="notif-buttons right">
	                    <button class="btn waves-effect waves-light col s12 btn_approve" id="btn_approve" onclick="approveCampaign({{ $ic['blogger_campaign_id'] }});">Approve</button>

						<button class="btn waves-effect waves-light red col s12 btn_denied" id="btn_denied" onclick="declineCampaign({{ $ic['blogger_campaign_id'] }});" >Decline</button>
					</div>
					<br/><br/>
                </li>
            @endforeach
       		</ul>
	    </div>
	    <div class="modal-footer grey lighten-3">
	        <a href="#!" class="modal-action modal-close waves-effect waves-green btn purple">Close</a>
	    </div>
	</div>

@stop

@section('footer')
	<script src="{{ config('s3.bucket_link') . elixir('assets/influencer/views/index.js') }}"></script>
	<script type="text/javascript">
		function approveCampaign(id){
			var param = {
		        status: 1,
		    };

		    $.ajax({
		        url: 'dashboard/notif/'+id,
		        data: param,
		        type: 'PUT',
		        headers:
	            {
	                'X-CSRF-Token': $('input[name="_token"]').val()
	            },
	            beforeSend: function(){ $('.btn_approve').html('Processing...'); document.getElementById("btn_approve").disabled = true; document.getElementById("btn_denied").disabled = true;},
		        success: function(data) {
		           Materialize.toast("You Approved the Invitation.", 2000,'green',function(){ location.reload(); })
		        }
		    });
		};

		function declineCampaign(id){
			var param = {
		        status: 2,
		    };

		    $.ajax({
		        url: 'dashboard/notif/'+id,
		        data: param,
		        type: 'PUT',
		        headers:
	            {
	                'X-CSRF-Token': $('input[name="_token"]').val()
	            },
	            beforeSend: function(){ $('.btn_denied').html('Processing...'); document.getElementById("btn_approve").disabled = true; document.getElementById("btn_denied").disabled = true; },
		        success: function(data) {
		           Materialize.toast("You Declined the Invitation.", 2000,'red',function(){ location.reload(); })
		        }
		    });
		};
	</script>
@stop
