@extends('userMaster')

@section('title')
	<title>Blogs</title>
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/influencer/views/index.css') }}" rel="stylesheet">

@stop

@section('content')

	<input type="hidden" name="_token" value="{{ csrf_token() }}" />
	<div class="dashboard-content">
		<div class="container">

			<h5 class="page-title">My Blogs</h5>

			<div class="section-content blog">

				<div class="panel">
					<table class="responsive-table highlight" cellspacing="0" width="100%" id="influencerblogs"></table>
					{{-- <table class="responsive-table highlight" cellspacing="0" width="100%" id="influencercampaign">
						<thead>
							<th>Blog Name</th>
							<th>Category</th>
							<th>Blog URL</th>
							<th class="center">Action</th>
						</thead>
						<tbody>
							@foreach($blogs as $blog)
							<tr>
								<td>{{ $blog->blog_name }}</td>
								<td>{{ $blog->category->category }}</td>
								<td><a href="{{ $blog->blog_url }}" target="_blank">{{ $blog->blog_url }}</a></td>
								<td class="center">
									<button id="btn-edit-blog" type="button" class="btn btn-danger btn-circle btn-view-campaign" title="Edit" " data-target="#edit-campaign" onclick="window.location.href='{{ route('influencer.blogs.edit', $blog->blog_id) }}'"><i class="material-icons">edit</i>
                    				</button>
									<button id="btn-edit-blog" type="button" class="btn btn-danger red btn-circle btn-delete-campaign" title="Delete" " data-target="#delete-campaign" onclick="deleteBlog(this)" data-campaign-id=""><i class="material-icons">delete</i>
	                    			</button>
                    			</td>
							</tr>
							@endforeach
						</tbody>
					</table> --}}
				</div>

			</div>

		</div>
	</div>

	<div id="delete-blog" class="modal">
        <div class="modal-content">
            <h5>Delete Blog</h5>
            Do you want to delete this blog?
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action modal-close btn green waves-effect waves-green btn-delete-yes-blog">Yes</a>
            <a href="#!" class="modal-action modal-close btn red waves-effect waves-red">No</a>
        </div>
    </div>

	<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
		<a href="{{ route('influencer.blogs.create') }}" class="btn-floating btn-large purple tooltipped" data-position="left" data-tooltip="Post Blog">
			<i class="large mdi mdi-tooltip-edit"></i>
		</a>
	</div>

@stop

@section('footer')
	<script src="{{ config('s3.bucket_link') . elixir('assets/influencer/views/index.js') }}"></script>
	<script src="{{ config('s3.bucket_link') . elixir('assets/influencer/views/blogs/index.js') }}"></script>
	<script type="text/javascript">
		function deleteBlog(blog) {
	        $('#delete-blog').modal('open');
	        $('.btn-delete-yes-blog').attr('data-blog-id', $(blog).attr('data-blog-id'));
	    }
	</script>
@stop
