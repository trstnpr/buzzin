@extends('userMaster')

@section('title')
	<title>Create Blog</title>
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/influencer/views/index.css') }}" rel="stylesheet">
	<style type="text/css">
		.btn-blog {
			margin: 0px;
		}
	</style>

@stop

@section('content')

	<div class="campaign-content">
		<div class="container">

			<h5 class="page-title">Create Blog</h5>

			<div class="section-content">
				<form class="blog-form" id="blog-form" action="{{ route('influencer.blogs.store') }}">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="row">
						<div class="col l10 s12">
							<div class="card teal hide" id="result_div">
						        <i class="material-icons">check</i> Created successfully!
						    </div>
							<div class="panel">
								<div class="row">
									<div class="col s12">
										<div class="form-group">
											<label>Blog Name</label>
											<input type="text" class="form-control b2i-field" name="blog_name" value="" required="required" />
										</div>
									</div>
									<div class="col s12">
										<div class="form-group">
											<label>Blog URL</label>
											<input type="text" class="form-control b2i-field" name="blog_url" value="" required="required" />
										</div>
									</div>
									<div class="col s12">
										<div class="form-group">
											<label>Category</label>
											<select class="form-control b2i-field" name="category_id" required="required">
												<option disabled selected>Choose Category</option>
												@foreach($categories as $cat)
												<option value="{{ $cat->category_id }}">{{ $cat->category }}</option>
												@endforeach
											</select>
										</div>
									</div>

								</div>
							</div>
						</div>

						<div class="col l2 s12">
							<div class="action-buttons">
								<button class="btn btn-large purple waves-effect waves-light col s12" type="submit" id="blog-button">CREATE</button>
							</div>
						</div>
					</div>
				</form>
			</div>

		</div>
	</div>

@stop

@section('footer')
	<script src="{{ config('s3.bucket_link') . elixir('assets/influencer/views/index.js') }}"></script>
	<script src="{{ config('s3.bucket_link') . elixir('assets/influencer/views/blogs/index.js') }}"></script>
@stop
