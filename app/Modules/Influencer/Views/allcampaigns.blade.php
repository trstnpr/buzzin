@extends('userMaster')

@section('title')
	<title>Apply Campaign</title>
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/influencer/views/index.css') }}" rel="stylesheet">

	<style type="text/css">
		.text-ellipsis {
			text-overflow: ellipsis !important;
			 overflow: hidden !important;
   			 white-space: nowrap !important;
   			 width:250px;
   			 display: block;
		}
	</style>

@stop

@section('content')

	<div class="all-campaign-content">
		<div class="container">

			  <div class="all-campaign">
			  	  <div class="left col s12 campaign-title">
			  	  	<h4>Campaigns</h4>
			  	  </div>
				  <div class="all-category input-field col s12 right">
					    <select class="form-control b2i-select-field" id="category_select">
					      	<option value="" disabled>Choose Category</option>
					      	@foreach($categories as $c)
					      		<option value="{{ $c->category_id }}" {{ ($c->category_id == $selected) ? 'selected' : '' }}>{{ $c->category }}</option>
					      	@endforeach
					    </select>
				  </div>
			  </div>

			  <div class="row">
			  @if($campaigns->count() == 0)
			  	<h5 class="center">No campaigns found.</h5>
			  @else
			  	@foreach($campaigns as $camp)
			  	<div class="col l4 m6 s12">
			  		 <div class="card">
					    <div class="card-image waves-effect waves-block waves-light">
					    	@if($camp->campaign_image == null)
					    	<a href="{{ route('influencer.campaign.show', $camp->campaign_id) }}">
							<img class="activator camp-img" src="{{ asset('images/duck.jpg') }}">
							</a>
							@else
							<a href="{{ route('influencer.campaign.show', $camp->campaign_id) }}">
							<img class="activator camp-img" src="{{ config('s3.bucket_link') . config('cdn.campaign') . '/' . $camp->campaign_image->image_url }}">
							</a>
							@endif
					    </div>
					    <div class="card-content">
					      	<span class="text-ellipsis card-title activator grey-text text-darken-4">{{ $camp->campaign_name }}</span>
					      	<p><a href="#">{{ $camp->profile->company->company_name }}</a></p>
					    </div>
					    <div class="card-reveal">
					      	<span class="card-title grey-text text-darken-4">{{ $camp->campaign_name }}<i class="material-icons right">close</i></span>
					      	<p>{!! $camp->campaign_description !!}</p>
					      	<div class="right">
					      	<a href="{{ route('influencer.campaign.show', $camp->campaign_id) }}" class="btn orange">View more</a>
					      </div>
					    </div>
					  </div>
			  	</div>
			  	@endforeach
			  @endif
			  </div>


		</div>
	</div>

@stop

@section('footer')
	<script src="{{ config('s3.bucket_link') . elixir('assets/influencer/views/index.js') }}"></script>
	<script>
		$(document).ready(function() {
			// $('.material-select').material_select();

			$('#category_select').on('change', function(){
				var url = $(this).val();
				var type = $("#category_select option:selected").text();
				window.location.href = '{{ route('influencer.allcampaigns') }}/?id='+url+'&type='+type;
			});
		});
	</script>
@stop
