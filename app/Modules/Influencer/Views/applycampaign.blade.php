@extends('userMaster')

@section('title')
	<title>Apply Campaign</title>
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/influencer/views/index.css') }}" rel="stylesheet">

	<style type="text/css">
	.chip {
		border-radius: 2px !important;
	}


	.campaign-apply-btn {
		float: right !important;
	}

	@media screen and (max-width: 570px) {
		.campaign-apply-btn {
			float: left !important;
			padding-top: 30px !important;
		}
		.campaign-info {
			padding-top: 20px !important;
		}
	}

	</style>

@stop

@section('content')

	<div class="dashboard-content">
		<div class="container">

			<h5 class="page-title">{{ $campaign->campaign_name }}</h5>

			<div class="section-content">

				<div class="panel">

					<div class="container">

						<div class="row">
							<div class="col l3 m4 s12">
								<div class="campaign-image left">
									<img src="{{ config('s3.bucket_link') . config('cdn.campaign') .'/'. $campaign->campaign_image->image_url }}" width="200" height="200" alt="Campaign Image">
								</div>
							</div>
							<div class="col l5 m4 s12">
								<div class="campaign-info left">
									<div class="chip teal white-text">
										by {{ $campaign->profile->company->company_name }}
									</div><br>
									<div class="chip purple white-text">
										{{ $campaign->category->category }}
									</div><br>
									<div class="chip blue white-text">
										Campaign Budget : {{ $campaign->fund_details }}
									</div><BR>
									<div class="chip green white-text">
										{{ $campaign->date_start->format('F d, Y')}}
									</div>
									<div class="chip red white-text">
										{{ $campaign->date_end->format('F d, Y')}}
									</div>
								</div>
							</div>
							<div class="col l4 m4 s12">
								<div class="campaign-apply-btn">
										<input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
										@if($influencer == null)
										<button type="button" id="apply-button" class="btn-large blue" onclick="updateCampaign('apply', 'apply', '{{$campaign->campaign_id}}')">Apply</button>
										@else
											@if($influencer->type == 'apply')
												@if($influencer->status == 0)
													<button type="button" id="cancel-button" class="btn-large red" onclick="updateCampaign('apply', 'cancel', '{{$campaign->campaign_id}}')">Cancel</button>
												@elseif($influencer->status == 1)
													<h6>You are approved</h6><br/>
													<button type="button" class="btn-large green" data-toggle="modal" data-target="#delete-campaign" onclick="withdrawCampaign(this)" data-campaign-id="{{$campaign->campaign_id}}">Withdraw from campaign</button>
												@else
													<button type="button" class="btn-large orange">You are declined</button>
												@endif
											@else
												@if($influencer->status == 0)
													<button type="button" class="btn-large blue" id="approve-button" onclick="updateCampaign('invite', 'approve', '{{$campaign->campaign_id}}')">Approve</button>
													<button type="button" class="btn-large red" id="decline-button" onclick="updateCampaign('invite', 'decline', '{{$campaign->campaign_id}}')">Decline</button>
												@elseif($influencer->status == 1)
													<button type="button" class="btn-large green">You approved this Campaign</button>
												@else
													<button type="button" class="btn-large orange">You declined this Campaign</button>
												@endif
											@endif
										@endif
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col s12">
								{!! $campaign->campaign_description !!}
							</div>
						</div>
					</div>

				</div>

			</div>

			</div>

		</div>

	<div id="delete-campaign" class="modal">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="modal-content">
            <h5>Withdraw Campaign</h5>
            Are you sure you want to withdraw your access from this campaign?
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action modal-close btn green waves-effect waves-green btn-delete-yes-campaign">Yes</a>
            <a href="#!" class="modal-action modal-close btn red waves-effect waves-red">No</a>
        </div>
    </div>

@stop

@section('footer')
	<script src="{{ config('s3.bucket_link') . elixir('assets/influencer/views/index.js') }}"></script>
	<script src="{{ config('s3.bucket_link') . elixir('assets/influencer/views/campaigns/index.js') }}"></script>

	<script type="text/javascript">
		function withdrawCampaign(campaign) {
	        $('#delete-campaign').modal('open');
	        $('.btn-delete-yes-campaign').attr('data-campaign-id', $(campaign).attr('data-campaign-id'));
	    }

	</script>
@stop
