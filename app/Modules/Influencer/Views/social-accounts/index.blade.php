@extends('userMaster')

@section('title')
	<title>Influencer</title>
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/influencer/views/index.css') }}" rel="stylesheet">
@stop

@section('content')
	
	<div class="socialAccounts-content">
		<div class="container">
			
			<h5 class="page-title">Social Connections</h5>

			<div class="section-content">
				<div class="row">

					<div class="col l12 s12">
						<div class="panel">
							<div class="row">
								<div class="col l6 s12">
									<div class="form-group">
										@if($facebook == null)
										<a href="{{ route('login.auth.redirect', 'facebook') }}" class="btn btn-large btn-facebook col s12"><i class="mdi mdi-facebook"></i> connect to facebook</a>
										@else
										<button class="btn btn-large btn-facebook col s12" disabled="disabled"><i class="mdi mdi-facebook"></i> connected to facebook</button>
										@endif
									</div>
								</div>
								<div class="col l6 s12">
									<div class="form-group">
										@if($google == null)
										<a href="{{ route('login.auth.redirect', 'google') }}" class="btn btn-large btn-google col s12"><i class="mdi mdi-google"></i> connect to google</a>
										@else
										<button class="btn btn-large btn-google col s12" disabled="disabled"><i class="mdi mdi-google"></i> connected to google</button>
										@endif
									</div>
								</div>
								<div class="col l6 s12">
									<div class="form-group">
										@if($twitter == null)
										<a href="{{ route('login.auth.redirect', 'twitter') }}" class="btn btn-large btn-twitter col s12"><i class="mdi mdi-twitter"></i> connect to twitter</a>
										@else
										<button class="btn btn-large btn-twitter col s12" disabled="disabled"><i class="mdi mdi-twitter"></i> connected to twitter</button>
										@endif
									</div>
								</div>
								<div class="col l6 s12">
									<div class="form-group">
										@if($instagram == null)
											<a href="{{ route('login.auth.redirect', 'instagram') }}" class="btn btn-large btn-instagram col s12"><i class="mdi mdi-instagram"></i> connect to instagram</a>
										@else
											<button class="btn btn-large btn-instagram col s12" disabled="disabled"><i class="mdi mdi-instagram"></i> connected to instagram</button>
										@endif
									</div>
								</div>
							</div>
						</div>
					</div>
					{{-- <div class="col l2 s12">
						<div class="action-buttons">
							<button class="btn waves-effect waves-light btn-large col s12">Save</button>
							<button class="btn waves-effect waves-light btn-large red col s12">Cancel</button>
						</div>
					</div> --}}
				</div>
			</div>

		</div>
	</style>

@stop

@section('footer')
	<script src="{{ config('s3.bucket_link') . elixir('assets/influencer/views/index.js') }}"></script>
@stop