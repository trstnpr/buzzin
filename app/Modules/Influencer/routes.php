<?php

Route::group(array('prefix' => 'influencer', 'module' => 'Influencer', 'namespace' => 'App\Modules\Influencer\Controllers', 'middleware' => ['web', 'auth', 'influencer']), function () {

    //DASHBOARD ROUTES
    Route::get('dashboard', ['uses' => 'InfluencerController@index', 'as' => 'influencer.dashboard']);
    Route::put('dashboard/notif/{id}', 'InfluencerController@notifStatus');

    //CAMPAIGN ROUTES
    Route::get('campaign', ['uses' => 'CampaignController@campaign', 'as' => 'influencer.campaign']);
    Route::get('allcampaigns', ['uses' => 'CampaignController@allcampaigns', 'as' => 'influencer.allcampaigns']);
    Route::get('campaign/{id}', ['uses' => 'CampaignController@showCampaign', 'as' => 'influencer.campaign.show']);
    Route::post('campaign/{id}/update', ['uses' => 'CampaignController@updateCampaign', 'as' => 'influencer.campaign.update']);
    Route::delete('campaign/delete/{id}', ['uses' => 'CampaignController@withdrawCampaign', 'as' => 'influencer.campaign.delete']);

    //INVITATION ROUTES
    Route::get('invitation', ['uses' => 'InvitationController@invitation', 'as' => 'influencer.invitation']);

    //APPLICATION ROUTES
    Route::get('application', ['uses' => 'ApplicationController@application', 'as' => 'influencer.application']);

    //BLOGS ROUTES
    Route::get('blogs', ['uses' => 'BlogsController@blogs', 'as' => 'influencer.blogs.index']);
    Route::get('blogs/getAll', ['uses' => 'BlogsController@getBlogs', 'as' => 'influencer.blogs.get']);
    Route::get('blogs/create', ['uses' => 'BlogsController@createBlogs', 'as' => 'influencer.blogs.create']);
    Route::post('blogs/store', ['uses' => 'BlogsController@storeBlogs', 'as' => 'influencer.blogs.store']);
    Route::get('blogs/edit/{id}', ['uses' => 'BlogsController@editBlogs', 'as' => 'influencer.blogs.edit']);
    Route::post('blogs/update/{id}', ['uses' => 'BlogsController@updateBlogs', 'as' => 'influencer.blogs.update']);
    Route::delete('blogs/delete/{id}', ['uses' => 'BlogsController@deleteBlogs', 'as' => 'influencer.blogs.delete']);

    //PROFILE ROUTES
    Route::get('profile/settings', ['uses' => 'InfluencerController@profileSettings', 'as' => 'influencer.profile.index']);
    Route::post('profile/settings/update', ['uses' => 'InfluencerController@profileSettingsUpdate', 'as' => 'influencer.profile.update']);

    //PASSWORD ROUTES
    Route::get('password/settings', ['uses' => 'InfluencerController@passwordSettings', 'as' => 'influencer.password.index']);
    Route::post('password/settings/update', ['uses' => 'InfluencerController@passwordSettingsUpdate', 'as' => 'influencer.password.update']);

    //SOCIAL ACCOUNT ROUTES
    Route::get('social-accounts', ['uses' => 'InfluencerController@socialAccounts', 'as' => 'influencer.social.index']);

    //CATEGORIES ROUTES
    Route::get('categories/settings', ['uses' => 'InfluencerController@categories', 'as' => 'influencer.categories.index']);
    Route::post('categories/settings/update', ['uses' => 'InfluencerController@categoriesUpdate', 'as' => 'influencer.categories.update']);

});
