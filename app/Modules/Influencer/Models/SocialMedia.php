<?php namespace App\Modules\Influencer\Models;

use Illuminate\Database\Eloquent\Model;

class SocialMedia extends Model {

	protected $table = 'social_media_account';

	protected $primaryKey = 'social_account_id';

    protected $fillable = ['profile_id', 'provider_name', 'provider_id', 'provider_key', 'profile_url'];

    /**
     * Get the profile of the social media.
     */
	public function profile()
	{
		return $this->belongsTo('App\Modules\User\Models\UserProfile', 'profile_id', 'profile_id');
	}

}
