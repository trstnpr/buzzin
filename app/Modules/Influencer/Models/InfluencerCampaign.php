<?php namespace App\Modules\Influencer\Models;

use Illuminate\Database\Eloquent\Model;

class InfluencerCampaign extends Model {

	protected $table = 'influencer_campaign';

	protected $primaryKey = 'blogger_campaign_id';

    protected $fillable = ['profile_id', 'campaign_id', 'generated_url', 'view_count', 'type', 'status'];

    public function profile()
    {
        return $this->belongsTo('App\Modules\User\Models\UserProfile', 'profile_id', 'profile_id');
    }

    public function campaign()
    {
    	return $this->belongsTo('App\Modules\Campaign\Models\Campaign', 'campaign_id', 'campaign_id');
    }

}
