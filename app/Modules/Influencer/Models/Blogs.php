<?php namespace App\Modules\Influencer\Models;

use Illuminate\Database\Eloquent\Model;

class Blogs extends Model {

	protected $table = 'blogs';

	protected $primaryKey = 'blog_id';

    protected $fillable = ['profile_id', 'blog_name', 'blog_url', 'category_id', 'google_view_id', 'google_project_id'];

    /**
     * Get the profile of the social media.
     */
	public function profile()
	{
		return $this->belongsTo('App\Modules\User\Models\UserProfile', 'profile_id', 'profile_id');
	}

	/**
     * Get the profile of the social media.
     */
	public function category()
	{
		return $this->hasOne('App\Modules\User\Models\Category', 'category_id', 'category_id');
	}

}
