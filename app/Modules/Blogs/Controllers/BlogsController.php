<?php namespace App\Modules\Blogs\Controllers;

use App\Http\Controllers\Controller;

class BlogsController extends Controller
{

    public function dashboard()
    {
        return view('Blogs::index');
    }

}
