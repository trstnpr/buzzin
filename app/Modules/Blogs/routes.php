<?php

Route::group(array('module' => 'Blogs', 'namespace' => 'App\Modules\Blogs\Controllers'), function () {

    Route::get('blogs/dashboard', 'BlogsController@dashboard');

});
