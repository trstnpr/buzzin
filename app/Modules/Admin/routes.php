<?php

Route::group(array('module' => 'Admin', 'namespace' => 'App\Modules\Admin\Controllers', 'middleware' => ['web']), function() {

    // Route::resource('admin', 'AdminController');
    Route::get('admin', ['uses' => 'AdminController@adminLogin', 'as' => 'admin']);
    Route::post('admin/login', ['uses' => 'AdminController@adminLoginProcess', 'as' => 'admin.login']);
    Route::get('admin/logout', ['uses' => 'AdminController@adminLogout', 'as' => 'admin.logout']);

    Route::get('admin/dashboard', ['uses' => 'AdminController@index', 'as' => 'admin.dashboard', 'middleware' => ['auth', 'admin']]);

    /*Campaign Routes*/
    Route::get('admin/campaign', ['uses' => 'CampaignAdminController@campaign', 'as' => 'admin.campaign', 'middleware' => ['auth', 'admin']]);
    Route::get('admin/campaign/{id}', ['uses' => 'CampaignAdminController@eshowCampaign', 'as' => 'admin.campaign.show', 'middleware' => ['auth', 'admin']]);
    Route::get('admin/campaign/{id}/edit', ['uses' => 'CampaignAdminController@editCampaign', 'as' => 'admin.campaign.edit', 'middleware' => ['auth', 'admin']]);
    Route::post('admin/campaign/{id}/update', ['uses' => 'CampaignAdminController@updateCampaign', 'as' => 'admin.campaign.update', 'middleware' => ['auth', 'admin']]);
    Route::delete('admin/campaign/{id}', ['uses' => 'CampaignAdminController@destroy', 'as' => 'admin.campaign.delete', 'middleware' => ['auth', 'admin']]);

    /*Business Routes*/
    Route::get('admin/business', ['uses' => 'BusinessController@business', 'as' => 'admin.business', 'middleware' => ['auth', 'admin']]);
    Route::get('admin/business/{id}', ['uses' => 'BusinessController@editBusiness', 'as' => 'admin.business.edit', 'middleware' => ['auth', 'admin']]);
    Route::post('admin/business/{id}/update', ['uses' => 'BusinessController@businessUpdate', 'as' => 'admin.business.update', 'middleware' => ['auth', 'admin']]);
    Route::delete('admin/business/{id}', ['uses' => 'BusinessController@deleteBusiness', 'as' => 'admin.business.delete', 'middleware' => ['auth', 'admin']]);

    /*Influencer Routes*/
    Route::get('admin/influencer', ['uses' => 'InfluencerAdminController@influencer', 'as' => 'admin.influencer', 'middleware' => ['auth', 'admin']]);
    Route::get('admin/influencer/{id}', ['uses' => 'InfluencerAdminController@editInfluencer', 'as' => 'admin.influencer.edit', 'middleware' => ['auth', 'admin']]);
    Route::post('admin/influencer/{id}/update', ['uses' => 'InfluencerAdminController@influencerUpdate', 'as' => 'admin.influencer.update', 'middleware' => ['auth', 'admin']]);
    Route::delete('admin/influencer/{id}', ['uses' => 'InfluencerAdminController@deleteInfluencer', 'as' => 'admin.influencer.delete', 'middleware' => ['auth', 'admin']]);

    /*Announcement Routes*/
    Route::get('admin/announcement', ['uses' => 'AnnouncementController@announcement', 'as' => 'admin.announcement', 'middleware' => ['auth', 'admin']]);
    Route::get('admin/announcement/getAllAnnouncement', ['uses' => 'AnnouncementController@getAllAnnouncement', 'as' => 'admin.announcement.getAll', 'middleware' => ['auth', 'admin']]);
    Route::get('admin/announcement/create', ['uses' => 'AnnouncementController@createAnnouncement', 'as' => 'admin.announcement.create', 'middleware' => ['auth', 'admin']]);
    Route::post('admin/announcement/store', ['uses' => 'AnnouncementController@announcementStore', 'as' => 'admin.announcement.store', 'middleware' => ['auth', 'admin']]);
    Route::get('admin/announcement/{announcement_id}', ['uses' => 'AnnouncementController@editAnnouncement', 'as' => 'admin.announcement.edit', 'middleware' => ['auth', 'admin']]);
    Route::post('admin/announcement/{announcement_id}/update', ['uses' => 'AnnouncementController@announcementUpdate', 'as' => 'admin.announcement.update', 'middleware' => ['auth', 'admin']]);
    Route::delete('admin/announcement/{announcement_id}', ['uses' => 'AnnouncementController@announcementDestroy', 'as' => 'admin.announcement.destroy', 'middleware' => ['auth', 'admin']]);

});	