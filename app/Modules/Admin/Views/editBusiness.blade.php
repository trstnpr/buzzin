@extends('adminMaster')

@section('title')
	<title>Edit Campaign</title>
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/company/views/index.css') }}" rel="stylesheet">
@stop

@section('content')
	
	<div class="settings-content">
		<div class="container">
			
			<h5 class="page-title">Edit Business</h5>

			<div class="section-content">
				<form class="profileSetting-form" id="form_update_business" action="{{ route('admin.business.update', $company['company_id']) }}">
					<input type="hidden" id="_token" value="{{ csrf_token() }}">
					<div class="row">
						<div class="col l10 s12">
							<div class="card teal hide" id="result_div">
						        <i class="material-icons">check</i> Updated successfully!
						    </div>
							<div class="panel">
								<div class="row">
									<div class="col s12">
										<div class="form-group">
											<label>Company</label>
											<input type="text" class="form-control b2i-field" name="company_name" value="{{$company['company_name']}}" required=""  />
										</div>
									</div>
									<div class="col l6 s12">
										<div class="form-group">
											<label>Firstname</label>
											<input type="text" class="form-control b2i-field" value="{{ $profile['first_name']}}" name="first_name" required=""  />
										</div>
									</div>
									<div class="col l6 s12">
										<div class="form-group">
											<label>Lastname</label>
											<input type="text" class="form-control b2i-field" value="{{ $profile['last_name']}}" name="last_name" required=""  />
										</div>
									</div>
									
									<div class="col s12">
										<div class="form-group">
											<label>Website</label>
											<input type="text" class="form-control b2i-field" name="company_website" value="{{$company['company_website']}}" />
										</div>
									</div>
									<div class="col s12">
										<div class="form-group">
											<label>Brand Name</label>
											<input type="text" class="form-control b2i-field" name="brand_name" value="{{$company['brand_name']}}" />
										</div>
									</div>
									<div class="col s12">
										<div class="form-group">
											<label>Contact Number</label>
											<input type="text" class="form-control b2i-field" name="company_contact_no" value="{{$company['company_contact_no']}}" required=""  />
										</div>
									</div>
									<div class="col l5 s12">
										<div class="form-group">
											<label>Company Address 1</label>
											<input type="text" class="form-control b2i-field" name="company_address_1" value="{{$company['company_address_1']}}" required=""  />
										</div>
									</div>
									<div class="col l4 s12">
										<div class="form-group">
											<label>Company Address 2</label>
											<input type="text" class="form-control b2i-field" name="company_address_2" value="{{$company['company_address_2']}}" />
										</div>
									</div>
									<div class="col l3 s12">
										<div class="form-group">
											<label>City</label>
											<input type="text" class="form-control b2i-field" name="company_city" value="{{$company['company_city']}}" required=""  />
										</div>
									</div>
									<div class="col l5 s12">
										<div class="form-group">
											<label>State</label>
											<input type="text" class="form-control b2i-field" name="company_state" value="{{$company['company_state']}}" />
										</div>
									</div>
									<div class="col l4 s12">
										<div class="form-group">
											<label>Zip</label>
											<input type="text" class="form-control b2i-field" name="company_zip" value="{{$company['company_zip']}}" />
										</div>
									</div>
									<div class="col l3 s12">
										<div class="form-group">
											<label>Country</label>
											<input type="text" class="form-control b2i-field" name="company_country" value="{{$company['company_country']}}" required=""  />
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="col l2 s12">
							<div class="action-buttons">
								<button class="btn btn-large waves-effect waves-light col s12" type="submit" id="btn_update_business">update</button>
								<a href="{{ route('admin.business') }}" class="btn btn-large waves-effect waves-light red col s12">Cancel</a>
							</div>
						</div>
					</div>
				</form>
			</div>

		</div>
	</div>

	<input type="hidden" id="comp_id" value="{{$company['company_id']}}">
	<input type="hidden" id="profile_id" value="{{$company['profile_id']}}">

@stop


@section('footer')
	<script src="{{ config('s3.bucket_link') . elixir('assets/admin/views/index.js') }}"></script>
	<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			tinymce.init({ selector:'.wysiwyg' });
		});
		
		$(document).ready(function(){
			$('#form_update_business').on('submit', (function(e){
		        e.preventDefault();
		        $.ajax({
		            url: $(this).attr('action'),
		            type: "POST",
		            headers:
		            {
		                'X-CSRF-Token': $('#_token').val()
		            },
		            data: new FormData(this),
		            contentType: false,
		            cache: false,
		            processData: false,
		            beforeSend: function(){ $('#btn_update_business').html('Processing...');},
		            error: function(data){
		                if(data.readyState == 4){
		                    errors = JSON.parse(data.responseText);
		                    $('#result_div').empty();
		                    $('#result_div').html('<ul class="error_list fa-ul white-text"></ul>');
		                    $.each(errors,function(key,value){
		                        $('.error_list').append('<li><i class="fa fa-li fa-times"></i> '+value+'</li>');
		                    });
		                    $('#result_div').removeClass('teal hide').addClass('red');
		                    $('#btn_update_business').html('Submit');

		                    setTimeout(function(){$('#result_div').addClass('hide').html(''); }, 4000);
		                }
		            },

		            success: function(data){
		                var msg = JSON.parse(data);
		                if(msg.result == 'success'){
		                    Materialize.toast(msg.message, 1000, "green", function(){ window.location.href = '{{route('admin.business')}}'; });
		                } else{
		                    Materialize.toast(msg.message, 4000, 'red');
		                }
		            }
		        });
		    }));
		});
	</script>
@stop