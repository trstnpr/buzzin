@extends('adminMaster')

@section('title')
	<title>Edit Influencer</title>
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/company/views/index.css') }}" rel="stylesheet">
@stop

@section('content')
	
	<div class="settings-content">
		<div class="container">
			
			<h5 class="page-title">Edit Influencer</h5>

			<div class="section-content">
				<form id="form_update_influencer" action="{{ route('admin.influencer.update', $influencer['profile_id']) }}">
					<input type="hidden" id="_token" id="csrf-token" value="{{ Session::token() }}" />
					<div class="row">
						<div class="col l10 s12">
							<div class="panel">
								<div class="row">
									<div class="col s12">
										<div class="form-group">
											<label>Organization Name</label>
											<input type="text" name="organization_name" required="required" class="form-control b2i-field" value="{{ $influencer['organization_name'] }}" />
										</div>
									</div>
									<div class="col s6">
										<div class="form-group">
											<label>First Name</label>
											<input type="text" name="first_name" required="required" class="form-control b2i-field" value="{{ $influencer['first_name'] }}" />
										</div>
									</div>
									<div class="col s6">
										<div class="form-group">
											<label>Last Name</label>
											<input type="text" name="last_name" required="required" class="form-control b2i-field" value="{{ $influencer['last_name'] }}" />
										</div>
									</div>
									<div class="col s12">
										<div class="form-group">
											<label>About Me</label>
											<textarea type="text" name="about_blogger" class="wysiwyg">{{ $influencer['about_blogger'] }}</textarea>
										</div>
									</div>
									<div class="col s6">
										<div class="form-group">
											<label>Website</label>
											<input type="text" name="website" required="required" class="form-control b2i-field" value="{{ $influencer['website'] }}" />
										</div>
									</div>
									<div class="col s6">
										<div class="form-group">
											<label>Blogger Pricing</label>
											<input type="text" name="blogger_pricing" required="required" class="form-control b2i-field" value="{{ $influencer['blogger_pricing'] }}" />
										</div>
									</div>
									<div class="col s12">
										<div class="form-group">
											<label>Contact Number</label>
											<input type="text" name="contact_number" required="required" class="form-control b2i-field" value="{{ $influencer['contact_number'] }}" />
										</div>
									</div>
									<div class="col s4">
										<div class="form-group">
											<label>Address 1</label>
											<input type="text" name="address_1" required="required" class="form-control b2i-field" value="{{ $influencer['address_1'] }}" />
										</div>
									</div>
									<div class="col s4">
										<div class="form-group">
											<label>Address 2 (optional)</label>
											<input type="text" name="address_2" class="form-control b2i-field" value="{{ $influencer['address_2'] }}" />
										</div>
									</div>
									<div class="col s4">
										<div class="form-group">
											<label>City</label>
											<input type="text" name="city" required="required" class="form-control b2i-field" value="{{ $influencer['city'] }}" />
										</div>
									</div>
									<div class="col s4">
										<div class="form-group">
											<label>State / Province</label>
											<input type="text" name="state" required="required" class="form-control b2i-field" value="{{ $influencer['state'] }}" />
										</div>
									</div>
									<div class="col s4">
										<div class="form-group">
											<label>Zip Code (optional)</label>
											<input type="text" name="zip_code" class="form-control b2i-field" value="{{ $influencer['zip_code'] }}" />
										</div>
									</div>
									<div class="col s4">
										<div class="form-group">
											<label>Country</label>
											<input type="text" name="country" required="required" class="form-control b2i-field" value="{{ $influencer['country'] }}" />
										</div>
									</div>
									
								</div>
							</div>
						</div>

						<div class="col l2 s12">
							<div class="action-buttons">
								<button type="submit" id="profile-button" class="btn btn-large waves-effect waves-light col s12" style="margin-bottom: 10px;">update</button>
								<a href="{{ route('admin.influencer') }}" class="btn btn-large waves-effect waves-light red col s12">cancel</a>
							</div>
						</div>
					</div>
				</form>
			</div>

		</div>
	</style>

@stop


@section('footer')
	<script src="{{ config('s3.bucket_link') . elixir('assets/admin/views/index.js') }}"></script>
	<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			tinymce.init({ selector:'.wysiwyg' });
		});
		
		$(document).ready(function(){
			$('#form_update_influencer').on('submit', (function(e){
		        e.preventDefault();
		        $.ajax({
		            url: $(this).attr('action'),
		            type: "POST",
		            headers:
		            {
		                'X-CSRF-Token': $('#_token').val()
		            },
		            data: new FormData(this),
		            contentType: false,
		            cache: false,
		            processData: false,
		            beforeSend: function(){ $('#profile-button').html('Processing...');},
		            error: function(data){
		                if(data.readyState == 4){
		                    errors = JSON.parse(data.responseText);
		                    $('#result_div').empty();
		                    $('#result_div').html('<ul class="error_list fa-ul white-text"></ul>');
		                    $.each(errors,function(key,value){
		                        $('.error_list').append('<li><i class="fa fa-li fa-times"></i> '+value+'</li>');
		                    });
		                    $('#result_div').removeClass('teal hide').addClass('red');
		                    $('#profile-button').html('Submit');

		                    setTimeout(function(){$('#result_div').addClass('hide').html(''); }, 4000);
		                }
		            },

		            success: function(data){
		                var msg = JSON.parse(data);
		                if(msg.result == 'success'){
		                    Materialize.toast(msg.message, 1000, "green", function(){ window.location.href = '{{route('admin.influencer')}}'; });
		                } else{
		                    Materialize.toast(msg.message, 4000, 'red');
		                }
		            }
		        });
		    }));
		});
	</script>
@stop