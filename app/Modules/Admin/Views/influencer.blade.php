@extends('adminMaster')

@section('title')
	<title>B2I Admin</title>
@stop

@section('meta')
	{{-- Put Seo tags Here... --}}
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/admin/views/index.css') }}" rel="stylesheet">
	{{-- <link rel="stylesheet" type="text/css" href="{{ config('s3.bucket_link') . elixir('assets/admin/jquery.dataTables.min.css') }}"> --}}
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
@stop

@section('content')

	<div class="influencer-content">
		<div class="container">
			<h5 class="page-title">Influencers</h5>
			<div class="section-content">
				<div class="panel influencer-panel">
					<table class="responsive-table datatable striped highlight" cellspacing="0" width="100%">
						<thead class="yellow lighten-3">
				            <tr>
				                <th>Influencer</th>
				                <th>Blog URL</th>
				                <th>Email Address</th>
				                <th>Mobile Number</th>
				                <th>Joined</th>
				                <th></th>
				            </tr>
				        </thead>
				        <tfoot class="yellow lighten-3">
				            <tr>
				                <th>Influencer</th>
				                <th>Blog URL</th>
				                <th>Email</th>
				                <th>Number</th>
				                <th>Joined</th>
				                <th><center>Actions</center></th>
				            </tr>
				        </tfoot>
				        <tbody>
				        	@if($influencer->count() == 0)
				        	<tr>
				        		<td colspan="6" align="center">No Campaigns Posted.</td>
				        	</tr>
				        	@else
				        	@foreach($influencer as $c)
				            <tr>
				                <td>{{ str_limit($c->profile->first_name.' '.$c->profile->last_name, 15)}}</td>
				                <td>{{ $c->profile->website }}</td>
				                <td>{{ $c->email }}</td>
				                <td>{{ $c->profile->contact_number }}</td>
				                <td>{{ $c->profile->created_at->format('M d, Y') }}</td>
				                <td class="action">
				                	<button id="btn-view-influencer" type="button" class="btn btn-primary blue btn-circle white-text btn-view-influencer" title="View" data-toggle="modal" data-target="#view-influencer"
				                        onclick="viewInfluencer(this)"
				                        data-first-name="{{ $c->profile->first_name }} {{ $c->profile->last_name }}"
				                        data-email="{{ $c->email }}"
				                        data-last-name="{{ $c->profile->last_name }}"
				                        data-organization-name="{{ $c->profile->organization_name }}"
				                        data-blogger-price="{{ $c->profile->blogger_pricing }}"
				                        data-blogger-website="{{ $c->profile->website }}"
				                        data-blogger-about="{{ $c->profile->about_blogger }}"
				                        data-blogger-address="{{ $c->profile->address_1 }}"
				                        data-blogger-city="{{ $c->profile->city }}"
				                        data-blogger-state="{{ $c->profile->state }}"
				                        data-blogger-country="{{ $c->profile->country }}"
				                        data-blogger-contact="{{ $c->profile->contact_number }}">
				                        <i class="material-icons">search</i>
				                    </button>
				                	<a href="{{ route('admin.influencer.edit', $c->profile->profile_id) }}" class="btn"><i class="material-icons">edit</i></a>
				                	<button id="btn-delete-influencer" type="button" class="btn btn-danger red btn-circle red btn-delete-influencer modal-trigger" title="Delete" data-toggle="modal" data-target="#delete-influencer"
				                        onclick="deleteInfluencer(this)"
				                        data-influencer-id="{{$c->profile->profile_id}}">
				                        <i class="material-icons">delete</i>
				                    </button>
				                </td>
				            </tr>
				        	@endforeach
				        	@endif
				        </tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

	<div id="delete-influencer" class="modal">
        <div class="modal-content">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <h5>Delete Influencer</h5>
            Do you want to delete this influencer?
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action modal-close btn green waves-effect waves-green btn-delete-yes-influencer">Yes</a>
            <a href="#!" class="modal-action modal-close btn red waves-effect waves-red">No</a>
        </div>
    </div>

	<div id="view-influencer" class="modal modal-fixed-footer">
	    <div class="modal-content">
	    	<div class="modal-header-influencer">
	    		<h5>Name: <span id="name"></span></h5>
	    	</div>

	        <ul class="collection with-header">
	        	<li class="collection-item"><div>Organization Name: <span id="o_name"></span></div><br /></li>
	            <li class="collection-item"><div>Email: <span id="b_email"></span></div><br /></li>
	            <li class="collection-item"><div>Blogger Pricing: <span id="b_price"></span></div><br /></li>
	            <li class="collection-item"><div>Website: <span id="b_website"></span></div><br /></li>
	            <li class="collection-item"><div>Address: <span id="b_address"></span></div><br /></li>
	            <li class="collection-item"><div>City: <span id="b_city"></span></div><br /></li>
	            <li class="collection-item"><div>State: <span id="b_state"></span></div><br /></li>
	            <li class="collection-item"><div>Country: <span id="b_country"></span></div><br /></li>
	            <li class="collection-item"><div>Contact: <span id="b_contact"></span></div><br /></li>
	            <li class="collection-item"><div>About Blogger: <span id="b_about"></span></div><br /></li>
	        </ul>
	    </div>
	    <div class="modal-footer view-influencer">
	        <a href="#!" class="modal-action modal-close waves-effect waves-red btn">Close</a>
	    </div>
	</div>

@stop

@section('footer')
	<script src="{{ config('s3.bucket_link') . elixir('assets/admin/views/index.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('.datatable').DataTable();
		});

		function deleteInfluencer(influencer) {
            $('#delete-influencer').modal('open');
            $('.btn-delete-yes-influencer').attr('data-influencer-id', $(influencer).attr('data-influencer-id'));
        }

		function viewInfluencer(influencer) {
	        $('#view-influencer').modal('open');
	        $('#name').html($(influencer).attr('data-first-name'));
	        $('#o_name').html($(influencer).attr('data-organization-name'));
	        $('#b_email').html($(influencer).attr('data-email'));
	        $('#b_price').html($(influencer).attr('data-blogger-price'));
	        $('#b_website').html($(influencer).attr('data-blogger-website'));
	        $('#b_address').html($(influencer).attr('data-blogger-address'));
	        $('#b_city').html($(influencer).attr('data-blogger-city'));
	        $('#b_state').html($(influencer).attr('data-blogger-state'));
	        $('#b_country').html($(influencer).attr('data-blogger-country'));
	        $('#b_contact').html($(influencer).attr('data-blogger-contact'));
	        $('#b_about').html($(influencer).attr('data-blogger-about'));
	    }

	</script>
@stop
