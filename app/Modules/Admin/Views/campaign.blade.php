@extends('adminMaster')

@section('title')
	<title>B2I Admin</title>
@stop

@section('meta')
	{{-- Put Seo tags Here... --}}
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/admin/views/index.css') }}" rel="stylesheet">
	{{-- <link rel="stylesheet" type="text/css" href="{{ config('s3.bucket_link') . elixir('assets/admin/jquery.dataTables.min.css') }}"> --}}
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
@stop

@section('content')

	<div class="campaign-content">
		<div class="container">

			<h5 class="page-title">Campaign</h5>
			<div class="section-content">
				<div class="nav-tabs">
					<ul class="tabs">
					    <li class="tab col s6"><a class="active" href="#pub_campaign">Published Campaign</a></li>
					    <li class="tab col s6"><a class="" href="#unpub_campaign">Unpublished Campaign</a></li>
					</ul>
				</div>

				<div class="panel" id="pub_campaign">
                    <table class="responsive-table datatable striped highlight" cellspacing="0" width="100%">
                    	<thead class="yellow lighten-3">
				            <tr>
				            	<th>Company</th>
				                <th>Campaign</th>
				                <th>Category</th>
				                <th>Compensation</th>
				                <th>Date Start</th>
				                <th>Date End</th>
				                <th>Actions</th>
				            </tr>
				        </thead>
				        <tfoot class="yellow lighten-3">
				            <tr>
				            	<th>Company</th>
				                <th>Campaign</th>
				                <th>Category</th>
				                <th>Compensation</th>
				                <th>Date Start</th>
				                <th>Date End</th>
				                <th>Actions</th>
				            </tr>
				        </tfoot>
				        <tbody>
				        	@if($pubCampaigns->count() == 0)
				        	<tr>
				        		<td colspan="6" align="center">No Campaigns Posted.</td>
				        	</tr>
				        	@else
				        	@foreach($pubCampaigns as $c)
				            <tr>
				            	<td>{{ str_limit($c->profile->company['company_name'], 30) }}</td>
				                <td>{{ str_limit($c->campaign_name, 20) }}</td>
				                <td>{{ $c->category->category }}</td>
				                <td>{{ $c->fund_details }}</td>
				                <td>{{ $c->date_start->format('M d, Y') }}</td>
				                <td>{{ $c->date_end->format('M d, Y') }}</td>
				                <td class="action">
				                	<button id="btn-view-campaign" type="button" class="btn btn-primary blue btn-circle white-text btn-view-campaign" title="View" data-toggle="modal" data-target="#view-campaign"
				                        onclick="viewCampaign(this)"
				                        data-campaign-id="{{ $c->campaign_id }}"
				                        data-campaign-name="{{ $c->campaign_name }}"
				                        data-campaign-description="{{ $c->campaign_description }}"
				                        data-category="{{ $c->category_id }}"
				                        data-pot-money="{{ $c->pot_money }}"
				                         data-fund-type="{{ $c->fund_type }}"
				                        data-fund-details="{{ $c->fund_details }}"
				                        data-campaign-start="{{ date_format(date_create($c->date_start), 'M d, Y') }}"
				                        data-campaign-end="{{ date_format(date_create($c->date_end), 'M d, Y') }}">
				                        <i class="material-icons">search</i>
				                    </button>
				                	<a href="{{ route('admin.campaign.edit', $c->campaign_id) }}" class="btn"><i class="material-icons">edit</i></a>
				                	<button id="btn-delete-campaign" type="button" class="btn btn-danger red btn-circle red btn-delete-campaign modal-trigger" title="Delete" data-toggle="modal" data-target="#delete-campaign"
				                        onclick="deleteCampaign(this)"
				                        data-campaign-id="{{$c->campaign_id}}">
				                        <i class="material-icons">delete</i>
				                    </button>
				                </td>
				            </tr>
				        	@endforeach
				        	@endif
				        </tbody>
                    </table>
				</div>
				<div class="panel" id="unpub_campaign">
                    <table class="responsive-table datatable striped highlight" cellspacing="0" width="100%">
                    	<thead class="yellow lighten-3">
				            <tr>
				            	<th>Company</th>
				                <th>Campaign</th>
				                <th>Category</th>
				                <th>Compensation</th>
				                <th>Date Start</th>
				                <th>Date End</th>
				                <th>Actions</th>
				            </tr>
				        </thead>
				        <tfoot class="yellow lighten-3">
				            <tr>
				            	<th>Company</th>
				                <th>Campaign</th>
				                <th>Category</th>
				                <th>Compensation</th>
				                <th>Date Start</th>
				                <th>Date End</th>
				                <th>Actions</th>
				            </tr>
				        </tfoot>
				        <tbody>
				        	@if($unPubCampaigns->count() == 0)
				        	<tr>
				        		<td colspan="6" align="center">No Campaigns Posted.</td>
				        	</tr>
				        	@else
				        	@foreach($unPubCampaigns as $c)
				            <tr>
				            	<td>{{ str_limit($c->profile->company['company_name'], 30) }}</td>
				                <td>{{ str_limit($c->campaign_name, 30) }}</td>
				                <td>{{ $c->category->category }}</td>
				                <td>{{ $c->fund_details }}</td>
				                <td>{{ $c->date_start->format('M d, Y') }}</td>
				                <td>{{ $c->date_end->format('M d, Y') }}</td>
				                <td class="action">
				                	<button id="btn-view-campaign" type="button" class="btn btn-primary blue btn-circle white-text btn-view-campaign" title="View" data-toggle="modal" data-target="#view-campaign"
				                        onclick="viewCampaign(this)"
				                        data-campaign-id="{{ $c->campaign_id }}"
				                        data-campaign-name="{{ $c->campaign_name }}"
				                        data-campaign-description="{{ $c->campaign_description }}"
				                        data-category="{{ $c->category_id }}"
				                        data-pot-money="{{ $c->pot_money }}"
				                        data-fund-type="{{ $c->fund_type }}"
				                        data-fund-details="{{ $c->fund_details }}"
				                        data-campaign-start="{{ date_format(date_create($c->date_start), 'M d, Y') }}"
				                        data-campaign-end="{{ date_format(date_create($c->date_end), 'M d, Y') }}">
				                        <i class="material-icons">search</i>
				                    </button>
				                	<a href="{{ route('admin.campaign.edit', $c->campaign_id) }}" class="btn"><i class="material-icons">edit</i></a>
				                	<button id="btn-delete-campaign" type="button" class="btn btn-danger red btn-circle red btn-delete-campaign modal-trigger" title="Delete" data-toggle="modal" data-target="#delete-campaign"
				                        onclick="deleteCampaign(this)"
				                        data-campaign-id="{{$c->campaign_id}}">
				                        <i class="material-icons">delete</i>
				                    </button>
				                </td>
				            </tr>
				        	@endforeach
				        	@endif
				        </tbody>
                    </table>
				</div>
			</div>
		</div>
	</div>

	<div id="delete-campaign" class="modal">
        <div class="modal-content">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <h5>Delete Campaign</h5>
            Do you want to delete this campaign?
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action modal-close btn green waves-effect waves-green btn-delete-yes-campaign">Yes</a>
            <a href="#!" class="modal-action modal-close btn red waves-effect waves-red">No</a>
        </div>
    </div>

    <div id="view-campaign" class="modal modal-fixed-footer">
	    <div class="modal-content">
	    	<div class="modal-header-campaign">
	    		<h5>Campaign Name: <span id="name"></span></h5>
	    	</div>

	        <ul class="collection with-header">
	            <li class="collection-item"><div>Descripiton: <span id="desc"></span></div><br /></li>
	            <li class="collection-item"><div>Campaign Category: <span id="cat"></span></div><br /></li>
	            <li class="collection-item"><div>Asset Type: <span id="asset"></span></div><br /></li>
	            <li class="collection-item"><div>Campaign Budget / Offer: <span id="fund"></span></div><br /></li>
	            <li class="collection-item"><div>Date Start: <span id="date_start"></span></div><br /></li>
	            <li class="collection-item"><div>Date End: <span id="date_end"></span></div><br /></li>
	        </ul>
	    </div>
	    <div class="modal-footer view-campaign">
	        <a href="#!" class="modal-action modal-close waves-effect waves-red btn">Close</a>
	    </div>
	</div>
@stop

@section('footer')
	<script src="{{ config('s3.bucket_link') . elixir('assets/admin/views/index.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('.datatable').DataTable();
		});

		function deleteCampaign(campaign) {
            $('#delete-campaign').modal('open');
            $('.btn-delete-yes-campaign').attr('data-campaign-id', $(campaign).attr('data-campaign-id'));
        }

        function viewCampaign(campaign) {
	        $('#view-campaign').modal('open');
	        $('#name').html($(campaign).attr('data-campaign-name'));
	        $('#cat').html($(campaign).attr('data-category'));

	        if($(campaign).attr('data-fund-type') == 0){
	        	$('#asset').html("Monetary");
	        }
	        else{
	        	$('#asset').html("Non-monetary");
	        }

	        $('#fund').html($(campaign).attr('data-fund-details'));
	        $('#f_money').html($(campaign).attr('data-fund-money'));
	        $('#f_details').html($(campaign).attr('data-fund-details'));
	        $('#date_start').html($(campaign).attr('data-campaign-start'));
	        $('#date_end').html($(campaign).attr('data-campaign-end'));
	        $('#desc').html($(campaign).attr('data-campaign-description'));
	    }
	</script>
@stop
