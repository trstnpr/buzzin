@extends('adminMaster')

@section('title')
	<title>B2I Admin</title>
@stop

@section('meta')
	{{-- Put Seo tags Here... --}}
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/admin/views/index.css') }}" rel="stylesheet">
	{{-- <link rel="stylesheet" type="text/css" href="{{ config('s3.bucket_link') . elixir('assets/admin/jquery.dataTables.min.css') }}"> --}}
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
@stop

@section('content')

	<div class="business-content">
		<div class="container">
			<h5 class="page-title">Businesses</h5>
			<div class="section-content">
				<div class="panel business-panel">
					<table class="responsive-table datatable striped highlight" cellspacing="0" width="100%">
						<thead class="yellow lighten-3">
				            <tr>
				                <th>Contact Person</th>
				                <th>Company</th>
				                <th>Brand</th>
				                <th>Published Campaigns</th>
				                <th>Unpublished Campaigns</th>
				                <th>Date Joined</th>
				                <th>Actions</th>
				            </tr>
				        </thead>
				        <tfoot class="yellow lighten-3">
				            <tr>
				                <th>Contact</th>
				                <th>Company</th>
				                <th>Brand</th>
				                <th>Published</th>
				                <th>Unpublished</th>
				                <th>Joined</th>
				                <th>Actions</th>
				            </tr>
				        </tfoot>
				        <tbody>
				        	@if($business->count() == 0)
				        	<tr>
				        		<td colspan="6" align="center">No Campaigns Posted.</td>
				        	</tr>
				        	@else
				        	@foreach($business as $b)
				            <tr>
				                <td>{{ $b->profile->first_name.' '.$b->profile->last_name }}</td>
				                <td>{{ str_limit($b->profile->company['company_name'], 15) }}</td>
				                <td>{{ str_limit($b->profile->company['brand_name'], 5) }}</td>
				                <?php
$pub_count = App\Modules\Campaign\Models\Campaign::where('profile_id', $b->profile->profile_id)->where('status', 1)->count();
$unpub_count = App\Modules\Campaign\Models\Campaign::where('profile_id', $b->profile->profile_id)->where('status', 0)->count();
?>
				                <td>{{ $pub_count }}</td>
				                <td>{{ $unpub_count }}</td>
				                <td>{{ date_format(date_create($b->profile->company['created_at']), "M d, Y") }}</td>
				                <td class="action">
				                	<button id="btn-view-business" type="button" class="btn btn-primary blue btn-circle white-text btn-view-business" title="View" data-toggle="modal" data-target="#view-business"
				                        onclick="viewBusiness(this)"
				                        data-first-name="{{ $b->profile->first_name }} {{ $b->profile->last_name }}"
				                        data-last-name="{{ $b->profile->last_name }}"
				                        data-email="{{ $b->email }}"
				                        data-company-name="{{ $b->profile->company['company_name'] }}"
				                        data-brand-name="{{ $b->profile->company['brand_name'] }}"
				                        data-company-website="{{ $b->profile->company['company_website'] }}"
				                        data-company-contact="{{ $b->profile->company['company_contact_no'] }}"
				                        data-company-address="{{ $b->profile->company['company_address_1'] }}"
				                        data-company-city="{{ $b->profile->company['company_city'] }}"
				                        data-company-state="{{ $b->profile->company['company_state'] }}"
				                        data-company-zip="{{ $b->profile->company['company_zip'] }}"
				                        data-company-country="{{ $b->profile->company['company_country'] }}"
				                        >
				                        <i class="material-icons">search</i>
				                    </button>
				                	<a href="{{ route('admin.business.edit', $b->profile->company['company_id']) }}" class="btn"><i class="material-icons">edit</i></a>
				                	<button id="btn-delete-business" type="button" class="btn btn-danger red btn-circle red btn-delete-business modal-trigger" title="Delete" data-toggle="modal" data-target="#delete-business"
				                        onclick="deleteBusiness(this)"
				                        data-business-id="{{$b->profile->profile_id}}">
				                        <i class="material-icons">delete</i>
				                    </button>
				                </td>
				            </tr>
				        	@endforeach
				        	@endif
				        </tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

	<div id="delete-business" class="modal">
        <div class="modal-content">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <h5>Delete Business</h5>
            Do you want to delete this business?
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action modal-close btn green waves-effect waves-green btn-delete-yes-business">Yes</a>
            <a href="#!" class="modal-action modal-close btn red waves-effect waves-red">No</a>
        </div>
    </div>

	<div id="view-business" class="modal modal-fixed-footer">
	    <div class="modal-content">
	    	<div class="modal-header-business">
	    		<h5>Name: <span id="name"></span></h5>
	    	</div>

	        <ul class="collection with-header">
	        	<li class="collection-item"><div>Contact Number: <span id="c_number"></span></div><br /></li>
	        	<li class="collection-item"><div>Email: <span id="email"></span></div><br /></li>
	            <li class="collection-item"><div>Company Name: <span id="c_name"></span></div><br /></li>
	            <li class="collection-item"><div>Brand Name: <span id="b_name"></span></div><br /></li>
	            <li class="collection-item"><div>Company Website: <span id="c_website"></span></div><br /></li>
	            <li class="collection-item"><div>Company Contact Number: <span id="c_number"></span></div><br /></li>
	            <li class="collection-item"><div>Company Address: <span id="c_address"></span></div><br /></li>
	            <li class="collection-item"><div>Company City: <span id="c_city"></span></div><br /></li>
	            <li class="collection-item"><div>Company State: <span id="c_state"></span></div><br /></li>
	            <li class="collection-item"><div>Company Zip: <span id="c_zip"></span></div><br /></li>
	            <li class="collection-item"><div>Company Country: <span id="c_country"></span></div><br /></li>
	        </ul>
	    </div>
	    <div class="modal-footer view-business">
	        <a href="#!" class="modal-action modal-close waves-effect waves-red btn">Close</a>
	    </div>
	</div>

@stop

@section('footer')
	<script src="{{ config('s3.bucket_link') . elixir('assets/admin/views/index.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('.datatable').DataTable();
		});

		function deleteBusiness(business) {
            $('#delete-business').modal('open');
            $('.btn-delete-yes-business').attr('data-business-id', $(business).attr('data-business-id'));
        }

		function viewBusiness(business) {
	        $('#view-business').modal('open');
	        $('#name').html($(business).attr('data-first-name'));
	        $('#c_name').html($(business).attr('data-company-name'));
	        $('#email').html($(business).attr('data-email'));
	        $('#b_name').html($(business).attr('data-brand-name'));
	        $('#c_website').html($(business).attr('data-company-website'));
	        $('#c_number').html($(business).attr('data-company-contact'));
	        $('#c_address').html($(business).attr('data-company-address'));
	        $('#c_city').html($(business).attr('data-company-city'));
	        $('#c_state').html($(business).attr('data-company-state'));
	        $('#c_zip').html($(business).attr('data-company-zip'));
	      	$('#c_country').html($(business).attr('data-company-country'));
	    }
	</script>
@stop
