@extends('adminMaster')

@section('title')
	<title>Edit Campaign</title>
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/company/views/index.css') }}" rel="stylesheet">
@stop

@section('content')
	
	<div class="campaign-content">
		<div class="container">
			
			<h5 class="page-title">Edit Campaign</h5>

			<div class="section-content">
				<form class="campaignPost-form" id="form_update_campaign" enctype="multipart/form-data" action="{{ route('admin.campaign.update', $campaign['campaign_id']) }}">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="row">
						<div class="col l10 s12">
							<div class="card teal hide" id="result_div">
						        <i class="material-icons">check</i> Updated successfully!
						    </div>
							<div class="panel">
								<div class="row">
									<div class="col s12">
										<div class="form-group">
											<label>Campaign</label>
											<input type="text" class="form-control b2i-field" name="campaign_name" value="{{$campaign['campaign_name']}}" />
										</div>
									</div>
									<div class="col s12">
										<div class="form-group">
											<label>Campaign URL</label>
											<input type="text" class="form-control b2i-field" name="campaign_url" value="{{$campaign['campaign_url']}}" />
										</div>
									</div>
									<div class="col s12">
										<div class="form-group">
											<label>Category</label>
											<select class="form-control b2i-field" name="category_id">
												<option disabled selected>Choose Category</option>
												@foreach($category as $c)
						                            <option value="{{$c['category_id']}}" {{ ($c['category_id'] == $campaign['category_id']) ? 'selected' : '' }}>{{$c['category']}}</option>
						                        @endforeach
											</select>
										</div>
									</div>

									<div class="col s12 l10">
										<div class="form-group">
											<label>Campaign Image</label>
											<div class="file-control">
												<input type="file" name="image_url" />
											</div>
										</div>
									</div>

									<div class="col l2 hide-on-med-and-down">
							            <small style="margin-top: -5px !important; display: block;">Current Image</small>
										<img src="{{ config('s3.bucket_link') . config('cdn.campaign') .'/'. $campaign['campaign_image']['image_url'] }}" alt="" class="responsive-img center-block" />
									</div>

									<div class="col s12">
										<div class="form-group">
											<label>Attach Campaign File</label>
											<div class="file-control">
												<input type="file" name="file_url"/>
											</div>
										</div>
									</div>
									
									<div class="col s12 hide-on-med-and-down">
							            <small style="margin-top: -5px !important; display: block;">Current File</small>
							            <a id="statement" href="{{ config('s3.bucket_link') . config('cdn.campaignFile') .'/'. $campaign['campaign_id'] .'/'. $campaign['file_url'] }}" target="_blank" data-toggle="tooltip" title="Download">{{ $campaign['file_url'] }}</a>
									</div>
									
									<div class="col s12">
										<div class="form-group">
											<label>Description</label>
											<textarea class="wysiwyg" name="campaign_description">
												{{$campaign['campaign_description']}}
											</textarea>
										</div>
									</div>

									<div class="col s12">
										<div class="form-group">
											<label>Mode of Compensation</label>
											<select class="form-control b2i-field" id="budget" name="fund_type">
												<option disabled selected>Choose</option>
												<option value="0" {{ ($campaign['fund_type'] == 0) ? 'selected' : '' }}> Monetary </option>
												<option value="1" {{ ($campaign['fund_type'] == 1) ? 'selected' : '' }}> Non-monetary </option>
											</select>
										</div>
									</div>

									<div class="col s12" id="campaign_budget">
										<div class="form-group">
											<label id="camp">Campaign Budget</label>
											<input type="text" class="form-control b2i-field" name="fund_details" value="{{$campaign['fund_details']}}"/>
										</div>
									</div>

									<div class="col s12">
										<div class="form-group">
											<input type="radio" id="test1" class="with-gap" value="1" {{ ($campaign['status'] == 1) ? 'checked' : '' }} name="status" />
	      									<label for="test1">Publish Now</label>

	      									<input type="radio" id="test2" class="with-gap" value="0" {{ ($campaign['status'] == 0) ? 'checked' : '' }} name="status" />
	      									<label for="test2">Save and Edit Later</label>
										</div>
									</div>

									<div class="col m6 s12">
										<div class="form-group">
											<label>Date Start</label>
											<input type="date" class="form-control b2i-field datepicker" name="date_start" value="{{ date_format(date_create($campaign['date_start']), 'd F, Y') }}"/>
										</div>
									</div>
									<div class="col m6 s12">
										<div class="form-group">
											<label>Date End</label>
											<input type="date" class="form-control b2i-field datepicker" name="date_end" value="{{ date_format(date_create($campaign['date_end']), 'd F, Y') }}"/>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="col l2 s12">
							<div class="action-buttons">
								<button class="btn btn-large waves-effect waves-light col s12" type="submit" id="btn_update_campaign" date-campaign-id="{{$campaign['campaign_id']}}">update</button>
								<a href="{{ url('/admin/campaign') }}" class="btn btn-large waves-effect waves-light red col s12">
									CANCEL
								</a>
							</div>
						</div>
					</div>
				</form>
			</div>

		</div>
	</div>

	<input type="hidden" id="c_id" value="{{$campaign['campaign_id']}}">
	<input type="hidden" id="c_view" value="{{$campaign['generated_url']}}">

@stop

@section('footer')
	<script src="{{ config('s3.bucket_link') . elixir('assets/admin/views/index.js') }}"></script>
	<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			tinymce.init({ selector:'.wysiwyg' });

			if($("#budget").val() == 1){
		    	$('#camp').html("Offer");
		    }
		    else{
		    	$('#camp').html("Campaign Budget");
		    }
		});

		$('.datepicker').pickadate({
			selectMonths: true,
			selectYears: 15
		});
		
		$(function() {
		    $("#budget").on('change', function() {
		        if($("#budget").val() == 1){
		        	$('#camp').html("Offer");
		        }
		        else{
		        	$('#camp').html("Campaign Budget");
		        }
		    });
		});

		$(document).ready(function(){
			$('#form_update_campaign').on('submit', (function(e){
		        e.preventDefault();
		        $.ajax({
		            url: $(this).attr('action'),
		            type: "POST",
		            headers:
		            {
		                'X-CSRF-Token': $('input[name="_token"]').val()
		            },
		            data: new FormData(this),
		            contentType: false,
		            cache: false,
		            processData: false,
		            beforeSend: function(){ $('#btn_update_campaign').html('Processing...');},
		            error: function(data){
		                if(data.readyState == 4){
		                    errors = JSON.parse(data.responseText);
		                    $('#result_div').empty();
		                    $('#result_div').html('<ul class="error_list fa-ul white-text"></ul>');
		                    $.each(errors,function(key,value){
		                        $('.error_list').append('<li><i class="fa fa-li fa-times"></i> '+value+'</li>');
		                    });
		                    $('#result_div').removeClass('teal hide').addClass('red');
		                    $('#btn_update_campaign').html('Submit');

		                    setTimeout(function(){$('#result_div').addClass('hide').html(''); }, 4000);
		                }
		            },

		            success: function(data){
		                var msg = JSON.parse(data);
		                if(msg.result == 'success'){
		                    Materialize.toast(msg.message, 1000, "green", function(){ window.location.href = '{{route('admin.campaign')}}'; });
		                } else{
		                    Materialize.toast(msg.message, 4000, 'red');
		                }
		            }
		        });
		    }));
		});
	</script>
@stop