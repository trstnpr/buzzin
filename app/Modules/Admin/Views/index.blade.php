@extends('adminMaster')

@section('title')
	<title>B2I Admin</title>
@stop

@section('meta')
	{{-- Put Seo tags Here... --}}
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/admin/views/index.css') }}" rel="stylesheet">
@stop

@section('content')

	<div class="dashboard-content">
		<div class="container">

			<h5 class="page-title">Dashboard</h5>
			<div class="section-content">
				<div class="row">
					<!-- <div class="col l6 s12">
						<div class="panel-brand date-panel">
							<div class="panel-head">
								<h4 class="panel-title">Date and Time</h4>
							</div>
							<div class="panel-body">
								<h6>
									<span class="day_"><span id="time">01:00</span></span>
	                				<span class="year_"><span id="ampm">AM</span></span>
                				</h6><br/>
								<h5>
									<span class="day_" id="day_">Feb 29</span>,
									<span class="year_" id="year_">2010</span>
								</h5>
								<h5>
									<span class="year_" id="date_">Mon</span>
								</h5>
							</div>
						</div>
					</div> -->
					<div class="col l6 s12">
						<a href="/admin/campaign">
						<div class="panel-brand campaign-panel">
							<div class="panel-head">
								<h4 class="panel-title">Published Campaigns</h4>
							</div>
							<div class="panel-body valign-wrapper">
								<h1 class="panel-label center-block">{{ $campaigns }}</h1>
							</div>
						</div>
						</a>
					</div>

					<div class="col l6 s12">
						<a href="/admin/campaign">
						<div class="panel-brand date-panel">
							<div class="panel-head">
								<h4 class="panel-title">Unpublished Campaigns</h4>
							</div>
							<div class="panel-body valign-wrapper">
								<h1 class="panel-label center-block">{{ $u_campaigns }}</h1>
							</div>
						</div>
						</a>
					</div>

					<div class="col l6 s12">
						<a href="/admin/business">
						<div class="panel-brand business-panel purple">
							<div class="panel-head">
								<h4 class="panel-title">Businesses</h4>
							</div>
							<div class="panel-body valign-wrapper">
								<h1 class="panel-label center-block">{{ $business }}</h1>
							</div>
						</div>
						</a>
					</div>
					<div class="col l6 s12">
						<a href="/admin/influencer">
						<div class="panel-brand influencer-panel orange">
							<div class="panel-head">
								<h4 class="panel-title center-block">Influencers</h4>
							</div>
							<div class="panel-body valign-wrapper">
								<h1 class="panel-label center-block">{{ $influencer }}</h1>
							</div>
						</div>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>

@stop

@section('footer')
	<script src="{{ config('s3.bucket_link') . elixir('assets/admin/views/index.js') }}"></script>
	<script src="{{ config('s3.bucket_link') . elixir('assets/company/views/index.js') }}"></script>
@stop
