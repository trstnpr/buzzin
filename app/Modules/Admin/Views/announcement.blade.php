@extends('adminMaster')

@section('title')
	<title>Announcement</title>
@stop

@section('stylesheet')

	<link href="{{ config('s3.bucket_link') . elixir('assets/admin/views/index.css') }}" rel="stylesheet">
	{{-- <link rel="stylesheet" type="text/css" href="{{ config('s3.bucket_link') . elixir('assets/company/jquery.dataTables.min.css') }}"> --}}
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
	<style type="text/css">
		.announcement-date {
			font-weight: 300;
		    font-size: 0.8rem !important;
		    color: #fff !important;
		    border-radius: 2px;
		}
	</style>
@stop

@section('content')

	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	<div class="campaign-content">
		<div class="container">

			<h5 class="page-title">Announcement</h5>

			<div class="section-content card-panel">
				<div class="panel">
					<table class="responsive-table highlight" cellspacing="0" width="100%" id="announcement"></table>
				</div>

				<!--Tabs-->
                    <!-- <div class="row">
                        <div class="col s12">
                              <ul class="tabs">
                                    <li class="tab col s6"><a class="active" href="#test1">Published Campaign</a></li>
                                    <li class="tab col s6"><a class="" href="#test2">Unpublished Campaign</a></li>
                              </ul>
                        </div>

                        <div id="test1" class="col s12">
                            <table class="responsive-table highlight" cellspacing="0" width="100%" id="campaign"></table>
                        </div>

                        <div id="test2" class="col s12">
                            <table class="responsive-table highlight" cellspacing="0" width="100%" id="campaignUnpublished"></table>
                        </div>
                    </div> -->
                <!--End Tabs-->

			</div>
		</div>

	</div>

    <div id="view-announcement" class="modal modal-fixed-footer">
	    <div class="modal-content">
	    	<div class="modal-header-announcement">
	    		<h5>Title: <span id="title"></span></h5>
	    	</div>

	        <ul class="collection with-header">
<!-- 	            <li class="collection-item"><div>Announcement: <span id="announcements"></span></div><br /></li>
	            <li class="collection-item"><div>Published Date: <span id="start"></span></div><br /></li>
	            <li class="collection-item"><div>Expiration Date: <span id="end"></span></div><br /></li> -->

	            <li class="collection-item"><div>Announcement: <span id="announcements"></span></div><br />
	            	<div class="right">
	            		<span class="announcement-date badge red">Expiration Date: <span id="end"></span></span>
	            		<span class="announcement-date badge blue">Published Date: <span id="start"></span></span>
	            	</div>
	            </li>
	        </ul>
	    </div>
	    <div class="modal-footer view-announcement">
	        <a href="#!" class="modal-action modal-close waves-effect purple btn">Close</a>
	    </div>
	</div>

	<div id="delete-announcement" class="modal">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="modal-content">
            <h5>Delete Announcement</h5>
            Do you want to delete this announcement?
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action modal-close btn green waves-effect waves-green btn-delete-yes-announcement" style="margin-left: 10px;">Yes</a>
            <a href="#!" class="modal-action modal-close btn red waves-effect waves-red">No</a>
        </div>
    </div>

    <div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
		<a href="{{ url('admin/announcement/create') }}" class="btn-floating btn-large purple tooltipped" data-position="left" data-tooltip="Post an Announcement">
			<i class="large mdi mdi-tooltip-edit"></i>
		</a>
	</div>
@stop

@section('footer')
	<script src="{{ config('s3.bucket_link') . elixir('assets/admin/views/index.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function(){
		    $('.dataTable').DataTable();
		});

		var nColNumber = -1;
	    var announcement = $('#announcement').DataTable({
	        'ajax': '/admin/announcement/getAllAnnouncement',
	        'processing': true,
	        'order': [[ 1, "desc" ]],
	        'columnDefs': [
	            { 'targets': [ ++nColNumber ], 'title':'Title', 'name': 'title', 'data': 'title' },
	            { 'targets': [ ++nColNumber ], 'title':'Published Date', 'name': 'published_date', 'data': 'published_date' },
	            { 'targets': [ ++nColNumber ], 'title':'Expiration Date', 'name': 'expiration_date', 'data': 'expiration_date' },
	            { 'className': 'center', 'targets': [ ++nColNumber ], 'title':'Actions', 'name': 'action', 'data': 'action'},
	        ]
	    });

		function deleteAnnouncement(announcement) {
	        $('#delete-announcement').modal('open');
	        $('.btn-delete-yes-announcement').attr('data-announcement-id', $(announcement).attr('data-announcement-id'));
	    }

	    function viewAnnouncement(announcement) {
	        $('#view-announcement').modal('open');
	        $('#title').html($(announcement).attr('data-title'));
	        $('#announcements').html($(announcement).attr('data-announcement'));
	        $('#start').html($(announcement).attr('data-published'));
	        $('#end').html($(announcement).attr('data-expiration'));
	    }

	    $('.btn-delete-yes-announcement').on('click', function() {
        $.ajax({
            url: 'announcement/'+$(this).attr('data-announcement-id'),
            headers:
            {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
            type: 'DELETE',
            success: function(data) {
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    Materialize.toast(msg.message, 4000, 'green');
                    reloadTable(announcement);
                } else{
                    Materialize.toast(msg.message, 4000, 'red');
                    location.reload();
                }
            }
        });
    });
	</script>
@stop
