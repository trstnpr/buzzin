@extends('adminMaster')

@section('title')
	<title>Announcement</title>
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/admin/views/index.css') }}" rel="stylesheet">
@stop

@section('content')

	<div class="campaign-content">
		<div class="container">

			<h5 class="page-title">Announcement</h5>

			<div class="section-content">
				<form class="campaignPost-form" id="form_edit_announcement">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="row">
						<div class="col l10 s12">
							<div class="card teal hide" id="result_div">
						        <i class="material-icons">check</i> Updated successfully!
						    </div>
							<div class="panel">
								<div class="row">
									<div class="col s12">
										<div class="form-group">
											<label>Title</label>
											<input type="text" class="form-control b2i-field" name="title" value="{{ $announcement['title'] }}" />
										</div>
									</div>

									<div class="col s12">
										<div class="form-group">
											<label>Announcement</label>
											<textarea class="wysiwyg" name="announcement">
											{{ $announcement['announcement'] }}
											</textarea>
										</div>
									</div>

									<div class="col m6 s12">
										<div class="form-group">
											<label>Published Date</label>
											<input type="date" class="form-control b2i-field datepicker" name="published_date" value="{{ $announcement['published_date'] }}"  />
										</div>
									</div>
									<div class="col m6 s12">
										<div class="form-group">
											<label>Expiration Date</label>
											<input type="date" class="form-control b2i-field datepicker" name="expiration_date" value="{{ $announcement['expiration_date'] }}"  />
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="col l2 s12">
							<div class="action-buttons">
								<button class="btn btn-large purple waves-effect waves-light col s12" type="submit" id="btn_update_announcement" style="margin-bottom: 10px;">submit</button>
								<a href="{{ url('/admin/announcement') }}" class="btn btn-large waves-effect waves-light red col s12">
									CANCEL
								</a>
							</div>
						</div>
					</div>
				</form>
			</div>

		</div>
	</div>

	<input type="hidden" id="a_id" value="{{ $announcement['announcement_id'] }}">
@stop

@section('footer')
	<script src="{{ config('s3.bucket_link') . elixir('assets/admin/views/index.js') }}"></script>
	<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			tinymce.init({ selector:'.wysiwyg' });
		});

		$('.datepicker').pickadate({
			selectMonths: true,
			selectYears: 15
		});

		var a_id = $('#a_id').val();

		$(document).ready(function(){
			$('#form_edit_announcement').on('submit', (function(e){
		        e.preventDefault();
		        $.ajax({
		            url: a_id+"/update",
		            type: "POST",
		            headers:
		            {
		                'X-CSRF-Token': $('input[name="_token"]').val()
		            },
		            data: new FormData(this),
		            contentType: false,
		            cache: false,
		            processData: false,
		            beforeSend: function(){ $('#btn_submit_announcement').html('Processing...');},
		            error: function(data){
		                if(data.readyState == 4){
		                    errors = JSON.parse(data.responseText);
		                    $('#result_div').empty();
		                    $('#result_div').html('<ul class="error_list fa-ul white-text"></ul>');
		                    $.each(errors,function(key,value){
		                        $('.error_list').append('<li><i class="fa fa-li fa-times"></i> '+value+'</li>');
		                    });
		                    $('#result_div').removeClass('teal hide').addClass('red');
		                    $('#btn_update_announcement').html('Submit');

		                    setTimeout(function(){$('#result_div').addClass('hide').html(''); }, 4000);
		                }
		            },

		            success: function(data){
		                var msg = JSON.parse(data);
		                if(msg.result == 'success'){
		                    Materialize.toast(msg.message, 1000, "green", function(){ window.location.href = '/admin/announcement'; });
		                } else{
		                    Materialize.toast(msg.message, 4000, 'red');
		                }
		            }
		        });
		    }));
		});
	</script>
@stop
