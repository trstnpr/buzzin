@extends('appformMaster')

@section('title')
	<title>Buzzin Admin Login</title>
@stop

@section('meta')
	{{-- Put Seo tags Here... --}}
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/app/views/index.css') }}" rel="stylesheet">
	<style type="text/css">
		* {
			color: #f2f2f2;
		}

		h5, input[type=password], input[type=email], label, span, h6 {
			color: #4a4a4a !important;
		}

	</style>
@stop

@section('content')

	<div class="signup-content">
		<section class="section-signup">
			<div class="container">
				<div class="row">
					<div class="col l4 offset-l4 m6 offset-m3 s12">
						<div class="card teal hide" id="result_div">
	                        <i class="material-icons">check</i> Login Successfully!
	                    </div>
						<div class="card-panel light-blue lighten-5">
							<a href="{{ route('app.index') }}">
								<img src="{{ config('s3.bucket_link') . elixir('images/assets/buzzinlogo2.png') }}" class="responsive-img brand-img" />
							</a>
							<h5 class="section-title">Admin</h5>

							<div class="tab-container" id="admin">
								<form id="form_admin_login">
									<div class="row">
										<input type="hidden" name="_token" value="{{ csrf_token() }}">
										<div class="col s12">
											<div class="form-group">
												<input placeholder="Email Address" type="email" class="form-control signup-field validate" name="email" required="required">
									        </div>
									    </div>
									    <div class="col s12">
											<div class="form-group">
												<input placeholder="Password" type="password" class="form-control signup-field" name="password" id="password" required="required">
									        </div>
									    </div>
									    <div class="col s12">
											<div class="form-group">
												<button class="btn col s12" type="submit" id="btn_admin">Submit</button>
									        </div>
									    </div>
									    <div class="col s12">
											<div class="form-group">
												<p>
													<input type="checkbox" class="filled-in" id="filled-in-box"/>
													<label for="filled-in-box">Remember me</label>
												</p>

									        </div>
									    </div>
								    </div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>

@stop

@section('footer')
	<script type="text/javascript">
		
	$(document).ready(function(){
		$('#form_admin_login').on('submit', (function(e){
	        e.preventDefault();
	        $.ajax({
	            url: "admin/login",
	            type: "POST",
	            data: new FormData(this),
	            contentType: false,
	            cache: false,
	            processData: false,
	            beforeSend: function(){ $('#btn_admin').html('Processing...');},
	            error: function(data){
	                if(data.readyState == 4){
	                    errors = JSON.parse(data.responseText);
	                    $('#result_div').empty();
	                    $('#result_div').html('<ul class="error_list fa-ul white-text"></ul>');
	                    $.each(errors,function(key,value){
	                        $('.error_list').append('<li><i class="fa fa-li fa-times"></i> '+value+'</li>');
	                    });
	                    $('#result_div').removeClass('teal hide').addClass('red');
	                    $('#password').val('');
	                    $('#btn_admin').html('Submit');

	                    setTimeout(function(){$('#result_div').addClass('hide').html(''); }, 4000);
	                }
	            },

	            success: function(data){
	                var msg = JSON.parse(data);
	                if(msg.result == 'success'){
	                    $("#btn_admin").html('Submit');
	                    Materialize.toast('Login Successfully', 4000, 'green');
	                    location.replace(msg.message);

	                } else{
	                    $("#password").val('');
	                    $("#btn_admin").html('Submit');
	                    Materialize.toast(msg.message, 4000, 'red');
	                    /*$("#result_div").removeClass('teal hide').addClass('red').html(msg.dialog);*/
	                }
	            }
	        });
	    }));
	});

	</script>

	<script src="{{ config('s3.bucket_link') . elixir('assets/app/views/index.js') }}"></script>
@stop