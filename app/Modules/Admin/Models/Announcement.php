<?php namespace App\Modules\Admin\Models;

use Illuminate\Database\Eloquent\Model;

class Announcement extends Model {

	protected $table = 'announcement';

    protected $fillable = ['admin_id',  'title', 'announcement', 'published_date', 'expiration_date'];

    protected $primaryKey = 'announcement_id';
}
