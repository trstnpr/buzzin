<?php namespace App\Modules\Admin\Controllers;

use Auth;
use Carbon\carbon;

use App\Services\ImageUploader;
use App\Services\FileUploader;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests\CampaignRequest;

use App\Modules\User\Models\Category;
use App\Modules\Campaign\Models\Campaign;
use App\Modules\Campaign\Models\CampaignImage;

class CampaignAdminController extends Controller {

    /**
     * Campaign page.
     *
     * @return view
     */
	public function campaign()
	{
		$this->data['pubCampaigns'] = Campaign::with('campaign_image')
                                        ->with([
                                        'profile' => function($query){
                                            $query->select()
                                            ->with([
                                                'company' => function($query){
                                                    $query->select();
                                                }]);
                                            }])
                                        ->where('status', '=', 1)->get();

		$this->data['unPubCampaigns'] = Campaign::with('campaign_image')
                                        ->with([
                                        'profile' => function($query){
                                            $query->select()
                                            ->with([
                                                'company' => function($query){
                                                    $query->select();
                                                }]);
                                            }])
                                        ->where('status', '=', 0)->get();
		//dd($this->data);
		return view("Admin::campaign", $this->data);
	}

    /**
     * Edit campaign.
     *
     * @return view
     */
	public function editCampaign($id)
	{
		$this->data['category'] = Category::all();
		$this->data['campaign'] = Campaign::with('campaign_image')->find($id);

		return view("Admin::editCampaign", $this->data);
	}

    /**
     * Update campaign.
     *
     * @return Response
     */
	public function updateCampaign($id, CampaignRequest $request)
	{  
       

		$start_date = date_format(date_create($request->date_start), 'Y-m-d');
        $end_date = date_format(date_create($request->date_end), 'Y-m-d');

        $url_code = $this->_generate_url_code();

		$data_campaign = [
			'campaign_name' => $request->campaign_name,
            'campaign_description' => $request->campaign_description,
            'category_id' => $request->category_id,
            'fund_type' => $request->fund_type,
            'fund_details' => $request->fund_details,
            'status' => $request->status,
            'date_start' => $start_date ,
            'date_end' => $end_date,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'campaign_url' => $request->campaign_url,
            'generated_url' => $request->campaign_name .' '. $url_code
		];

		if ($request->hasFile('file_url')){
            $fileUrl = $request->file_url;

            $fileUploader = new FileUploader;

            $file = $fileUploader->upload($fileUrl, $id);

            $data_campaign = [
                'file_url' => $file
            ];
        }

		$campaign = Campaign::where('campaign_id', '=', $id)->update($data_campaign);

        if($campaign){
            if ($request->hasFile('image_url')){
                $imageUrl = $request->image_url;
                $del = CampaignImage::where('campaign_id', $id)->delete();
                
                $this->_validate_image_format($imageUrl->getClientOriginalExtension());

                            $imageUploader = new ImageUploader;

                            $result = $imageUploader->upload($imageUrl, $id);

                            $data_campaign_image = [
                                'campaign_id' => $id,
                                'image_url' => $id.'/'.$result,
                                'created_at' => Carbon::now()
                            ];

                        $insert_Images = CampaignImage::insert($data_campaign_image);
            }

			return json_encode(array('result' => 'success', 'message' => 'Campaign Successfully Updated!'));
		}
		return json_encode(array('result' => 'error', 'message' => 'There\'s an error encountered while updating'));
	}

	/**
     * Delete campaign.
     *
     * @return Response
     */
    public function destroy($id)
    {
        if (Campaign::where('campaign_id', $id)->delete()) {
        	/*CampaignImage::where('campaign_id', $id)->delete();*/
            return json_encode(array('result' => 'success', 'message' => 'Successfully Deleted.'));
        }

        return json_encode(array('result' => 'error', 'message' => 'There is an error occured while deleting. Please try again later.'));
    }

	/**
     * Validating image format
     *
     * @return string
     */
    private function _validate_image_format($file_extension)
    {
        if (!in_array($file_extension, array('gif', 'png', 'jpg', 'jpeg', 'PNG', 'JPG'))) {
            return redirect()->back()->with('error', 'Image invalid format.')->withInput();
        }
    }

    /**
     * Generate url code.
     *
     * @return string
     */
    private function _generate_url_code()
    {
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZzxcvbnmasdfghjklqwertyuiop';

        $pin = mt_rand(10, 99) . mt_rand(10, 99) . $characters[rand(0, strlen($characters) - 1)];

        return str_shuffle($pin);
    }
}
