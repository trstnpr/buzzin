<?php namespace App\Modules\Admin\Controllers;

use Auth;
use Carbon\carbon;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests\CompanyProfileRequest;

use App\Modules\User\Models\User;
use App\Modules\User\Models\UserProfile;
use App\Modules\Company\Models\Company;
use App\Modules\Campaign\Models\Campaign;

class BusinessController extends Controller {

	/**
	 * Display a business resource.
	 *
	 * @return Response
	 */

	public function business()
	{
		$this->data['business'] = User::with([
                                        'profile' => function($query){
                                            $query->select()
                                            ->with([
                                                'company' => function($query){
                                                    $query->select()
                                                    ->with([
                                                        'campaign' => function($query){
                                                            $query->where('status', 1);
                                                        }]);
                                                }]);
                                            }])
                                    ->where('role_id', '=', 3)
                                    ->get();

        $business_id = array();

        if ($this->data['business']) {
            foreach ($this->data['business'] as $key => $value) {
                $business_id[] = $value['profile']['profile_id'];
            }
        }

        else{
            $business_id[] = 0;
        }

       /* $this->data['pub_count'] = Campaign::whereIn('profile_id', $business_id)->where('status', 1)->count();*/

		return view("Admin::business", $this->data);
	}

    /**
     * Edit business.
     *
     * @return Response
     */
    public function editBusiness($id)
    {
        $this->data['company'] = Company::where('company_id', $id)->first();
        $this->data['profile'] = UserProfile::where('profile_id', $this->data['company']['profile_id'])->first();

        return view("Admin::editBusiness", $this->data);
    }

    /**
     * Edit business.
     *
     * @return Response
     */
    public function businessUpdate($id, CompanyProfileRequest $request)
    {   
        $company = Company::where('company_id', $id)->first();
        /*$profile = UserProfile::where('user_id', $company['profile_id'])->first();*/

        $data_profile = [
                'first_name' => $request->first_name,
                'last_name' => $request->last_name
        ];

        $data_company = [
                'company_name' => $request->company_name,
                'brand_name' => $request->brand_name,
                'company_website' => $request->company_website,
                'company_contact_no' => $request->company_contact_no,
                'company_address_1' => $request->company_address_1,
                'company_address_2' => $request->company_address_2,
                'company_city' => $request->company_city,
                'company_state' => $request->company_state,
                'company_zip' => $request->company_zip,
                'company_country' => $request->company_country

        ];
        $profileUpdate = UserProfile::where('user_id', $company['profile_id'])->update($data_profile);
        $company = Company::where('company_id', $id)->update($data_company);
        
        if($company){              
            return json_encode(array('result' => 'success', 'message' => 'Successfully Updated.'));
        }

        return json_encode(array('result' => 'error', 'message' => 'There is an error occured while updating. Please try again later.'));
    }

    /**
     * Delete business.
     *
     * @return Response
     */
    public function deleteBusiness($id)
    {   
        $user = UserProfile::where('profile_id', $id)->first();
        if (UserProfile::where('profile_id', $id)->delete()) {
            User::where('id', $user['user_id'])->delete();
            return json_encode(array('result' => 'success', 'message' => 'Successfully Deleted.'));
        }

        return json_encode(array('result' => 'error', 'message' => 'There is an error occured while deleting. Please try again later.'));
    }
}
