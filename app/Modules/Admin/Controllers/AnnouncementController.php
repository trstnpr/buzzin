<?php namespace App\Modules\Admin\Controllers;

use Auth;
use Carbon\carbon;

use App\Services\ImageUploader;
use App\Services\FileUploader;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests\AnnouncementRequest;

use App\Modules\Admin\Models\Announcement;

class AnnouncementController extends Controller {

	/**
     * Announcement Page
     *
     * @return Response
     */
    public function announcement()
    {	
        return view("Admin::announcement");
    }

    /**
     * Get all announcement.
     *
     * @return array
     */
    public function getAllAnnouncement()
    {
        $announcement = Announcement::where('admin_id', Auth::user()->id)->orderBy('published_date', 'desc')->get()->toArray();

        $sc = array_map(function ($structure) use ($announcement) {

            $start = date_format(date_create($structure['published_date']), "M d, Y");
            $end = date_format(date_create($structure['expiration_date']), "M d, Y");

            $action = ' <button id="btn-view-announcement" type="button" class="btn btn-primary blue btn-circle white-text btn-view-announcement" title="View" data-toggle="modal" data-target="#view-announcement"
                        onclick="viewAnnouncement(this)"
                        data-announcement-id="' . $structure['announcement_id'] . '"
                        data-title="' . $structure['title'] . '"
                        data-announcement="' . strip_tags($structure['announcement']) . '"
                        data-published="' . $start . '"
                        data-expiration="' . $end . '">
                        <i class="material-icons">search</i>
                    </button> ';

            $action .= '<a href="announcement/' . $structure['announcement_id'] . '" class="btn waves-effect white-text" title="Edit">
                            <i class="material-icons">edit</i>
                        </a> ';

            $action .= '<button id="btn-delete-announcement" type="button" class="btn btn-danger red btn-circle red btn-delete-announcement" title="Delete" data-toggle="modal" data-target="#delete-announcement"
                        onclick="deleteAnnouncement(this)"
                        data-announcement-id="' . $structure['announcement_id'] . '">
                        <i class="material-icons">delete</i>
                    </button>';

            return [
                'title' => $structure['title'],
                'published_date' => $start,
                'expiration_date' => $end,
                'action' => $action
            ];
        }, $announcement);

        return ['data' => $sc];

    }

    /**
     * Edit Announcement.
     *
     * @return view
     */
	public function createAnnouncement()
	{
		return view("Admin::create_announcement");
	}

    /**
     * Add Announcement.
     *
     * @return Response
     */
    public function announcementStore(AnnouncementRequest $request)
	{	
		$published_date = date_format(date_create($request->published_date), 'Y-m-d');
        $expiration_date = date_format(date_create($request->expiration_date), 'Y-m-d');

		$data = [
			'admin_id' => Auth::user()->id,
            'title' => $request->title,
            'announcement' => $request->announcement,
            'published_date' => $published_date,
            'expiration_date' => $expiration_date
		];

		$announcement = Announcement::create($data);

		if($announcement){
			return json_encode(array('result' => 'success', 'message' => 'Announcement Successfully Added!'));
		}
			
		return json_encode(array('result' => 'error', 'message' => 'There\'s an error encountered while saving'));
	}

	/**
     * Edit Announcement.
     *
     * @return view
     */
	public function editAnnouncement($id)
	{
		$data['announcement'] = Announcement::where('announcement_id', $id)->first();

		return view("Admin::edit_announcement", $data);
	}

	/**
     * Add Announcement.
     *
     * @return Response
     */
    public function announcementUpdate($announcement_id, AnnouncementRequest $request)
	{	
		$published_date = date_format(date_create($request->published_date), 'Y-m-d');
        $expiration_date = date_format(date_create($request->expiration_date), 'Y-m-d');

		$data = [
			'admin_id' => Auth::user()->id,
            'title' => $request->title,
            'announcement' => $request->announcement,
            'published_date' => $published_date,
            'expiration_date' => $expiration_date
		];

		$announcement = Announcement::where('announcement_id', $announcement_id)->update($data);

		if($announcement){
			return json_encode(array('result' => 'success', 'message' => 'Announcement Successfully Updated!'));
		}
			
		return json_encode(array('result' => 'error', 'message' => 'There\'s an error encountered while updating'));
	}

	/**
     * Delete announcement.
     *
     * @return Response
     */
    public function announcementDestroy($id)
    {
        if (Announcement::where('announcement_id', $id)->delete()) {
            return json_encode(array('result' => 'success', 'message' => 'Successfully Deleted.'));
        }

        return json_encode(array('result' => 'error', 'message' => 'There is an error occured while deleting. Please try again later.'));
    }
}
