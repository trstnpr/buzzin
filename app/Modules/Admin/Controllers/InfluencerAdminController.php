<?php namespace App\Modules\Admin\Controllers;

use Auth;
use Carbon\carbon;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Modules\User\Models\User;
use App\Modules\User\Models\UserProfile;
use App\Modules\User\Models\Category;

class InfluencerAdminController extends Controller {

    /**
     * Influencer Page.
     *
     * @return view
     */
	public function influencer()
	{
		$this->data['influencer'] = User::with('profile')->where('role_id', '=', 2)->get();

		return view("Admin::influencer", $this->data);
	}

    /**
     * Influencer Page.
     *
     * @return view
     */
    public function editInfluencer($id)
    {
        $this->data['influencer'] = UserProfile::where('profile_id', $id)->first();

        return view("Admin::editInfluencer", $this->data);
    }

    /**
     * Update Influencer.
     *
     * @return Response
     */
    public function influencerUpdate($id, Request $request)
    {   
        $input = $request->except('_token');
        if (UserProfile::where('profile_id', '=', $id)->update($input)) {
            return json_encode(array('result' => 'success', 'message' => 'Influencer Updated!'));
        }
        return json_encode(array('result' => 'error', 'message' => 'There\'s an error updating influencer profile!'));
    }

    /**
     * Delete influencer.
     *
     * @return Response
     */
    public function deleteInfluencer($id)
    {   
        $user = UserProfile::where('profile_id', $id)->first();
        if (UserProfile::where('profile_id', $id)->delete()) {
            User::where('id', $user['user_id'])->delete();
            return json_encode(array('result' => 'success', 'message' => 'Successfully Deleted.'));
        }

        return json_encode(array('result' => 'error', 'message' => 'There is an error occured while deleting. Please try again later.'));
    }

	public function adminLogout()
	{
		Auth::logout();
        return redirect('/admin');
	}
}
