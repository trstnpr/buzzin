<?php namespace App\Modules\Admin\Controllers;

use Auth;
use Carbon\carbon;

use App\Services\ImageUploader;
use App\Services\FileUploader;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests\AnnouncementRequest;
use App\Http\Requests\CompanyProfileRequest;

use App\Modules\User\Models\User;
use App\Modules\User\Models\UserProfile;
use App\Modules\User\Models\Category;
use App\Modules\Company\Models\Company;
use App\Modules\Campaign\Models\Campaign;
use App\Modules\Campaign\Models\CampaignImage;
use App\Modules\Admin\Models\Announcement;

class AdminController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->data['campaigns'] = Campaign::with('campaign_image')->where('status', '=', 1)->count();
        $this->data['u_campaigns'] = Campaign::with('campaign_image')->where('status', '=', 0)->count();
		$this->data['business'] = User::with('profile')->where('role_id', '=', 3)->count();
		$this->data['influencer'] = User::with('profile')->where('role_id', '=', 2)->count();

		return view("Admin::index", $this->data);
	}

    public function adminLogin()
    {
        return view("Admin::adminLogin");
    }

    public function adminLoginProcess(Request $request)
    {
    	if (Auth::attempt(['email' => $request->email, 'password' => $request->password, 'role_id' => 1])) {
    		return json_encode(array('result' => 'success', 'message' => $this->getUserRole(Auth::user()->role_id) . '/dashboard'));
    	}
    	return json_encode(array('result' => 'error', 'message' => 'Authentication Failed!'));
    }

	public function influencer()
	{
		$this->data['influencer'] = User::with('profile')->where('role_id', '=', 2)->get();

		return view("Admin::influencer", $this->data);
	}

    /**
     * Delete influencer.
     *
     * @return Response
     */
    public function deleteInfluencer($id)
    {   
        $user = UserProfile::where('profile_id', $id)->first();
        if (UserProfile::where('profile_id', $id)->delete()) {
            User::where('id', $user['user_id'])->delete();
            return json_encode(array('result' => 'success', 'message' => 'Successfully Deleted.'));
        }

        return json_encode(array('result' => 'error', 'message' => 'There is an error occured while deleting. Please try again later.'));
    }

	public function adminLogout()
	{
		Auth::logout();
        return redirect('/admin');
	}
}
