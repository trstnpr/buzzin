-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.13-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for b2i_db
CREATE DATABASE IF NOT EXISTS `b2i_db` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `b2i_db`;


-- Dumping structure for table b2i_db.b2i_announcement
CREATE TABLE IF NOT EXISTS `b2i_announcement` (
  `announcement_id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_id` int(11) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `announcement` varchar(255) DEFAULT NULL,
  `published_date` date DEFAULT NULL,
  `expiration_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`announcement_id`),
  KEY `admin_id` (`admin_id`),
  CONSTRAINT `FK1_admin_id` FOREIGN KEY (`admin_id`) REFERENCES `b2i_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table b2i_db.b2i_announcement: ~2 rows (approximately)
DELETE FROM `b2i_announcement`;
/*!40000 ALTER TABLE `b2i_announcement` DISABLE KEYS */;
INSERT INTO `b2i_announcement` (`announcement_id`, `admin_id`, `title`, `announcement`, `published_date`, `expiration_date`, `created_at`, `updated_at`) VALUES
	(1, 32, 'Anouncement 12', '<p><strong>asasasasasssssssss</strong></p>', '2016-11-04', '2016-11-05', '2016-11-04 02:19:41', '2016-11-04 02:52:59'),
	(3, 32, 'Announcement 1', '<p><strong><em>QWERTY</em></strong></p>', '2016-11-05', '2016-11-11', '2016-11-04 03:57:35', '2016-11-07 05:17:40');
/*!40000 ALTER TABLE `b2i_announcement` ENABLE KEYS */;


-- Dumping structure for table b2i_db.b2i_blogs
CREATE TABLE IF NOT EXISTS `b2i_blogs` (
  `blog_id` int(11) NOT NULL AUTO_INCREMENT,
  `profile_id` int(11) DEFAULT NULL,
  `blog_name` varchar(150) DEFAULT NULL,
  `blog_url` varchar(150) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `google_view_id` varchar(150) DEFAULT NULL,
  `google_project_id` varchar(150) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`blog_id`),
  KEY `profile_id` (`profile_id`),
  KEY `category_id` (`category_id`),
  CONSTRAINT `FK_b2i_blogs_b2i_blog_category` FOREIGN KEY (`category_id`) REFERENCES `b2i_category` (`category_id`),
  CONSTRAINT `FK_b2i_blogs_b2i_profile` FOREIGN KEY (`profile_id`) REFERENCES `b2i_profile` (`profile_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table b2i_db.b2i_blogs: ~0 rows (approximately)
DELETE FROM `b2i_blogs`;
/*!40000 ALTER TABLE `b2i_blogs` DISABLE KEYS */;
/*!40000 ALTER TABLE `b2i_blogs` ENABLE KEYS */;


-- Dumping structure for table b2i_db.b2i_blog_count
CREATE TABLE IF NOT EXISTS `b2i_blog_count` (
  `count_id` int(11) NOT NULL AUTO_INCREMENT,
  `blogger_campaign_id` int(11) DEFAULT NULL,
  `ip_address` varchar(150) DEFAULT NULL,
  `count_no` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`count_id`),
  KEY `blogger_campaign_id` (`blogger_campaign_id`),
  CONSTRAINT `FK1_blogger_campaign_id` FOREIGN KEY (`blogger_campaign_id`) REFERENCES `b2i_influencer_campaign` (`blogger_campaign_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table b2i_db.b2i_blog_count: ~0 rows (approximately)
DELETE FROM `b2i_blog_count`;
/*!40000 ALTER TABLE `b2i_blog_count` DISABLE KEYS */;
/*!40000 ALTER TABLE `b2i_blog_count` ENABLE KEYS */;


-- Dumping structure for table b2i_db.b2i_campaign
CREATE TABLE IF NOT EXISTS `b2i_campaign` (
  `campaign_id` int(11) NOT NULL AUTO_INCREMENT,
  `profile_id` int(11) DEFAULT NULL,
  `campaign_name` varchar(150) DEFAULT NULL,
  `campaign_url` varchar(255) NOT NULL,
  `generated_url` varchar(255) NOT NULL,
  `campaign_description` longtext,
  `category_id` int(11) DEFAULT NULL,
  `pot_money` varchar(50) DEFAULT NULL,
  `fund_type` int(11) DEFAULT NULL COMMENT '0=Monetary, 1=Non-monetary',
  `fund_details` longtext,
  `view_count` bigint(20) NOT NULL DEFAULT '0',
  `date_start` datetime DEFAULT NULL,
  `date_end` datetime DEFAULT NULL,
  `status` int(2) DEFAULT '0' COMMENT '0=Unpublished,1=Published',
  `file_url` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`campaign_id`),
  KEY `profile_id` (`profile_id`),
  KEY `category_id` (`category_id`),
  CONSTRAINT `FK_b2i_campaign_b2i_blog_category` FOREIGN KEY (`category_id`) REFERENCES `b2i_category` (`category_id`),
  CONSTRAINT `FK_b2i_campaign_b2i_profile` FOREIGN KEY (`profile_id`) REFERENCES `b2i_profile` (`profile_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

-- Dumping data for table b2i_db.b2i_campaign: ~11 rows (approximately)
DELETE FROM `b2i_campaign`;
/*!40000 ALTER TABLE `b2i_campaign` DISABLE KEYS */;
INSERT INTO `b2i_campaign` (`campaign_id`, `profile_id`, `campaign_name`, `campaign_url`, `generated_url`, `campaign_description`, `category_id`, `pot_money`, `fund_type`, `fund_details`, `view_count`, `date_start`, `date_end`, `status`, `file_url`, `created_at`, `updated_at`) VALUES
	(2, 4, 'Xxxx', 'http://facebook.comss', 'Xxxx 47S94', '<p>asasasdasdasda</p>', 1, '10000', NULL, 'aas', 0, '2016-11-03 00:00:00', '2016-11-03 00:00:00', 0, NULL, '2016-11-03 07:19:10', '2016-11-03 07:19:10'),
	(4, 10, 'Campaign 103', '', 'Campaign 103_5n471', '																																																																								<p><strong><em>Sniff Knot collar and leash for Dogs! :)</em></strong></p>\r\n											\r\n											\r\n											\r\n											\r\n											\r\n											', 1, '5000', NULL, 'Sniff Knot', 3, '2016-10-26 00:00:00', '2016-10-28 00:00:00', 1, NULL, '2016-10-26 01:52:54', '2016-11-04 09:36:30'),
	(6, 26, 'Sample Campaign', 'http://www.google.com.ph', 'Sample Campaign 1552T', '												<p>Sample campaign description.</p>\r\n											', 16, '10000', NULL, NULL, 0, '2016-11-05 00:00:00', '2016-11-06 00:00:00', 1, NULL, '2016-11-03 05:38:55', '2016-11-03 05:39:43'),
	(7, 10, 'Campaign 104', 'twitter.com', 'Campaign 104_757Z8', '																								<p><strong><em>Sniff Knot collar and leash for Dogs! :)</em></strong></p>\r\n											\r\n											', 1, '5000', NULL, 'Sniff Knot', 1, '2016-10-26 00:00:00', '2016-10-28 00:00:00', 1, NULL, '2016-10-26 01:52:54', '2016-11-04 09:03:04'),
	(8, 10, 'Incubix Technologies gigSplash Buzzin Teachat IKonsulta Floox Marino Daily Leverage', '', 'Incubix Technologies gigSplash Buzzin Teachat IKonsulta Floox Marino Daily Leverage V1628', '<p>asasasasas</p>', 2, 'Starbucks', 0, '10000', 0, '2016-11-04 00:00:00', '2016-11-05 00:00:00', 1, NULL, '2016-11-07 06:48:27', '2016-11-07 06:48:27'),
	(12, 10, 'Incubix Technologies gigSplash Buzzin Teachat IKonsulta Floox Marino Daily Leverage', '', 'Incubix Technologies gigSplash Buzzin Teachat IKonsulta Floox Marino Daily Leverage_70P78', '<p>aaaaaaaaaa</p>', 2, 'GC', 0, '100', 0, '2016-11-23 00:00:00', '2016-11-26 00:00:00', 0, NULL, '2016-11-04 08:00:34', '2016-11-07 06:50:59'),
	(13, 10, 'Galaxy', '', 'Galaxy_697R8', '<p>POTA</p>', 1, '1000', NULL, NULL, 0, '2016-11-04 00:00:00', '2016-11-05 00:00:00', 1, NULL, '2016-11-04 09:37:29', '2016-11-04 09:54:27'),
	(15, 10, 'Pandora', '', 'Pandora 697L8', '<p>Pandora</p>', 3, NULL, 0, '100000', 0, '2016-11-04 00:00:00', '2016-11-05 00:00:00', 1, 'tNAXsKiR.docx', '2016-11-07 05:35:04', '2016-11-07 05:35:04'),
	(16, 10, 'lalalaalall', '', 'lalalaalall Q4077', '<p>lghju</p>', 1, '1000', NULL, NULL, 0, '2016-11-04 00:00:00', '2016-11-05 00:00:00', 1, 'jBb19ZYYx0.pdf', '2016-11-04 10:54:00', '2016-11-04 10:54:00'),
	(17, 10, 'Non Monetarys', '', 'Non Monetarys_84p85', '<p>Lalalalalalalala</p>', 3, NULL, 1, 'SM Movie Tickets', 0, '2016-11-07 00:00:00', '2016-11-08 00:00:00', 1, NULL, '2016-11-07 02:09:53', '2016-11-07 02:41:08'),
	(18, 10, 'Caaaaaamp', '', 'Caaaaaamp_5989a', '<p>Dog clothes</p>', 8, NULL, 0, '10000', 0, '2016-11-11 00:00:00', '2016-11-12 00:00:00', 1, NULL, '2016-11-11 08:07:55', '2016-11-11 08:12:22');
/*!40000 ALTER TABLE `b2i_campaign` ENABLE KEYS */;


-- Dumping structure for table b2i_db.b2i_campaign_image
CREATE TABLE IF NOT EXISTS `b2i_campaign_image` (
  `campaign_image_id` int(11) NOT NULL AUTO_INCREMENT,
  `campaign_id` int(11) DEFAULT NULL,
  `image_url` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`campaign_image_id`),
  KEY `campaign_id` (`campaign_id`),
  CONSTRAINT `FK_b2i_campaign_image_b2i_campaign` FOREIGN KEY (`campaign_id`) REFERENCES `b2i_campaign` (`campaign_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=latin1;

-- Dumping data for table b2i_db.b2i_campaign_image: ~12 rows (approximately)
DELETE FROM `b2i_campaign_image`;
/*!40000 ALTER TABLE `b2i_campaign_image` DISABLE KEYS */;
INSERT INTO `b2i_campaign_image` (`campaign_image_id`, `campaign_id`, `image_url`, `created_at`, `updated_at`) VALUES
	(29, 6, '6/lvTbMCOm5f.jpg', NULL, '2016-11-03 05:39:43'),
	(31, 2, '2/6TOHO1fyZQoY.jpg', NULL, '2016-11-03 07:19:10'),
	(32, 4, '4/wDY0oldc6Ut.jpg', NULL, '2016-11-04 06:39:21'),
	(38, 12, '12/LlGn3ehdgU.jpg', '2016-11-04 08:00:34', NULL),
	(39, 7, '7/CpvaRpnhnf1fYw.jpg', NULL, '2016-11-04 09:03:04'),
	(40, 7, '7/TJSbWX7ZBnBs.jpg', NULL, '2016-11-04 09:03:04'),
	(41, 13, '13/t4vmMXa2sdlO50Q.jpg', '2016-11-04 09:37:29', NULL),
	(44, 16, '16/ZBCUi6pLhj2v.jpg', '2016-11-04 10:54:00', NULL),
	(47, 17, '17/acZDRSgRhoKbeF02.jpg', '2016-11-07 02:09:54', NULL),
	(52, 15, '15/rQCqIJrPYLm.jpg', '2016-11-07 05:34:54', NULL),
	(53, 8, '8/achTwKSH7z.jpg', '2016-11-07 05:42:46', NULL),
	(54, 18, '18/8upa1DR6Hk.jpg', '2016-11-11 08:07:55', NULL);
/*!40000 ALTER TABLE `b2i_campaign_image` ENABLE KEYS */;


-- Dumping structure for table b2i_db.b2i_campaign_message
CREATE TABLE IF NOT EXISTS `b2i_campaign_message` (
  `campaign_message_id` int(11) NOT NULL AUTO_INCREMENT,
  `campaign_id` int(11) DEFAULT NULL COMMENT 'mapped to campaign table',
  `profile_id` int(11) DEFAULT NULL COMMENT 'mapped to profile table (influencer role)',
  `message` longtext,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`campaign_message_id`),
  KEY `campaign_id` (`campaign_id`),
  KEY `profile_id` (`profile_id`),
  CONSTRAINT `FK_b2i_campaign_message_b2i_campaign` FOREIGN KEY (`campaign_id`) REFERENCES `b2i_campaign` (`campaign_id`),
  CONSTRAINT `FK_b2i_campaign_message_b2i_profile` FOREIGN KEY (`profile_id`) REFERENCES `b2i_profile` (`profile_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table b2i_db.b2i_campaign_message: ~0 rows (approximately)
DELETE FROM `b2i_campaign_message`;
/*!40000 ALTER TABLE `b2i_campaign_message` DISABLE KEYS */;
/*!40000 ALTER TABLE `b2i_campaign_message` ENABLE KEYS */;


-- Dumping structure for table b2i_db.b2i_category
CREATE TABLE IF NOT EXISTS `b2i_category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(150) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

-- Dumping data for table b2i_db.b2i_category: ~18 rows (approximately)
DELETE FROM `b2i_category`;
/*!40000 ALTER TABLE `b2i_category` DISABLE KEYS */;
INSERT INTO `b2i_category` (`category_id`, `category`, `created_at`, `updated_at`) VALUES
	(1, 'Arts/Culture', '2016-10-25 11:08:20', '2016-10-25 11:08:20'),
	(2, 'Beauty/Make-up', '2016-10-25 12:10:15', '2016-10-25 12:10:15'),
	(3, 'Business/Finance', '2016-10-25 12:10:35', '2016-10-25 12:10:35'),
	(4, 'Corporate/Brand', '2016-10-26 06:22:21', '2016-10-26 06:22:21'),
	(5, 'Entertainment', '2016-10-26 06:22:57', '2016-10-26 06:22:57'),
	(6, 'Family/Relationships', '2016-10-26 06:22:57', '2016-10-26 06:22:57'),
	(7, 'Fashion/Style', '2016-10-26 06:23:27', '2016-10-26 06:23:27'),
	(8, 'Food/Dining', '2016-10-26 06:23:27', '2016-10-26 06:23:27'),
	(9, 'Health and Fitness', '2016-10-26 06:23:55', '2016-10-26 06:23:55'),
	(10, 'Lifestyle', '2016-10-26 06:23:55', '2016-10-26 06:23:55'),
	(11, 'Nature/Environment', '2016-10-26 06:24:28', '2016-10-26 06:24:28'),
	(12, 'News and Events', '2016-10-26 06:24:28', '2016-10-26 06:24:28'),
	(13, 'Personal Diary', '2016-10-26 06:24:55', '2016-10-26 06:24:55'),
	(14, 'Photography/Photo Blog', '2016-10-26 06:24:55', '2016-10-26 06:24:55'),
	(15, 'Society/Politics', '2016-10-26 06:25:40', '2016-10-26 06:25:40'),
	(16, 'Sports/Recreational Activities', '2016-10-26 06:25:40', '2016-10-26 06:25:40'),
	(17, 'Technology/Electronics/Gadgets', '2016-10-26 06:26:23', '2016-10-26 06:26:23'),
	(18, 'Travel/Adventures', '2016-10-26 06:26:23', '2016-10-26 06:26:23');
/*!40000 ALTER TABLE `b2i_category` ENABLE KEYS */;


-- Dumping structure for table b2i_db.b2i_company
CREATE TABLE IF NOT EXISTS `b2i_company` (
  `company_id` int(11) NOT NULL AUTO_INCREMENT,
  `profile_id` int(11) NOT NULL DEFAULT '0',
  `company_name` varchar(150) DEFAULT NULL,
  `brand_name` varchar(150) DEFAULT NULL,
  `company_website` varchar(150) DEFAULT NULL,
  `company_contact_no` varchar(20) DEFAULT NULL,
  `company_address_1` varchar(150) DEFAULT NULL,
  `company_address_2` varchar(150) DEFAULT NULL,
  `company_city` varchar(100) DEFAULT NULL,
  `company_state` varchar(50) DEFAULT NULL,
  `company_zip` varchar(10) DEFAULT NULL,
  `company_country` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`company_id`),
  KEY `profile_id` (`profile_id`),
  CONSTRAINT `FK_b2i_company_b2i_profile` FOREIGN KEY (`profile_id`) REFERENCES `b2i_profile` (`profile_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Dumping data for table b2i_db.b2i_company: ~5 rows (approximately)
DELETE FROM `b2i_company`;
/*!40000 ALTER TABLE `b2i_company` DISABLE KEYS */;
INSERT INTO `b2i_company` (`company_id`, `profile_id`, `company_name`, `brand_name`, `company_website`, `company_contact_no`, `company_address_1`, `company_address_2`, `company_city`, `company_state`, `company_zip`, `company_country`, `created_at`, `updated_at`) VALUES
	(1, 4, 'NightOwl Studios', 'abbaab', 'devhub.ph', '12121212', 'PSB', '', 'Makati', 'Metro Manila', '1212121', 'United States', '2016-10-25 10:53:29', '2016-11-07 05:13:51'),
	(2, 5, 'Sample Company XYZ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-10-25 12:05:48', '2016-10-25 12:05:48'),
	(3, 9, 'incubix tech', 'gigsplash', 'incubixtech.com', '09088909780', 'pacific star', '', 'makati', 'makati', '1100', 'Philippines', '2016-10-25 22:26:42', '2016-11-07 05:42:29'),
	(4, 10, 'Incubix Tech.,', 'Incubix Technologies gigSplash Buzzin Teachat IKonsulta Floox Marino Daily Leverage', 'incubixtech.com', '123456789', 'Pacific Star Building Makati Ave.,', '', 'Makati City', 'Cagayan Valley (Region II)', '1209', 'Philippines', '2016-10-26 01:50:22', '2016-11-11 09:47:51'),
	(8, 26, 'IncubixTech', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-10-28 09:04:15', '2016-10-28 09:04:15');
/*!40000 ALTER TABLE `b2i_company` ENABLE KEYS */;


-- Dumping structure for table b2i_db.b2i_influencer_campaign
CREATE TABLE IF NOT EXISTS `b2i_influencer_campaign` (
  `blogger_campaign_id` int(11) NOT NULL AUTO_INCREMENT,
  `profile_id` int(11) DEFAULT NULL COMMENT 'mapped to profile table (blogger role)',
  `campaign_id` int(11) DEFAULT NULL COMMENT 'mapped to campaign table',
  `generated_url` varchar(255) DEFAULT NULL,
  `view_count` bigint(20) DEFAULT '0',
  `type` varchar(50) DEFAULT NULL COMMENT 'apply or invite',
  `status` int(11) DEFAULT '0' COMMENT '0=Pending, 1=Approved, 2=Declined',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`blogger_campaign_id`),
  KEY `campaign_id` (`campaign_id`),
  KEY `profile_id` (`profile_id`),
  CONSTRAINT `FK_b2i_bloggers_campaign_b2i_campaign` FOREIGN KEY (`campaign_id`) REFERENCES `b2i_campaign` (`campaign_id`) ON DELETE CASCADE,
  CONSTRAINT `FK_b2i_bloggers_campaign_b2i_profile` FOREIGN KEY (`profile_id`) REFERENCES `b2i_profile` (`profile_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table b2i_db.b2i_influencer_campaign: ~4 rows (approximately)
DELETE FROM `b2i_influencer_campaign`;
/*!40000 ALTER TABLE `b2i_influencer_campaign` DISABLE KEYS */;
INSERT INTO `b2i_influencer_campaign` (`blogger_campaign_id`, `profile_id`, `campaign_id`, `generated_url`, `view_count`, `type`, `status`, `created_at`, `updated_at`) VALUES
	(1, 11, 4, 'Campaign 103_4_v9688', 1, 'apply', 1, '2016-11-04 12:22:06', '2016-11-11 03:46:30'),
	(2, 10, 7, 'Campaign 104_7_69y91', 4, 'apply', 1, '2016-11-04 12:22:06', '2016-11-11 03:44:29'),
	(3, 11, 7, 'Campaign 104_7_11I60', 0, 'apply', 1, '2016-11-04 07:47:37', '2016-11-11 03:29:03'),
	(4, 11, 8, NULL, 0, 'invite', 0, '2016-11-07 06:55:12', '2016-11-07 06:55:12');
/*!40000 ALTER TABLE `b2i_influencer_campaign` ENABLE KEYS */;


-- Dumping structure for table b2i_db.b2i_profile
CREATE TABLE IF NOT EXISTS `b2i_profile` (
  `profile_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `organization_name` varchar(150) DEFAULT NULL,
  `about_blogger` longtext COMMENT 'for influencer role only',
  `blogger_pricing` varchar(50) DEFAULT NULL,
  `website` varchar(150) DEFAULT NULL,
  `address_1` varchar(255) DEFAULT NULL,
  `address_2` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `zip_code` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `contact_number` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`profile_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `FK_b2i_profile_b2i_user` FOREIGN KEY (`user_id`) REFERENCES `b2i_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

-- Dumping data for table b2i_db.b2i_profile: ~8 rows (approximately)
DELETE FROM `b2i_profile`;
/*!40000 ALTER TABLE `b2i_profile` DISABLE KEYS */;
INSERT INTO `b2i_profile` (`profile_id`, `user_id`, `first_name`, `last_name`, `organization_name`, `about_blogger`, `blogger_pricing`, `website`, `address_1`, `address_2`, `city`, `state`, `zip_code`, `country`, `contact_number`, `created_at`, `updated_at`) VALUES
	(4, 9, 'john', 'san pedro', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-10-25 10:53:29', '2016-11-07 05:42:29'),
	(5, 10, 'Ronalyn', 'Corsino', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-10-25 12:05:48', '2016-11-07 06:44:44'),
	(8, 13, 'john', 'san pedro', 'Incubix Technologies gigSplash Buzzin Teachat IKonsulta Floox Marino Daily Leverage', '', '111', '111', '11', '1', '11', '111', '11', '11', '111', '2016-10-25 22:25:46', '2016-11-07 06:47:53'),
	(9, 14, 'john', 'san pedro', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-10-25 22:26:42', '2016-10-25 22:30:32'),
	(10, 15, 'Ronalyn', 'Corsino', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-10-26 01:50:22', '2016-11-11 09:47:51'),
	(11, 16, 'Christian Dave', 'Tangonan', 'Incubix', '', '10', 'incubixtech.com', 'Tondo St.,', '', 'Manila', 'Central Luzon (Region III)', '', 'United States', '789654', '2016-10-26 14:19:16', '2016-11-11 09:53:51'),
	(12, 17, 'Ace', 'Gapuz', 'Buzzin', '<p>CEO, Co-owner, Blogger</p>', '1000000', 'letsbuzzin.com', 'Maginhawa St.,', '', 'Quezon City', 'Metro Manila', '', 'Philippines', '123232', '2016-10-27 02:30:18', '2016-11-07 06:01:55'),
	(26, 31, 'Louie', 'Villena', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-10-28 09:04:15', NULL);
/*!40000 ALTER TABLE `b2i_profile` ENABLE KEYS */;


-- Dumping structure for table b2i_db.b2i_profile_category
CREATE TABLE IF NOT EXISTS `b2i_profile_category` (
  `profile_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `profile_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`profile_category_id`),
  KEY `category_id` (`category_id`),
  KEY `profile_id` (`profile_id`),
  CONSTRAINT `FK1_profile_category_category_id` FOREIGN KEY (`category_id`) REFERENCES `b2i_category` (`category_id`) ON DELETE SET NULL,
  CONSTRAINT `FK2_profile_category_profile_id` FOREIGN KEY (`profile_id`) REFERENCES `b2i_profile` (`profile_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- Dumping data for table b2i_db.b2i_profile_category: ~6 rows (approximately)
DELETE FROM `b2i_profile_category`;
/*!40000 ALTER TABLE `b2i_profile_category` DISABLE KEYS */;
INSERT INTO `b2i_profile_category` (`profile_category_id`, `profile_id`, `category_id`, `created_at`, `updated_at`) VALUES
	(2, 11, 1, '2016-11-11 15:45:10', NULL),
	(3, 11, 2, '2016-11-11 15:45:10', NULL),
	(5, 12, 1, NULL, NULL),
	(6, 12, 3, NULL, NULL),
	(9, 10, 1, NULL, NULL),
	(10, 10, 3, NULL, NULL);
/*!40000 ALTER TABLE `b2i_profile_category` ENABLE KEYS */;


-- Dumping structure for table b2i_db.b2i_region
CREATE TABLE IF NOT EXISTS `b2i_region` (
  `region_id` int(11) NOT NULL AUTO_INCREMENT,
  `region` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`region_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table b2i_db.b2i_region: ~5 rows (approximately)
DELETE FROM `b2i_region`;
/*!40000 ALTER TABLE `b2i_region` DISABLE KEYS */;
INSERT INTO `b2i_region` (`region_id`, `region`, `created_at`, `updated_at`) VALUES
	(1, 'Ilocos Region (Region I)', '2016-11-11 17:39:50', NULL),
	(2, 'Cagayan Valley (Region II)', '2016-11-11 17:40:22', NULL),
	(3, 'Central Luzon (Region III)', '2016-11-11 17:40:20', NULL),
	(4, 'CALABARZON (Region IV-A)', '2016-11-11 17:40:20', NULL),
	(5, 'MIMAROPA (Region IV-B)', '2016-11-11 17:40:31', NULL);
/*!40000 ALTER TABLE `b2i_region` ENABLE KEYS */;


-- Dumping structure for table b2i_db.b2i_roles
CREATE TABLE IF NOT EXISTS `b2i_roles` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(50) DEFAULT NULL,
  `role_description` longtext,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table b2i_db.b2i_roles: ~3 rows (approximately)
DELETE FROM `b2i_roles`;
/*!40000 ALTER TABLE `b2i_roles` DISABLE KEYS */;
INSERT INTO `b2i_roles` (`role_id`, `role_name`, `role_description`, `created_at`, `updated_at`) VALUES
	(1, 'Admin', 'Can access to all functionality', '2016-10-20 18:39:59', '2016-10-20 18:40:00'),
	(2, 'Influencer', 'Influencer account', '2016-10-20 18:40:33', '2016-10-20 18:40:34'),
	(3, 'Business', 'Business account, can create campaign', '2016-10-20 18:41:44', '2016-10-20 18:41:46');
/*!40000 ALTER TABLE `b2i_roles` ENABLE KEYS */;


-- Dumping structure for table b2i_db.b2i_social_media_account
CREATE TABLE IF NOT EXISTS `b2i_social_media_account` (
  `social_account_id` int(11) NOT NULL AUTO_INCREMENT,
  `profile_id` int(11) DEFAULT NULL,
  `provider_name` varchar(150) DEFAULT NULL COMMENT 'Facebook, Twitter, Instagram, Youtube',
  `provider_id` varchar(150) DEFAULT NULL,
  `provider_key` varchar(150) DEFAULT NULL,
  `profile_url` varchar(150) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`social_account_id`),
  KEY `profile_id` (`profile_id`),
  CONSTRAINT `FK_b21_media_account_b2i_profile` FOREIGN KEY (`profile_id`) REFERENCES `b2i_profile` (`profile_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table b2i_db.b2i_social_media_account: ~2 rows (approximately)
DELETE FROM `b2i_social_media_account`;
/*!40000 ALTER TABLE `b2i_social_media_account` DISABLE KEYS */;
INSERT INTO `b2i_social_media_account` (`social_account_id`, `profile_id`, `provider_name`, `provider_id`, `provider_key`, `profile_url`, `created_at`, `updated_at`) VALUES
	(4, 11, 'google', '101333257721044282806', '101333257721044282806', NULL, '2016-10-26 14:19:16', '2016-10-26 14:19:16'),
	(5, 12, 'facebook', '10154549762995256', '10154549762995256', NULL, '2016-10-27 02:30:18', '2016-10-27 02:30:18');
/*!40000 ALTER TABLE `b2i_social_media_account` ENABLE KEYS */;


-- Dumping structure for table b2i_db.b2i_user
CREATE TABLE IF NOT EXISTS `b2i_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(150) DEFAULT NULL,
  `remember_token` varchar(150) DEFAULT NULL,
  `verification_code` varchar(150) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL COMMENT 'mapped to b2i_roles',
  `status` int(11) DEFAULT NULL COMMENT '0=Inactive,1=Active,2=Suspended',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `FK_b2i_user_b2i_roles` FOREIGN KEY (`role_id`) REFERENCES `b2i_roles` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;

-- Dumping data for table b2i_db.b2i_user: ~9 rows (approximately)
DELETE FROM `b2i_user`;
/*!40000 ALTER TABLE `b2i_user` DISABLE KEYS */;
INSERT INTO `b2i_user` (`id`, `email`, `password`, `remember_token`, `verification_code`, `role_id`, `status`, `created_at`, `updated_at`) VALUES
	(9, 'karlmichailg@gmail.com', '$2y$10$P8yxvk5nW4JW.b0zdAJKauZBveplguAKdmAhOR5xwHNAZ5TLByDVu', 'IqHsErCdWYImsHAoKAGnpj0CdlkNqRueHWfvt5YWT7w5R7qi4XpLJWT6Txhw', '212842Z35', 3, 1, '2016-10-25 10:53:29', '2016-10-25 12:04:28'),
	(10, 'sample@company.com', '$2y$10$yc.p/XF0EjFXA3uIOh4.huLT78CQZ5zFo0rmuM0.M9l85U3AikZsS', 'O3kFEPd2SDUDLKab39b222VRnifh1udO0iArZyg0FqBndAiSqbUAD3yYSGyS', '1O7149753', 3, 1, '2016-10-25 12:05:48', '2016-10-26 14:09:01'),
	(13, 'john@incubixtech.com', '$2y$10$BOpqva6WIDgiWEXBve0P4.vqoPX1ZFYQ8kGKEFcqN8tZc/85yGALa', '0XZiNfqxdt9Q2ljSFLXRBE7qCzdNQbjoJU8iAgjqC9RGIQzwyuhRFG4vdGVN', '25a674152', 2, 1, '2016-10-25 22:25:46', '2016-10-27 03:05:26'),
	(14, 'jspnyc321@gmail.com', '$2y$10$eENQDQa.FYPnNSQSOsasZeBSyMA9NOaaeX3lf3VadkPGn9Di2DKJK', NULL, '7q6476706', 3, 1, '2016-10-25 22:26:42', '2016-10-25 22:27:49'),
	(15, 'corsinoronalyn@gmail.com', '$2y$10$byNQnpi4Xm3efS14nk1zGuDIIdBeJ8nxN0F97zBueDcySd8oIgjRq', 'Z4DhePk6lR7WRrnUK3GfFG3cqInZT5pW9KqQd7AnDNDN5HgAfA9uTm1CbQdG', '811260f08', 3, 1, '2016-10-26 01:50:22', '2016-11-11 09:54:43'),
	(16, 'blush.luminter@gmail.com', '$2y$10$/1hna/CjXiwSPGFkYgmnOe.jl/yYkBFEe1vX5SGNTw24rB5kdDxPS', 'pN68WP3MALJxp99zBNQPBrJTZ28f9lnCexvVgoJU4EIgK8Cw9m68UX38VHqJ', NULL, 2, 1, '2016-10-26 14:19:16', '2016-11-11 09:54:31'),
	(17, 'hazel.bernadette@hotmail.com', '$2y$10$lNXdJeEJ9XQPWjh76Ah3duPW3aFYVF2tZN0uN2vkvx.hEg4ikalUC', NULL, NULL, 2, 1, '2016-10-27 02:30:18', '2016-10-27 02:30:18'),
	(31, 'louie@incubixtech.com', '$2y$10$FgVsvc/RfW36uFjQm0SzI.2GIlfcl2YTkDdKW0McnSFjItZKxnR6W', 'LCokpm8g4fOabejk1P8aiDsP5ZG4Eie9UbdXK6QFkwY82jRIJlGRRgPWcPdj', 'Z46875119', 3, 1, '2016-10-28 09:04:15', '2016-11-03 13:13:38'),
	(32, 'admin@incubixtech.com', '$2y$10$FgVsvc/RfW36uFjQm0SzI.2GIlfcl2YTkDdKW0McnSFjItZKxnR6W', '3RCjD1Q62QoiAquUstzBNdmJ1XT0zLecjnN4l03MvVX7CKMbejl7DFRTPhKZ', NULL, 1, 1, '2016-11-02 20:39:57', '2016-11-11 09:52:47');
/*!40000 ALTER TABLE `b2i_user` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
