<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="initial-scale=1, width=device-width">
		@yield('title')
		<link rel="apple-touch-icon" sizes="180x180" href="{{ config('s3.bucket_link') . elixir('images/favicon/apple-touch-icon.png') }}">
		<link rel="icon" type="image/png" href="{{ config('s3.bucket_link') . elixir('images/favicon/favicon-32x32.png') }}" sizes="32x32">
		<link rel="icon" type="image/png" href="{{ config('s3.bucket_link') . elixir('images/favicon/favicon-16x16.png') }}" sizes="16x16">
		<link rel="manifest" href="/manifest.json">
		<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
		<meta name="theme-color" content="#ffffff">
		@yield('meta')
		<link href="{{ config('s3.bucket_link') . elixir('assets/app/app.css') }}" rel="stylesheet">
		<link href="{{ config('s3.bucket_link') . elixir('assets/app/aos.css') }}" rel="stylesheet">
		<style type="text/css">
			#sidenav-overlay {
				z-index: 0 !important;
			}
		</style>
		@yield('stylesheet')
	</head>
	<body>
		<div id="preloader">
			<div id="status">&nbsp;</div>
		</div>

		<header>
			<div class="navbar-fixed">
				<nav>
					<div class="container nav-wrapper">
						<a href="{{ route('app.index') }}" class="brand-logo">
							<img src="{{ config('s3.bucket_link') . elixir('images/assets/buzzin.png') }}" />
						</a>
						  <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
						<ul class="right hide-on-med-and-down">
							<li><a href="{{ route('app.about') }}">ABOUT</a></li>
							<li><a href="{{ route('app.business') }}">BUSINESS</a></li>
							<li><a href="{{ route('app.influencer') }}">INFLUENCER</a></li>
							<li><a href="{{ route('app.contact') }}">CONTACT US</a></li>
							@if(Auth::check())
								@if(Auth::user()->role_id == 2)
									<li><a href="/influencer/dashboard">MY DASHBOARD</a></li>
									<li><a href="/logout">LOGOUT</a></li>
								@elseif(Auth::user()->role_id == 3)
									<li><a href="/company/dashboard">MY DASHBOARD</a></li>
									<li><a href="/logout">LOGOUT</a></li>
								@else
									<li><a href="/admin/dashboard">MY DASHBOARD</a></li>
									<li><a href="/logout">LOGOUT</a></li>
								@endif
							@else
							<li><a href="{{ route('app.signup') }}">JOIN US</a></li>
							<li><a href="{{ route('app.login') }}">LOGIN</a></li>
							@endif
						</ul>
						<ul class="side-nav" id="mobile-demo">
							<li><a href="{{ route('app.about') }}">ABOUT</a></li>
							<li><a href="{{ route('app.business') }}">BUSINESS</a></li>
							<li><a href="{{ route('app.influencer') }}">INFLUENCER</a></li>
							<li><a href="{{ route('app.contact') }}">CONTACT US</a></li>
							@if(Auth::check())
								@if(Auth::user()->role_id == 2)
									<li><a href="/influencer/dashboard">MY DASHBOARD</a></li>
								@elseif(Auth::user()->role_id == 3)
									<li><a href="/company/dashboard">MY DASHBOARD</a></li>
								@else
									<li><a href="/admin/dashboard">MY DASHBOARD</a></li>
								@endif
							@else
							<li><a href="{{ route('app.signup') }}">JOIN US</a></li>
							<li><a href="{{ route('app.login') }}">LOGIN</a></li>
							@endif
					     </ul>
					</div>
				</nav>
			</div>
		</header>

		<main>
			<div class="page-wrap">

				@yield('content')
				@yield('custom-scripts')

			</div>
		</main>

		<footer class="page-footer grey darken-3" style="padding: 10px 0px;">
			<div class="footer-copyright">
				<div class="container">
					<a href="#" class="grey-text text-lighten-4">Copyright &copy; Blogapalooza Buzzin 2016</a>
					<a class="grey-text text-lighten-4 right" href="devhub.ph"></a>
				</div>
			</div>
		</footer>

		<script src="{{ config('s3.bucket_link') . elixir('assets/app/app.js') }}"></script>
		<script src="{{ config('s3.bucket_link') . elixir('assets/app/aos.js') }}"></script>
		@yield('footer')
	</body>
</html>
