<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="initial-scale=1, width=device-width">
		@yield('title')
		<link rel="apple-touch-icon" sizes="180x180" href="{{ config('s3.bucket_link') . elixir('images/favicon/apple-touch-icon.png') }}">
		<link rel="icon" type="image/png" href="{{ config('s3.bucket_link') . elixir('images/favicon/favicon-32x32.png') }}" sizes="32x32">
		<link rel="icon" type="image/png" href="{{ config('s3.bucket_link') . elixir('images/favicon/favicon-16x16.png') }}" sizes="16x16">
		<link rel="manifest" href="/manifest.json">
		<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
		<meta name="theme-color" content="#ffffff">
		<link href="{{ config('s3.bucket_link') . elixir('assets/influencer/influencer.css') }}" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="{{ config('s3.bucket_link') . elixir('assets/user/jquery.dataTables.min.css') }}">
		@yield('stylesheet')

	</head>
	<body>

		<header id="header" class="page-topbar">
		    <div class="navbar-fixed">
	            <nav class="nav-user">
					<div class="nav-wrapper">
						<a href="{{ route('app.index') }}" class="brand-logo center"><img src="{{ config('s3.bucket_link') . elixir('images/assets/buzzin-shadow.png') }}" /></a>
						<a href="#" data-activates="slide-out" class="sidebar-collapse  btn-medium waves-effect waves-light hide-on-large-only darken-2"><i class="material-icons" >menu</i></a>

						{{-- <ul class="right hide-on-med-and-down">
							<li class="waves-effect waves-light"><a href="{{ url('logout') }}"><i class="mdi mdi-power-settings large"></i></a></li>
						</ul> --}}
					</div>
	            </nav>
	        </div>
		</header>

		<div id="main">
			<div class="wrapper">
	            <aside id="left-sidebar-nav">
	                <ul id="slide-out" class="side-nav fixed leftside-navigation grey lighten-3">
	                    <li class="user-details grey darken-2 center">
		                    		<!-- <img src="{{ asset('images/user.jpg') }}" class="user-image left circle" width="50" height="50" /> -->
			                    	<span class="user-name white-text" title='<?php $user = App\Modules\User\Models\UserProfile::where('user_id', Auth::user()->id)->first();
echo $user->first_name . ' ' . $user->last_name;?>'><?php $user = App\Modules\User\Models\UserProfile::where('user_id', Auth::user()->id)->first();
echo $user->first_name . ' ' . $user->last_name;?></span>

	                    </li>

	                    <li>
	                    	<a href="{{ route('influencer.dashboard') }}" class="waves-effect waves-cyan"><i class="material-icons" >dashboard</i> Dashboard</a>
	                    </li>
	                    <li>
	                    	<a href="{{ route('influencer.campaign') }}" class="waves-effect waves-cyan"><i class="material-icons" >announcement</i> My Campaigns</a>
	                    </li>
	                    <li>
	                    	<a href="{{ route('influencer.invitation') }}" class="waves-effect waves-cyan"><i class="material-icons" >insert_invitation</i> Invitations</a>
	                    </li>
	        	        <li>
	                    	<a href="{{ route('influencer.application') }}" class="waves-effect waves-cyan"><i class="material-icons" >menu</i> Applications</a>
	                    </li>
	        	        <li>
	                    	<a href="{{ route('influencer.blogs.index') }}" class="waves-effect waves-cyan"><i class="material-icons" >mode_edit</i> Blogs</a>
	                    </li>
	                    <li class="divider"></li>
	                    <li class="no-padding">
	                    	<ul class="collapsible collapsible-accordion">
	                    		<li class="active">
	                    			<a class="collapsible-header active"><i class="material-icons">settings</i> Settings<i class="material-icons right">arrow_drop_down</i></a>
	                    			<div class="collapsible-body">
										<ul>
											<li><a href="{{ route('influencer.social.index') }}"><i class="material-icons">apps</i> Social Connections</a></li>
											<li><a href="{{ route('influencer.categories.index') }}"><i class="material-icons">list</i> Categories of Interest</a></li>
											<li><a href="{{ route('influencer.profile.index') }}"><i class="material-icons">account_box</i> Profile Settings</a></li>
											<li><a href="{{ route('influencer.password.index') }}"><i class="material-icons">lock</i> Password Settings</a></li>
										</ul>
									</div>
	                    		</li>
	                    	</ul>
	                    </li>
	                    <li>
	                    	<a href="{{ route('app.logout') }}" class="waves-effect waves-cyan"><i class="material-icons" >settings_power</i> Logout</a>
	                    </li>
	                </ul>
	            </aside>

	            <section id="content">

		            @yield('content')
		            @yield('custom-scripts')

	            </section>

			</div>
		</div>
		<footer class="footer grey darken-2">
	        <div class="footer-copyright">
	            <div class="container white-text">
	                Copyright © 2016 <a class="grey-text text-lighten-4" href="!#" target="_blank">Buzzin by Blogapalooza</a>
	            </div>
	        </div>
	    </footer>

		<script src="{{ config('s3.bucket_link') . elixir('assets/influencer/influencer.js') }}"></script>
		@yield('footer')
	</body>
</html>
