<!DOCTYPE html>
<html>
<head>
	<title></title>


		<style type="text/css">
			.container {
				padding: 30px;
				width: 100%;
			}

			.contact-page {
				color:#000 !important; font-family: Arial, Helvetica, sans-serif; width:100%;
			}

			.contact-page-panel {
				width:600px; margin: 0 auto;
			}

			.contact-page-header {
				padding: 1em 0; background: #da7027;
			}

			.collection {
				border: 0px solid #e0e0e0;
			}

			.collection .collection-item {
				border-bottom: 0px solid #e0e0e0 !important;
			}
			.contact-fields {
				padding: 20px !important;

			}

			.card-panel {
				transition: box-shadow .25s;
			    padding: 20px;
			    margin: auto;
			    border-radius: 2px;
			    background-color: #fff;
			    box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
			}

			.collection {
			    border: 1px solid #e0e0e0;
			    border-radius: 2px;
			    overflow: hidden;
			    position: relative;
			}

			.collection.with-header .collection-header {
			    background-color: #fff;
			    border-bottom: 1px solid #e0e0e0;
			    padding: 10px 20px;
			}

			.collection .collection-item {
			    background-color: #fff;
			    line-height: 1.5rem;
			    padding: 10px 20px;
			    margin: 0;
			    border-bottom: 1px solid #e0e0e0;
			}
			ul {
			    list-style-type: none;
			}
		</style>


</head>
<body>


	<div class="container contact-page">
		<div class="card-panel contact-page-panel">
			<center>
				<div class="contact-page-header">
					<img alt="Buzzin" src="http://d209mjazlw7bf7.cloudfront.net/build/images/assets/buzzinlogo2-e728cfebb0.png" style="margin-left: 1em; width: 110px;"/>
				</div>
			</center>

			<div style='line-height: 120%;'>
				<br>
				     <ul class="collection with-header">
					        <li class="collection-header"><h4>Someone messaged us.</h4></li>
					        <li class="collection-item contact-fields">
					        	<span class="left">Email:&nbsp;</span><span class="right">{{ $data['email_user'] }}</span>
					        </li>
					        <li class="collection-item contact-fields">
					        	<span class="left">Full Name:&nbsp;</span><span class="right">{{ $data['name'] }}</span>
					        </li>
					        <li class="collection-item contact-fields">
					        	<span class="left">Contact Number:&nbsp;</span><span class="right">{{ $data['contact'] }}</span>
					        </li>
					        <li class="collection-item contact-fields">
					        	<span class="left">Subject:&nbsp;</span><span class="right">{{ $data['subject'] }}</span>
					        </li>
					        <li class="collection-item contact-fields">
					        	<span class="left">Role:&nbsp;</span><span class="right">{{ $data['i_am'] }}</span>
					        </li>
					        <li class="collection-item contact-fields">
					        	<span class="left">Interested in:&nbsp;</span><span class="right">{{ $data['interested_in'] }}</span>
					        </li>
					        <li class="collection-item contact-fields">
					        	<span class="left">Message:&nbsp;</span><span class="right">{{ $data['message'] }}</span>
					        </li>
				      </ul>
				<br/><br/><br/>
				<center>
					<small>
						Copyright &copy; 2016 Buzzin
					</small>
				</center>
			</div>
		</div>
	</div>


</body>
</html>
