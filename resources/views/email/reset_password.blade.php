<!DOCTYPE html>
<html>
<head>
	<title></title>

		<style type="text/css">

		.pswd-reset {
			 color:#000 !important; font-family: Arial, Helvetica, sans-serif; width:100%;
			 padding: 30px;
		}
		.pswd-body {
			 width:600px; margin: 0 auto;
		}
		.pswd-logo {
			padding: 1em 0; background: #9c27b0;
		}
		.generated-pswd {
			padding-bottom: 20px;
		}
		.b2i-orange {
			color: #da7027;
			font-weight: bold;
		}
		.card-panel {
			transition: box-shadow .25s;
		    padding: 20px;
		    margin: auto;
		    border-radius: 2px;
		    background-color: #fff;
		    border: 1px solid #bbb;
		}


		</style>
</head>
<body>

<div class="container pswd-reset">
	<div class="card-panel pswd-body">
		<center>
			<div class="pswd-logo">
				<img alt="Buzzin" src="http://d209mjazlw7bf7.cloudfront.net/build/images/assets/buzzinlogo2-e728cfebb0.png" style="margin-left: 1em; width: 100px;"/>
			</div>
			<h3 style='letter-spacing: 2px; color:#000 !important;'> Password Reset</h3>
			<div style='margin: 1em !important; line-height: 150%;'>
				<p style='font-size: 15px; letter-spacing: 1px; color:#000 !important;'>
					We received your request!<br/>
					We generated a temporary password to your account.<br/>
					Please log in to <span class="b2i-orange">Buzzin</span> with the password below.
				</p>
				<br>
					<div class="generated-pswd">
						<span  style='-webkit-border-radius: 3px;-moz-border-radius: 3px;border-radius: 3px;font-family: Arial;color: #ffffff;font-size: 20px;background: #9c27b0;padding: 15px;text-decoration: none; width:80%; display:block; margin: 0 auto; text-shadow: 1px 0px 2px #000; letter-spacing: 1px;'>
							<strong>{{ $data['temp_password'] }}</strong>
						</span>
					</div>
					Click <a href="http://letsbuzzin.com/login" class="b2i-orange" style='text-decoration: none';> here </a>to login to your account.<br/><br/>
				<small>
					Copyright &copy; 2016 Buzzin
				</small>
			</div>
		</center>
	</div>
</div>
</body>
</html>
