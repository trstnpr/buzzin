<!DOCTYPE html>
<head>
	<style type="text/css">

		.container {
			padding: 30px;
			width: 100%;
		}
		.email-activation {
			color:#000 !important;
			font-family: Arial, Helvetica, sans-serif;

		}
		.section-email {
			width:600px;
			margin: 0 auto;
		}
		.activation-logo {
			padding: 1em 0; background: #9c27b0;
		}
		.activation-logo img {
			margin-left: 1em;
			width: 100px;
		}
		.activate-btn {
			margin: 0 auto;
			display: block;

		}
		.btn-act {
			padding-bottom: 20px;
		}
		small {

			color: #000;
		}
		.activation-text {
			font-size: 120%; letter-spacing: 1px; color:#000 !important;
		}
		.activation-head-text {
			letter-spacing: 2px; color:#000 !important;
		}


		.card-panel {
			transition: box-shadow .25s;
		    padding: 20px;
		    margin: auto;
		    border-radius: 2px;
		    background-color: #fff;
		    box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
		}

		.btn-large {
		    text-decoration: none;
		    color: #fff;
		    background-color: #26a69a;
		    text-align: center;
		    letter-spacing: .5px;
		    transition: .2s ease-out;
		    cursor: pointer;
		    border: none;
		    border-radius: 2px;
		    display: inline-block;
		    height: 36px;
		    line-height: 36px;
		    outline: 0;
		    padding: 0 2rem;
		    text-transform: uppercase;
		    vertical-align: middle;
		    -webkit-tap-highlight-color: transparent;

		}

		.purple {
		    background-color: #9c27b0 !important;
		}

	</style>

</head>
<body>
	<div class="container">
		<div class="email-activation">
			<div class="section-email card-panel">
				<center>
					<div class="activation-logo">
						<img alt="Buzzin" src="http://d209mjazlw7bf7.cloudfront.net/build/images/assets/buzzinlogo2-e728cfebb0.png" />
					</div>
					<h2 class="activation-head-text"></h2>
					<div>
						<p class="activation-text">
							Your application in the campaign<br/>
							has been approved.<br/>
							Please share the link below in your blogs.
						</p>
						<br>
						<a href="{{ $data['link'] }}">
							{{ $data['link'] }}
						</a><br/><br/>
						<small>
							Copyright &copy; 2016 Buzzin
						</small>
					</div>
				</center>
			</div>
		</div>
	</div>
</body>
</html>
