<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="initial-scale=1, width=device-width">
		@yield('title')
		<link rel="apple-touch-icon" sizes="180x180" href="{{ config('s3.bucket_link') . elixir('images/favicon/apple-touch-icon.png') }}">
		<link rel="icon" type="image/png" href="{{ config('s3.bucket_link') . elixir('images/favicon/favicon-32x32.png') }}" sizes="32x32">
		<link rel="icon" type="image/png" href="{{ config('s3.bucket_link') . elixir('images/favicon/favicon-16x16.png') }}" sizes="16x16">
		<link rel="manifest" href="/manifest.json">
		<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
		<meta name="theme-color" content="#ffffff">
		<link href="{{ config('s3.bucket_link') . elixir('assets/app/app.css') }}" rel="stylesheet">
		@yield('stylesheet')
	</head>
	<body>
		<div id="preloader">
			<div id="status">&nbsp;</div>
		</div>

		<main>
			<div class="page-wrap">

				@yield('content')
				@yield('custom-scripts')

			</div>
		</main>

		<script src="{{ config('s3.bucket_link') . elixir('assets/app/app.js') }}"></script>
		@yield('footer')
	</body>
</html>
