<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="initial-scale=1, width=device-width">
		@yield('title')
		<link rel="apple-touch-icon" sizes="180x180" href="{{ config('s3.bucket_link') . elixir('images/favicon/apple-touch-icon.png') }}">
		<link rel="icon" type="image/png" href="{{ config('s3.bucket_link') . elixir('images/favicon/favicon-32x32.png') }}" sizes="32x32">
		<link rel="icon" type="image/png" href="{{ config('s3.bucket_link') . elixir('images/favicon/favicon-16x16.png') }}" sizes="16x16">
		<link rel="manifest" href="/manifest.json">
		<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
		<meta name="theme-color" content="#ffffff">
		<link href="{{ config('s3.bucket_link') . elixir('assets/company/company.css') }}" rel="stylesheet">
		@yield('stylesheet')

	</head>
	<body>

		<header id="header" class="page-topbar">
		    <div class="navbar-fixed">
	            <nav class="nav-user">
	                <div class="nav-wrapper">
						<a href="{{ route('app.index') }}" class="brand-logo center"><img src="{{ config('s3.bucket_link') . elixir('images/assets/buzzin-shadow.png') }}" /></a>
						<a href="#" data-activates="slide-out" class="sidebar-collapse  btn-medium waves-effect waves-light hide-on-large-only darken-2"><i class="material-icons" >menu</i></a>
					</div>
	            </nav>
	        </div>
		</header>

		<?php
$profileUpdate = App\Modules\User\Models\UserProfile::where('user_id', Auth::user()->id)->first();
$company = App\Modules\Company\Models\Company::where('profile_id', $profileUpdate['profile_id'])->first();
?>
		<div id="main">
			<div class="wrapper">
	            <aside id="left-sidebar-nav">
	                <ul id="slide-out" class="side-nav fixed leftside-navigation grey lighten-3">
	                    <li class="user-details grey darken-2 center">
		                    		<!-- <img src="{{ asset('images/user.jpg') }}" class="user-image left circle" width="50" height="50" /> -->
			                    	<span class="user-name white-text" title='{{ $company['company_name'] }}'>{{ $company['company_name'] }}</span>
	                    </li>

	                    <li>
	                    	<a href="{{ url('company/dashboard') }}" class="waves-effect waves-cyan"><i class="material-icons">dashboard</i> Dashboard</a>
	                    </li>
	                    <li>
	                    	<a href="{{ url('company/campaign') }}" class="waves-effect waves-cyan"><i class="material-icons">announcement</i> Campaign</a>
	                    </li>
	                    <li class="divider"></li>
	                    <li class="no-padding">
	                    	<ul class="collapsible collapsible-accordion">
	                    		<li class="active">
	                    			<a class="collapsible-header active"><i class="material-icons">settings</i> Settings<i class="material-icons right">arrow_drop_down</i></a>
	                    			<div class="collapsible-body">
										<ul>
											<li><a href="{{ url('company/settings') }}"><i class="material-icons">account_box</i>  Profile Settings</a></li>
											<li><a href="{{ url('company/passwordSetting') }}"><i class="material-icons">settings_application</i> Password Settings</a></li>
										</ul>
									</div>
	                    		</li>
	                    	</ul>
	                    </li>
	                    <li>
	                    	<a href="{{ url('logout') }}" class="waves-effect waves-cyan"><i class="material-icons">settings_power</i> Logout</a>
	                    </li>
	                </ul>
	            </aside>

	            <section id="content">

		            @yield('content')

	            </section>

			</div>
		</div>

		<footer class="page-footer grey darken-2" style="padding-top: 0px !important;">
	        <div class="footer-copyright">
	            <div class="container white-text">
	                Copyright © 2016 <a class="grey-text text-lighten-4" href="!#" target="_blank">Buzzin by Blogapalooza</a>
	            </div>
	        </div>
	    </footer>

		<script src="{{ config('s3.bucket_link') . elixir('assets/company/company.js') }}"></script>
		@yield('footer')
	</body>
</html>
