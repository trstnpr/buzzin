$(document).ready(function() {
    $('#form_bus_login').on('submit', (function(e){
        e.preventDefault();
        $.ajax({
            url: "login/authenticate",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function(){ $('#btn_bus').html('Processing...');},
            error: function(data){
                if(data.readyState == 4){
                    errors = JSON.parse(data.responseText);
                    $('#result_div').empty();
                    $('#result_div').html('<ul class="error_list fa-ul white-text"></ul>');
                    $.each(errors,function(key,value){
                        $('.error_list').append('<li><i class="fa fa-li fa-times"></i> '+value+'</li>');
                    });
                    $('#result_div').removeClass('teal hide').addClass('red');
                    $('#password').val('');
                    $('#btn_bus').html('Submit');

                    setTimeout(function(){$('#result_div').addClass('hide').html(''); }, 4000);
                }
            },

            success: function(data){
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    $("#btn_bus").html('Submit');
                    Materialize.toast('Login Successfully', 4000, 'green');
                    location.replace(msg.message);

                } else{
                    $("#password").val('');
                    $("#btn_bus").html('Submit');
                    Materialize.toast(msg.message, 4000, 'red');
                    /*$("#result_div").removeClass('teal hide').addClass('red').html(msg.dialog);*/
                }
            }
        });
    }));

    $('#form_blog_login').on('submit', (function(e){
        e.preventDefault();
        $.ajax({
            url: "login/authenticate",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function(){ $('#btn_blog').html('Processing...');},
            error: function(data){
                if(data.readyState == 4){
                    errors = JSON.parse(data.responseText);
                    $('#result_div').empty();
                    $('#result_div').html('<ul class="error_list fa-ul white-text"></ul>');
                    $.each(errors,function(key,value){
                        $('.error_list').append('<li><i class="fa fa-li fa-times"></i> '+value+'</li>');
                    });
                    $('#result_div').removeClass('teal hide').addClass('red');
                    $('#password1').val('');
                    $('#btn_blog').html('Submit');

                    setTimeout(function(){$('#result_divs').addClass('hide').html(''); }, 4000);
                }
            },
            success: function(data){
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    $("#btn_blog").html('Submit');
                    Materialize.toast('Login Successfully', 4000, 'green');
                    location.replace(msg.message);

                } else{
                    $("#password1").val('');
                    $("#btn_blog").html('Submit');
                    Materialize.toast(msg.message, 4000, 'red');
                    /*$("#result_div").removeClass('teal hide').addClass('red').html(msg.dialog);*/
                }
            }
        });
    }));

    $('#forgot_password_form').on('submit', (function(e){
        e.preventDefault();
        $.ajax({
            url: "password/reset",
            type: "POST",
            headers:
            {
                'X-CSRF-Token': $('#_tokens').val()
            },
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function(){ $('#btn-reset-password').html('Processing...');
                                        document.getElementById("btn-reset-password").disabled = true;  },
            error: function(data){
                if(data.readyState == 4){
                    errors = JSON.parse(data.responseText);
                    $('#result_div').empty();
                    $('#result_div').html('<ul class="error_list fa-ul white-text"></ul>');
                    $.each(errors,function(key,value){
                        $('.error_list').append('<li><i class="fa fa-li fa-times"></i> '+value+'</li>');
                    });
                    $('#result_div').removeClass('teal hide').addClass('red');
                    $('#password').val('');
                    $('#btn-reset-password').html('Reset');

                    setTimeout(function(){$('#result_div').addClass('hide').html(''); }, 4000);
                }
            },

            success: function(data){
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    Materialize.toast(msg.message, 4000, 'green');
                    location.reload();
                } else{
                    $("#emails").val('');
                    $("#btn-reset-password").html('Submit');
                    Materialize.toast(msg.message, 4000, 'red');
                    /*$("#result_div").removeClass('teal hide').addClass('red').html(msg.dialog);*/
                }
            }
        });
    }));
});