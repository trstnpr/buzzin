    $('#form_contact_us').on('submit', (function(e){
        e.preventDefault();
        $.ajax({
            url: "/contactus/store",
            type: "POST",
            headers:
            {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function(){ $('#btn_contact').html('Processing...');
                                    document.getElementById("btn_contact").disabled = true; },
            error: function(data){
                if(data.readyState == 4){
                    errors = JSON.parse(data.responseText);
                    $('#result_div').empty();
                    $('#result_div').html('<ul class="error_list fa-ul white-text"></ul>');
                    $.each(errors,function(key,value){
                        $('.error_list').append('<li><i class="fa fa-li fa-times"></i> '+value+'</li>');
                    });
                    $('#result_div').removeClass('teal hide').addClass('red');
                    $('#btn_contact').html('Submit');
                    document.getElementById("btn_contact").disabled = false;

                    setTimeout(function(){$('#result_div').addClass('hide').html(''); }, 4000);
                }
            },
            success: function(data){
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    Materialize.toast(msg.message, 400, 'green');
                    location.reload();

                } else{
                    Materialize.toast(msg.message, 4000, 'red');
                    location.reload();
                }
            }
        });
    }));
