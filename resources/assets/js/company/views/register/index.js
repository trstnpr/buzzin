//SIGNUP JQUERY FUNCTION
$(document).ready(function() {
    $('#form_bus_registration').on('submit', (function(e){
        e.preventDefault();
        $.ajax({
            url: "signup/register",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function(){ $('#btn_business').html('Processing...');
                                    document.getElementById("btn_business").disabled = true;  },
            error: function(data){
                if(data.readyState == 4){
                    errors = JSON.parse(data.responseText);
                    $('#result_div').empty();
                    $('#result_div').html('<ul class="error_list fa-ul white-text"></ul>');
                    $.each(errors,function(key,value){
                        $('.error_list').append('<li class="white-text"><i class="fa fa-li fa-times white-text"></i> '+value+'</li>');
                    });
                    $('#result_div').removeClass('teal hide').addClass('red');
                    $('#password').val('');
                    $('#confirm_password').val('');
                    $('#btn_business').html('Submit');
                    document.getElementById("btn_business").disabled = false;

                    setTimeout(function(){$('#result_div').addClass('hide').html(''); }, 4000);
                }
            },
            success: function(data){
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    $("#btn_business").html('Submit');
                    Materialize.toast(msg.message, 400, 'green');
                    window.location.reload();

                } else{
                    Materialize.toast(msg.message, 4000, 'red');
                    $("#password").val('');
                    $("#confirm_password").val('');
                    $("#btn_business").html('Submit');
                    document.getElementById("btn_business").disabled = false;
                    /*$("#result_div").removeClass('teal hide').addClass('red').html(msg.dialog);*/
                }
            }
        });
    }));

    $('#form_blog_registration').on('submit', (function(e){
        e.preventDefault();
        $.ajax({
            url: "signup/register",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function(){ $('#btn_blog').html('Processing...');
                                    document.getElementById("btn_blog").disabled = true; },
            error: function(data){
                if(data.readyState == 4){
                    errors = JSON.parse(data.responseText);
                    $('#result_div').empty();
                    $('#result_div').html('<ul class="error_list fa-ul white-text"></ul>');
                    $.each(errors,function(key,value){
                        $('.error_list').append('<li class="white-text"><i class="fa fa-li fa-times white-text"></i> '+value+'</li>');
                    });
                    $('#result_div').removeClass('teal hide').addClass('red');
                    $('#password1').val('');
                    $('#confirm_password1').val('');
                    $('#btn_blog').html('Submit');
                    document.getElementById("btn_blog").disabled = false;

                    setTimeout(function(){$('#result_div').addClass('hide').html(''); }, 4000);
                }
            },
            success: function(data){
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    $("#btn_blog").html('Submit');
                    Materialize.toast(msg.message, 4000, 'green');
                    window.location.reload();

                } else{
                    Materialize.toast(msg.message, 4000, 'red');
                    $("#password1").val('');
                    $("#confirm_password1").val('');
                    $("#btn_blog").html('Submit');
                    document.getElementById("btn_blog").disabled = false;
                    /*$("#result_div").removeClass('teal hide').addClass('red').html(msg.dialog);*/
                }
            }
        });
    }));

});