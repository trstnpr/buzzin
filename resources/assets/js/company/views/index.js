	function showDate() {
	    var today = new Date();

	    var month = new Array();
	        month[0] = "Jan";
	        month[1] = "Feb";
	        month[2] = "Mar";
	        month[3] = "Apr";
	        month[4] = "May";
	        month[5] = "Jun";
	        month[6] = "Jul";
	        month[7] = "Aug";
	        month[8] = "Sept";
	        month[9] = "Oct";
	        month[10] = "Nov";
	        month[11] = "Dec";

	    var weekday = new Array();
	        weekday[0]=  "Sun";
	        weekday[1] = "Mon";
	        weekday[2] = "Tue";
	        weekday[3] = "Wed";
	        weekday[4] = "Thu";
	        weekday[5] = "Fri";
	        weekday[6] = "Sat";

	    var M = month[today.getMonth()];
	    var d = today.getDate();
	    var Y = today.getFullYear();
	    var D = weekday[today.getDay()];

	    historyDate = M + " "+d+","+Y+" "+D;
	    $('#day_').html(M + " " + d);
	    $('#year_').html(Y + " ");
	    $('#date_').html(D+ " ");
	}
	showDate();

	function checkTime(i) {
	    if (i < 10) {
	        i = "0" + i;
	    }
	    return i;
	}

	function startTime() {
	    var today = new Date();
	    var h = today.getHours();
	    var m = today.getMinutes();
	    var s = today.getSeconds();
	    var ampm = h >= 12 ? 'PM' : 'AM';
	    // add a zero in front of numbers<10
	    m = checkTime(m);
	    s = checkTime(s);
	    h = h % 12;
	    h = h ? h : 12;
	    h = checkTime(h);
	   
	    globalHour = h + ":" + m;
	    globalAmpm = ampm;
	    $('#time').html(h + ":" + m);
	    $('#ampm').html(ampm);

	    t = setTimeout(function () {
	        startTime()
	    }, 500);
	}
	startTime();