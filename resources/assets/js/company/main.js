$(".button-collapse").sideNav();
$('.sidebar-collapse').sideNav();
$('.tooltipped').tooltip({delay: 50});
$('.materialboxed').materialbox();
$('.datepicker').pickadate({
	selectMonths: true,
	selectYears: 20,
	closeOnSelect: true,
	onSet: function () {
        this.close();
    }
});
$('.material-select').material_select();
$('.modal').modal();