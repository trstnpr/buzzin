$(document).ready(function(){
    $('#campaign-form').on('submit', (function(e){
        e.preventDefault();
        var action = $(this).attr('action');
        $.ajax({
            url: action,
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function(){ $('#campaign-button').html('Processing...');},
            error: function(data){
                if(data.readyState == 4){
                    errors = JSON.parse(data.responseText);
                    message = '<ul>';
                    $.each(errors,function(key,value){
                        message += '<li>'+value+'</li>';
                    });
                    message += '</ul>';
                    $('#campaign-button').html('Save Changes');
                    Materialize.toast(message, 4000, 'red');
                }
            },
            success: function(data){
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    Materialize.toast(msg.message, 1000, "green", function(){ window.location.reload(); });
                } else{
                    $('#campaign-button').html('Save Changes');
                    Materialize.toast(msg.message, 4000, 'red');
                }
            }
        });
    }));
});

function updateCampaign(type, status, campaign_id){
    var action = $('#action').val();
    var data = {
        'campaign_id' : campaign_id,
        'type' : type,
        'status' : status
    }
    $.ajax({
        url: '/influencer/campaign/'+campaign_id+'/update',
        type: "POST",
        headers:
        {
            'X-CSRF-Token': $('input[name="_token"]').val()
        },
        data: data,
        cache: false,
        beforeSend: function(){ $('#'+status+'-button').html('Processing...');},
        error: function(data){
            if(data.readyState == 4){
                errors = JSON.parse(data.responseText);
                message = '<ul>';
                $.each(errors,function(key,value){
                    message += '<li>'+value+'</li>';
                });
                message += '</ul>';
                $('#'+status+'-button').html(status);
                Materialize.toast(message, 4000, 'red');
            }
        },
        success: function(data){
            var msg = JSON.parse(data);
            if(msg.result == 'success'){
                Materialize.toast(msg.message, 1000, "green", function(){ window.location.reload(); });
            } else{
                $('#'+status+'-button').html(status);
                Materialize.toast(msg.message, 4000, 'red');
            }
        }
    });
}

    $('.btn-delete-yes-campaign').on('click', function() {
        $.ajax({
            url: '/influencer/campaign/delete/'+$(this).attr('data-campaign-id'),
            headers:
            {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
            type: 'DELETE',
            success: function(data) {
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    Materialize.toast(msg.message, 4000, 'green');
                    window.location.href = "/influencer/dashboard";
                } else{
                    Materialize.toast(msg.message, 4000, 'red');
                    location.reload();
                }
            }
        });
    });