$(document).ready(function(){
    $('.modal').modal();
    
    $('#blog-form').on('submit', (function(e){
        e.preventDefault();
        var action = $(this).attr('action');
        $.ajax({
            url: action,
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function(){ $('#blog-button').html('Processing...');},
            error: function(data){
                if(data.readyState == 4){
                    errors = JSON.parse(data.responseText);
                    message = '<ul>';
                    $.each(errors,function(key,value){
                        message += '<li>'+value+'</li>';
                    });
                    message += '</ul>';
                    $('#blog-button').html('Submit');
                    Materialize.toast(message, 4000, 'red');
                }
            },
            success: function(data){
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    Materialize.toast(msg.message, 1000, "green", function(){ window.location.href = msg.redirect; });
                } else{
                    $('#blog-button').html('Submit');
                    Materialize.toast(msg.message, 4000, 'red');
                }
            }
        });
    }));

    var nColNumber = -1;
    var blogs = $('#influencerblogs').DataTable({
        'ajax': '/influencer/blogs/getAll',
        'processing': true,
        'order': [[ 1, "desc" ]],
        'columnDefs': [
            { 'targets': [ ++nColNumber ], 'title':'Blog Name', 'name': 'name', 'data': 'name' },
            { 'targets': [ ++nColNumber ], 'title':'Blog Category', 'name': 'category', 'data': 'category' },
            { 'targets': [ ++nColNumber ], 'title':'Blog URL', 'name': 'url', 'data': 'url' },
            { 'className': 'center', 'targets': [ ++nColNumber ], 'title':'Actions', 'name': 'action', 'data': 'action'},
           
        ]
    });

    function reloadTable(table)
    {
        table.ajax.reload();
    }

    /*function deleteBlog(blog) {
        $('#delete-blog').openModal();
        $('.btn-delete-yes-blog').attr('data-blog-id', $(campaign).attr('data-blog-id'));
    }*/

    $('.btn-delete-yes-blog').on('click', function() {
        $.ajax({
            url: 'blogs/delete/'+$(this).attr('data-blog-id'),
            headers:
            {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
            type: 'DELETE',
            success: function(data) {
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    Materialize.toast(msg.message, 4000, 'green');
                    reloadTable(blogs);
                } else{
                    Materialize.toast(msg.message, 4000, 'red');
                    location.reload();
                }
            }
        });
    });
});