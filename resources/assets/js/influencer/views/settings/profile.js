$(document).ready(function(){
    $('#profile-form').on('submit', (function(e){
        e.preventDefault();
        var action = $(this).attr('action');
        $.ajax({
            url: action,
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function(){ $('#profile-button').html('Processing...');},
            error: function(data){
                if(data.readyState == 4){
                    errors = JSON.parse(data.responseText);
                    message = '<ul>';
                    $.each(errors,function(key,value){
                        message += '<li>'+value+'</li>';
                    });
                    message += '</ul>';
                    $('#profile-button').html('Submit');
                    Materialize.toast(message, 4000, 'red');
                }
            },
            success: function(data){
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    Materialize.toast(msg.message, 1000, "green", function(){ window.location.reload(); });
                } else{
                    $('#profile-button').html('Submit');
                    Materialize.toast(msg.message, 4000, 'red');
                }
            }
        });
    }));
});