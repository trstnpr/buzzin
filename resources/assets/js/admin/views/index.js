function reloadTable(table)
    {
        table.ajax.reload();
    }

    $('.btn-delete-yes-campaign').on('click', function() {
        $.ajax({
            url: 'campaign/'+$(this).attr('data-campaign-id'),
            headers:
            {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
            type: 'DELETE',
            success: function(data) {
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    Materialize.toast(msg.message, 4000, 'green');
                    location.reload();
                } else{
                    Materialize.toast(msg.message, 4000, 'red');
                    location.reload();
                }
            }
        });
    });

    $('.btn-delete-yes-business').on('click', function() {
        $.ajax({
            url: 'business/'+$(this).attr('data-business-id'),
            headers:
            {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
            type: 'DELETE',
            success: function(data) {
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    Materialize.toast(msg.message, 4000, 'green');
                    location.reload();
                } else{
                    Materialize.toast(msg.message, 4000, 'red');
                    location.reload();
                }
            }
        });
    });

    $('.btn-delete-yes-influencer').on('click', function() {
        $.ajax({
            url: 'influencer/'+$(this).attr('data-influencer-id'),
            headers:
            {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
            type: 'DELETE',
            success: function(data) {
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    Materialize.toast(msg.message, 4000, 'green');
                    location.reload();
                } else{
                    Materialize.toast(msg.message, 4000, 'red');
                    location.reload();
                }
            }
        });
    });