gulp = require('gulp');

var elixir = require('laravel-elixir');
require('laravel-elixir-bowerbundle');

var AWS = require('aws-sdk');
var config = new AWS.Config().loadFromPath('upload.json');
var s3 = require('gulp-s3-upload')(config);

var bower_path = "./bower_components";
var paths = {
  'bootstrap'  : bower_path + "/bootstrap-sass/assets"
};
elixir.config.sourcemaps = false;

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    // General
    mix.bower(['jquery', 'materialize', 'datatables.net'], 'public/assets/bundle.js');
    mix.copy('bower_components/materialize/fonts', 'public/build/assets/fonts');
    mix.copy('bower_components/font-awesome/fonts', 'public/build/assets/fonts');
    mix.copy('bower_components/material-design-icons/iconfont', 'public/build/assets/fonts');
    mix.copy('bower_components/mdi/fonts', 'public/build/assets/fonts');
    mix.copy('resources/assets/fonts', 'public/build/assets/fonts');

    // App
    mix.copy('bower_components/aos/dist', 'public/assets/app');
    mix.scripts(['public/assets/bundle.js','resources/assets/js/app/main.js'], 'public/assets/app/app.js', './');
    mix.copy('resources/assets/js/app/slick.min.js', 'public/assets/app');
    mix.sass('app/main.scss', 'public/assets/app/app.css');
    mix.sass('app/views/index.scss', 'public/assets/app/views/index.css');
    mix.copy('resources/assets/js/app/views', 'public/assets/app/views');

    // User
    mix.copy('bower_components/datatables.net-dt/css/jquery.dataTables.min.css', 'public/assets/user');
    mix.scripts(['public/assets/bundle.js','resources/assets/js/user/main.js'], 'public/assets/user/user.js', './');
    mix.sass('user/main.scss', 'public/assets/user/user.css');
    mix.sass('user/views/index.scss', 'public/assets/user/views/index.css');
    mix.copy('resources/assets/js/user/views', 'public/assets/user/views');

    // Company
    mix.copy('bower_components/datatables.net-dt/css/jquery.dataTables.min.css', 'public/assets/company');
    mix.scripts(['public/assets/bundle.js','resources/assets/js/company/main.js'], 'public/assets/company/company.js', './');
    mix.sass('company/main.scss', 'public/assets/company/company.css');
    mix.sass('company/views/index.scss', 'public/assets/company/views/index.css');
    mix.copy('resources/assets/js/company/views', 'public/assets/company/views');

    // Influencer
    mix.scripts(['public/assets/bundle.js','resources/assets/js/influencer/main.js'], 'public/assets/influencer/influencer.js', './');
    mix.sass('influencer/main.scss', 'public/assets/influencer/influencer.css');
    mix.sass('influencer/views/index.scss', 'public/assets/influencer/views/index.css');
    mix.copy('resources/assets/js/influencer/views', 'public/assets/influencer/views');

    // Admin
    mix.copy('bower_components/datatables.net-dt/css/jquery.dataTables.min.css', 'public/assets/admin');
    mix.scripts(['public/assets/bundle.js','resources/assets/js/admin/main.js'], 'public/assets/admin/admin.js', './');
    mix.sass('admin/main.scss', 'public/assets/admin/admin.css');
    mix.sass('admin/views/index.scss', 'public/assets/admin/views/index.css');
    mix.copy('resources/assets/js/admin/views', 'public/assets/admin/views');

    // Versioning & Cache Busting
    mix.version([
        'public/assets/app/app.css',
        'public/assets/app/app.js',
        'public/assets/app/views/index.css',
        'public/assets/app/views/index.js',
        'public/assets/app/slick.min.js',
        'public/assets/app/views/*',
        'public/assets/app/aos.css',
        'public/assets/app/aos.js',

        'public/assets/user/jquery.dataTables.min.css',
        'public/assets/user/user.css',
        'public/assets/user/user.js',
        'public/assets/user/views/index.css',
        'public/assets/user/views/index.js',
        'public/assets/user/views/*',

        'public/assets/admin/jquery.dataTables.min.css',
        'public/assets/admin/admin.css',
        'public/assets/admin/admin.js',
        'public/assets/admin/views/index.css',
        'public/assets/admin/views/index.js',
        'public/assets/admin/views/*',

        'public/assets/influencer/influencer.css',
        'public/assets/influencer/influencer.js',
        'public/assets/influencer/views/index.css',
        'public/assets/influencer/views/index.js',
        'public/assets/influencer/views/*',

        'public/assets/company/jquery.dataTables.min.css',
        'public/assets/company/company.css',
        'public/assets/company/company.js',
        'public/assets/company/views/index.css',
        'public/assets/company/views/index.js',
        'public/assets/company/views/*',

        'public/images/*'
    ]);
});

/**
 * Define a new gulp task upload that uploads current static assets to S3.
 * Run gulp --production first before running this task.
 */
gulp.task('uploadDev', function() {
    gulp.src('public/build/**')
        .pipe(s3({
            Bucket: '/dev-b2i/build',
            ACL:    'public-read'
        }, {
            maxRetries: 50
        }))
    ;
    gulp.src('public/images/**')
        .pipe(s3({
            Bucket: '/dev-b2i/images/',
            ACL:    'public-read'
        }, {
            maxRetries: 50
        }))
    ;
});

/**
 * Define a new gulp task upload that uploads current static assets to S3.
 * Run gulp --production first before running this task.
 */
gulp.task('upload', function() {
    gulp.src('public/build/**')
        .pipe(s3({
            Bucket: '/business-to-influencer/build',
            ACL:    'public-read'
        }, {
            maxRetries: 50,
            signatureVersion: 'v3'
        }))
    ;
    gulp.src('public/images/**')
        .pipe(s3({
            Bucket: '/business-to-influencer/images',
            ACL:    'public-read'
        }, {
            maxRetries: 50,
            signatureVersion: 'v3'
        }))
    ;
});


