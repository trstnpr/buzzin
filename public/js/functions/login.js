$(document).ready(function() {
    $('#form_bus_login').on('submit', (function(e){
        e.preventDefault();
        $.ajax({
            url: "login/authenticate",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function(){ $('#btn_bus').html('Processing...');},
            error: function(data){
                if(data.readyState == 4){
                    errors = JSON.parse(data.responseText);
                    $('#result_div').empty();
                    $('#result_div').html('<span class="error_list"></span>');
                    $.each(errors,function(key,value){
                        $('.error_list').append('<p><i class="material-icons">error</i>'+value+'</p>');
                    });
                    $('#result_div').removeClass('teal hide').addClass('red');
                    $('#password').val('');
                    $('#btn_bus').html('Submit');

                    setTimeout(function(){$('#result_div').html(''); }, 4000);
                }
            },

            success: function(data){
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    $("#btn_bus").html('Submit');
                    Materialize.toast('Login Successfully', 4000, 'green');
                    location.replace(msg.message);

                } else{
                    $("#password").val('');
                    $("#btn_bus").html('Submit');
                    Materialize.toast(msg.message, 4000, 'red');
                    /*$("#result_div").removeClass('teal hide').addClass('red').html(msg.dialog);*/
                }
            }
        });
    }));

    $('#form_blog_login').on('submit', (function(e){
        e.preventDefault();
        $.ajax({
            url: "login/authenticate",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function(){ $('#btn_blog').html('Processing...');},
            error: function(data){
                if(data.readyState == 4){
                    errors = JSON.parse(data.responseText);
                    $('#result_divs').empty();
                    $('#result_divs').html('<span class="error_list"></span>');
                    $.each(errors,function(key,value){
                        $('.error_list').append('<p><i class="material-icons">error</i>'+value+'</p>');
                    });
                    $('#result_divs').removeClass('teal hide').addClass('red');
                    $('#password1').val('');
                    $('#btn_blog').html('Submit');

                    setTimeout(function(){$('#result_divs').html(''); }, 4000);
                }
            },
            success: function(data){
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    $("#btn_blog").html('Submit');
                    Materialize.toast('Login Successfully', 4000, 'green');
                    location.replace(msg.message);

                } else{
                    $("#password1").val('');
                    $("#btn_blog").html('Submit');
                    Materialize.toast(msg.message, 4000, 'red');
                    /*$("#result_div").removeClass('teal hide').addClass('red').html(msg.dialog);*/
                }
            }
        });
    }));
});