$(document).ready(function() {
    $('#form_create_campaign').on('submit', (function(e){
        e.preventDefault();
        $.ajax({
            url: "store",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function(){ $('#btn_create_campaign').html('Processing...');},
            error: function(data){
                if(data.readyState == 4){
                    errors = JSON.parse(data.responseText);
                    $('#result_div').empty();
                    $('#result_div').html('<span class="error_list"></span>');
                    $.each(errors,function(key,value){
                        $('.error_list').append('<p><i class="material-icons">error</i>'+value+'</p>');
                    });
                    $('#result_div').removeClass('teal hide').addClass('red');
                    $('#btn_create_campaign').html('Submit');

                    setTimeout(function(){$('#result_div').html(''); }, 4000);
                }
            },
            success: function(data){
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                   Materialize.toast(msg.message, 1000, "green", function(){ window.location.href = '/company/campaign'; });
                } else{
                    Materialize.toast(msg.message, 4000, 'red');
                }
            }
        });
    }));

    var camp_id = $('#c_id').val();
    $('#form_update_campaign').on('submit', (function(e){
        console.log(camp_id);
        e.preventDefault();
        $.ajax({
            url: camp_id+"/update",
            type: "POST",
            headers:
            {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function(){ $('#btn_update_campaign').html('Processing...');},
            error: function(data){
                if(data.readyState == 4){
                    errors = JSON.parse(data.responseText);
                    $('#result_div').empty();
                    $('#result_div').html('<span class="error_list"></span>');
                    $.each(errors,function(key,value){
                        $('.error_list').append('<p><i class="material-icons">error</i>'+value+'</p>');
                    });
                    $('#result_div').removeClass('teal hide').addClass('red');
                    $('#btn_update_campaign').html('Submit');

                    setTimeout(function(){$('#result_div').html(''); }, 4000);
                }
            },

            success: function(data){
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    Materialize.toast(msg.message, 1000, "green", function(){ window.location.href = '/company/campaign'; });
                } else{
                    Materialize.toast(msg.message, 4000, 'red');
                }
            }
        });
    }));

    var nColNumber = -1;
    var campaign = $('#campaign').DataTable({
        'ajax': '/company/campaign/getAll',
        'processing': true,
        'order': [[ 1, "desc" ]],
        'columnDefs': [
            { 'targets': [ ++nColNumber ], 'title':'Campaign Name', 'name': 'name', 'data': 'name' },
            { 'targets': [ ++nColNumber ], 'title':'Campaign Category', 'name': 'category', 'data': 'category' },
            { 'targets': [ ++nColNumber ], 'title':'URL', 'name': 'url', 'data': 'url' },
            { 'targets': [ ++nColNumber ], 'title':'Date Start', 'name': 'date_start', 'data': 'date_start' },
            { 'targets': [ ++nColNumber ], 'title':'Date End', 'name': 'date_end', 'data': 'date_end' },
            { 'className': 'center', 'targets': [ ++nColNumber ], 'title':'Actions', 'name': 'action', 'data': 'action'},
           
        ]
    });

    function reloadTable(table)
    {
        table.ajax.reload();
    }

    /*function deleteCampaign(campaign) {
        $('#delete-campaign').openModal();
        $('.btn-delete-yes-campaign').attr('data-campaign-id', $(campaign).attr('data-campaign-id'));
    }*/

    $('.btn-delete-yes-campaign').on('click', function() {
        $.ajax({
            url: 'campaign/'+$(this).attr('data-campaign-id'),
            headers:
            {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
            type: 'DELETE',
            success: function(data) {
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    Materialize.toast(msg.message, 4000, 'green');
                    reloadTable(campaign);
                } else{
                    Materialize.toast(msg.message, 4000, 'red');
                    location.reload();
                }
            }
        });
    });
});