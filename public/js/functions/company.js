var comp_id = $('#comp_id').val();
    $('#form_bus_settings').on('submit', (function(e){
        e.preventDefault();
        $.ajax({
            url: "settings/"+comp_id,
            type: "POST",
            headers:
            {
                'X-CSRF-Token': $('#_token').val()
            },
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function(){ $('#btn_bus_setting').html('Processing...');},
            error: function(data){
                if(data.readyState == 4){
                    errors = JSON.parse(data.responseText);
                    $('#result_div').empty();
                    $('#result_div').html('<span class="error_list"></span>');
                    $.each(errors,function(key,value){
                        $('.error_list').append('<p><i class="material-icons">error</i>'+value+'</p>');
                    });
                    $('#result_div').removeClass('teal hide').addClass('red');
                    $('#btn_bus_setting').html('Submit');

                    setTimeout(function(){$('#result_div').html(''); }, 4000);
                }
            },

            success: function(data){
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    Materialize.toast(msg.message, 1000, "green", function(){ window.location.href = '/company/settings'; });
                } else{
                    Materialize.toast(msg.message, 4000, 'red');
                }
            }
        });
    }));

var prof_id = $('#prof_id').val();
    $('#form_acct_settings').on('submit', (function(e){
        e.preventDefault();
        $.ajax({
            url: "settings/"+prof_id,
            type: "POST",
            headers:
            {
                'X-CSRF-Token': $('#_token').val()
            },
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function(){ $('#btn_acct_setting').html('Processing...');},
            error: function(data){
                if(data.readyState == 4){
                    errors = JSON.parse(data.responseText);
                    $('#result_div').empty();
                    $('#result_div').html('<span class="error_list"></span>');
                    $.each(errors,function(key,value){
                        $('.error_list').append('<p><i class="material-icons">error</i>'+value+'</p>');
                    });
                    $('#result_div').removeClass('teal hide').addClass('red');
                    $('#btn_acct_setting').html('Submit');

                    setTimeout(function(){$('#result_div').html(''); }, 4000);
                }
            },

            success: function(data){
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    Materialize.toast(msg.message, 1000, "green", function(){ window.location.href = '/company/settings'; });
                } else{
                    Materialize.toast(msg.message, 4000, 'red');
                }
            }
        });
    }));

    $('#form_password_settings').on('submit', (function(e){
        e.preventDefault();
        $.ajax({
            url: "changePassword",
            type: "POST",
            headers:
            {
                'X-CSRF-Token': $('#_token').val()
            },
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function(){ $('#btn_password').html('Processing...');},
            error: function(data){
                if(data.readyState == 4){
                    errors = JSON.parse(data.responseText);
                    $('#result_divs').empty();
                    $('#result_divs').html('<span class="error_list"></span>');
                    $.each(errors,function(key,value){
                        $('.error_list').append('<p><i class="material-icons">error</i>'+value+'</p>');
                    });
                    $('#result_divs').removeClass('teal hide').addClass('red');
                    $("#cur_pass").val('');
                    $("#password").val('');
                    $("#confirm").val('');
                    $('#btn_password').html('Update');

                    setTimeout(function(){$('#result_divs').html(''); }, 4000);
                }
            },

            success: function(data){
                var msg = JSON.parse(data);

                if(msg.result == 'success'){
                    Materialize.toast(msg.message, 1000, "green", function(){ window.location.href = '/company/passwordSetting'; });
                } else{
                    Materialize.toast(msg.message, 4000, 'red');
                    $("#cur_pass").val('');
                    $("#password").val('');
                    $("#confirm").val('');
                    $('#btn_password').html('Update');
                }
            }
        });
    }));