<?php

return [
	'campaign' => env('CAMPAIGN_URL', '/images/campaign'),
	'campaignFile' => env('CAMPAIGN_FILE', '/file/campaign'),
];
