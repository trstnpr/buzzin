server "52.220.213.216", port: 22, roles: [:app], :primary => true, user: 'deployer'
set :application => "letsbuzzin.com"
set :branch, "master"
set :deploy_to, "/var/www/vhosts/localhost.localdomain/letsbuzzin.com"
