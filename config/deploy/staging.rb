server "13.76.47.147", port: 22, roles: [:app], :primary => true, user: 'deployer'
set :application => "b2i.devhub.ph"
set :branch, "master"
set :deploy_to, "/var/www/vhosts/localhost.localdomain/b2i.devhub.ph"
