<?php

return [
    'key'           => env('S3_ACCESS_KEY_ID', ''),
    'secret'        => env('S3_SECRET_ACCESS_KEY', ''),
    'endpoint'      => env('S3_ENDPOINT', ''),
    'bucket'        => env('S3_BUCKET', ''),
    'bucket_link'   => env('S3_BUCKET_LINK', ''),
];
